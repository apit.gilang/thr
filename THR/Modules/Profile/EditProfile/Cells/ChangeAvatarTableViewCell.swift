//
//  ChangeAvatarTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 07/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

protocol ChangeAvatarDelegate {
    func didChangeAvatar()
}

class ChangeAvatarTableViewCell: UITableViewCell {

    @IBOutlet weak var changeAvatarButton: UIButton!
    var changeAvatarDelegate: ChangeAvatarDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func changeAvatarPressed(_ sender: Any) {
        changeAvatarDelegate?.didChangeAvatar()
    }
}
