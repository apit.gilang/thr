//
//  ShortCutTableViewCell.swift
//  GreenHcm
//
//  Created by Apit Aprida on 06/03/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

protocol ShortCutDelegate {
    func didSelectShortCut(withShorCut item: ShortCut)
}

class ShortCutTableViewCell: UITableViewCell {

    var shortCuts: [ShortCut] = []

    @IBOutlet weak var collectionView: UICollectionView!
    
    var delegate: ShortCutDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: String(describing: ShortCutCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ShortCutCollectionViewCell.self))
    }
    
    func centerItemsInCollectionView(cellWidth: Double, numberOfItems: Double, spaceBetweenCell: Double, collectionView: UICollectionView) -> UIEdgeInsets {
        let totalWidth = cellWidth * numberOfItems
        let totalSpacingWidth = spaceBetweenCell * (numberOfItems - 1)
        let leftInset = (collectionView.frame.width - CGFloat(totalWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
}

extension ShortCutTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shortCuts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ShortCutCollectionViewCell.self), for: indexPath) as! ShortCutCollectionViewCell
        cell.shortCutLabel.text = shortCuts[indexPath.row].name
        cell.shortCutImage.image = shortCuts[indexPath.row].image
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionSize = collectionView.frame.size.width
        let width = (collectionSize / 4) - 4
        return CGSize(width: width, height: width + 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return centerItemsInCollectionView(cellWidth: Double(collectionView.frame.width / 4) - 4, numberOfItems: Double(shortCuts.count), spaceBetweenCell: 0, collectionView: collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectShortCut(withShorCut: shortCuts[indexPath.item])
    }
}
