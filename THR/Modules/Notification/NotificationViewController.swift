//
//  NotificationViewController.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/7/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantNotification {
    // TODO: Add segue ids
    static let pushToDetail = "pushToDetail"
    static let pushToLeave = "pushToLeave"
    static let pushToApproval = "pushToApproval"
    static let pushToOvertime = "pushToOvertime"
}

// MARK: -  NotificationViewController

class NotificationViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var notifSeparator: UIView!
    @IBOutlet weak var infoSeparator: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var notifView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var notifButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    
    var isInfo = false
    var selectedIndex = -1
    var presenter: NotificationPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationPresenter.config(withNotificationViewController: self)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        notifSeparator.backgroundColor = UIColor.appColor(.blue)
        infoSeparator.backgroundColor = UIColor.white
        notifView.tintColor = UIColor.appColor(.blue)
        infoView.tintColor = UIColor.gray
        notifButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        presenter.getNotification()
    }
    
    
    @IBAction func notifButton(_ sender: Any) {
        notifSeparator.backgroundColor = UIColor.appColor(.blue)
        infoSeparator.backgroundColor = UIColor.white
        notifView.tintColor = UIColor.appColor(.blue)
        infoView.tintColor = UIColor.gray
        notifButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        isInfo = false
        tableView.reloadData()
        
    }
    @IBAction func infoButton(_ sender: Any) {
        notifSeparator.backgroundColor = UIColor.white
        infoSeparator.backgroundColor = UIColor.appColor(.blue)
        notifView.tintColor = UIColor.gray
        infoView.tintColor = UIColor.appColor(.blue)
        infoButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        isInfo = true
        tableView.reloadData()
    }
    
}
// MARK: - NotificationView
extension NotificationViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isInfo ? 5 : presenter.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isInfo {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: InfoTableViewCell.self), for: indexPath) as! InfoTableViewCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NotificationTableViewCell.self), for: indexPath) as! NotificationTableViewCell
            cell.setupNotificationView(notification: presenter.notifications[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        presenter.updateNotification(withId: presenter.notifications[selectedIndex].id ?? 0)
    }
}
extension NotificationViewController: NotificationView {
    func onErrorRequestAPI(withMessage message: String) {
        showPageError(withMessage: message)
    }
    
    func onSuccessUpdateNotif() {
        if isInfo {
            performSegue(withIdentifier: SegueConstantNotification.pushToDetail, sender: nil)
        } else {
            let selectedNotif = presenter.notifications[selectedIndex]
            if selectedNotif.notifableType == EnumApprovalType.approval.typeName {
                performSegue(withIdentifier: SegueConstantNotification.pushToApproval, sender: nil)
            } else if selectedNotif.notifableType == EnumApprovalType.leave.typeName {
                performSegue(withIdentifier: SegueConstantNotification.pushToLeave, sender: nil)
            } else {
                performSegue(withIdentifier: SegueConstantNotification.pushToOvertime, sender: nil)
            }
        }
    }
    
    func updateView() {
        tableView.reloadData()
    }
    
}
