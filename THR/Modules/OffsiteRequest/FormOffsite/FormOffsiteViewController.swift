//
//  FormOffsiteViewController.swift
//  THR
//
//  Created by Apit Aprida on 06/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import BottomPopup
import GoogleMaps
import CoreLocation
import AssistantKit
import NVActivityIndicatorView

// MARK: - SegueConstants

enum SegueConstantFormOffsite {
    // TODO: Add segue ids
}

protocol FormOffsiteRequestDelegate {
    func didFinishedRequest()
}

// MARK: -  FormOffsiteViewController

class FormOffsiteViewController: CustomBottomPopupViewController {
    
    // MARK: Properties
    @IBOutlet weak var formRequestView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var startTimeField: UITextField!
    @IBOutlet weak var pinImageView: UIImageView!
    @IBOutlet weak var lockLocationSwitch: UISwitch!
    @IBOutlet weak var radiusField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var reasonField: UITextField!
    
    var presenter: FormOffsitePresenter!
    var locationManager = CLLocationManager()
    var latitude: Double?
    var longitude: Double?
    var address: String?
    var marker: GMSMarker!
    var parameter = OffsiteRequestParamApi()
    private var startTimePicker = UIDatePicker()
    var formOffsiteDelegate: FormOffsiteRequestDelegate?
    var circle = GMSCircle()
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        FormOffsitePresenter.config(withFormOffsiteViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        mapView.bringSubviewToFront(pinImageView)
        mapView.delegate = self
        reasonField.delegate = self
        radiusField.delegate = self
        addressField.delegate = self
        customDateDelegate = self
        startTimeField.tintColor = .appColor(.whiteBlue)
        setupPicker(datePicker: startTimePicker, textField: startTimeField, mode: .date, type: .offsiteAttendance)
    }
    
    override var height: CGFloat {
        return UIScreen.main.bounds.size.height * 0.8
    }
    
    override var cornerRadius: CGFloat {
        return 20
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        reasonField.resignFirstResponder()
        radiusField.resignFirstResponder()
        addressField.resignFirstResponder()
        parameter.addressName = addressField.text
        var error = ""
        if parameter.longtitude == nil || parameter.latitude == nil {
            error = "Please wait, your location still updating."
        } else if parameter.date == nil {
            error = "Date can't be empty."
        } else if parameter.reason == nil {
            error = "Reason can't be empty."
        } else if parameter.addressName == nil {
            error = "Address can't be empty."
        } else if parameter.radius == nil {
            error = "Radius can't be empty."
        }
        if error == "" {
            presenter.submitOffsiteRequest(withParameter: parameter)
        } else {
            alert.showBasicAlert("", message: error, controller: self)
        }
    }
    
    @IBAction func lockLocationSwitched(_ sender: Any) {
        mapView.isHidden = !lockLocationSwitch.isOn
    }
    
    func reverseGeocode(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            self.addressField.text = lines.joined(separator: "\n")
            self.parameter.latitude = coordinate.latitude
            self.parameter.longtitude = coordinate.longitude
            let labelHeight = self.mapView.frame.size.height / 2
            self.mapView.padding = UIEdgeInsets(
                top: labelHeight,
                left: 0,
                bottom: labelHeight - 24.5,
                right: 0)
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func drawCircle(position: CLLocationCoordinate2D, radius: Double) {
        circle.map = nil
        circle = GMSCircle(position: position, radius: radius)
        circle.strokeColor = UIColor.appColor(.blue)
        circle.fillColor = UIColor.appColor(.lightBlue).withAlphaComponent(0.25)
        circle.map = mapView
    }
}

extension FormOffsiteViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == radiusField {
            if let input = textField.text {
                let radius = Double(input)
                let position = CLLocationCoordinate2D(latitude: parameter.latitude ?? 0, longitude: parameter.longtitude ?? 0)
                drawCircle(position: position, radius: radius ?? 0)
                parameter.radius = radius
            }
        } else if textField == addressField {
            parameter.addressName = textField.text
        } else if textField == reasonField {
            parameter.reason = textField.text
        }
    }
}

extension FormOffsiteViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocode(coordinate: position.target)
    }
}

extension FormOffsiteViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        reverseGeocode(coordinate: position)
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

extension FormOffsiteViewController: CustomDatePickerDelegate {
    func didChangedPicker(withPicker picker: UIDatePicker) {
        parameter.date = getDate(from: startTimePicker.date, format: "yyyy-MM-dd")
        startTimeField?.text = getDate(from: startTimePicker.date)
    }
    
    func didFinishedPicker(withPicker picker: UIDatePicker) {
        parameter.date = getDate(from: startTimePicker.date, format: "yyyy-MM-dd")
        startTimeField.text = getDate(from: startTimePicker.date)
    }
}

// MARK: - FormOffsiteView

extension FormOffsiteViewController: FormOffsiteView {
    func onSuccessSubmitOffsite() {
        dismiss(animated: true) {
            self.formOffsiteDelegate?.didFinishedRequest()
        }
    }
    
    func onErrorSubmitOffsite(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
