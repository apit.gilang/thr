//
//  DetailArticleTableViewCell.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/15/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

protocol LoveButtonDelegate {
    func didPressLoveButton()
}

class DetailArticleTableViewCell: UITableViewCell {
    @IBOutlet weak var loveImage: UIImageView!
    @IBOutlet weak var lovedLabel: UILabel!
    
    var loveButtonDelegate: LoveButtonDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        loveButtonDelegate?.didPressLoveButton()
    }
    
}
