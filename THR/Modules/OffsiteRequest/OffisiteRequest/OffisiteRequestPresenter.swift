//
//  OffisiteRequestPresenter.swift
//  THR
//
//  Created by Apit Aprida on 06/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol OffisiteRequestViewPresenter: class {
    init(view: OffisiteRequestView)
    func getOffsiteRequest(isShowToast: Bool)
}

protocol OffisiteRequestView: class {
    func onSuccessGetOffsiteRequest(isShowToast: Bool)
    func onErrorGetOffsiteRequest(withMessage message: String)
}

class OffisiteRequestPresenter: OffisiteRequestViewPresenter {
    
    static func config(withOffisiteRequestViewController vc: OffisiteRequestViewController) {
        let presenter = OffisiteRequestPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: OffisiteRequestView
    var offsiteRequest: OffsiteRequest?
    
    required init(view: OffisiteRequestView) {
        self.view = view
    }
    
    func getOffsiteRequest(isShowToast: Bool) {
        THRApi.instance.request(ApiOffsiteRequest.getOffsiteRequest, success: { (json) in
            self.offsiteRequest = OffsiteRequest(json: json)
            self.view.onSuccessGetOffsiteRequest(isShowToast: isShowToast)
        }) { (error) in
            self.view.onErrorGetOffsiteRequest(withMessage: error)
        }
    }
}
