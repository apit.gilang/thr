//
//  ResumeButtonTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 01/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class ResumeButtonTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
