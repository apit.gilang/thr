//
//  Settings.swift
//  THR
//
//  Created by Apit Aprida on 07/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

class Settings {
    enum Keys: String {
        case accessToken = "accessToken"
        case secretKey = "secretKey"
        case fcmToken = "fcmToken"
        case isRegisteredFace = "isRegisteredFace"
        case theme = "theme"
        case userFullname = "userFullname"
    }

    static var accessToken: String? {
        get {
            return UserDefaults.standard.string(forKey: Keys.accessToken.rawValue) ?? ""
        }
        set {
            return UserDefaults.standard.set(newValue, forKey: Keys.accessToken.rawValue)
        }
    }
    
    static var secretKey: String? {
        get {
            return UserDefaults.standard.string(forKey: Keys.secretKey.rawValue) ?? ""
        }
        set {
            return UserDefaults.standard.set(newValue, forKey: Keys.secretKey.rawValue)
        }
    }
    
    static var userFullname: String? {
        get {
            return UserDefaults.standard.string(forKey: Keys.userFullname.rawValue) ?? ""
        }
        set {
            return UserDefaults.standard.set(newValue, forKey: Keys.userFullname.rawValue)
        }
    }
    
    static var isRegisteredFace: Bool {
        get {
            return UserDefaults.standard.bool(forKey: Keys.isRegisteredFace.rawValue)
        }
        set {
            return UserDefaults.standard.set(newValue, forKey: Keys.isRegisteredFace.rawValue)
        }
    }

    static var fcmToken: String? {
        get {
            return UserDefaults.standard.string(forKey: Keys.fcmToken.rawValue) ?? ""
        }
        set {
            return UserDefaults.standard.set(newValue, forKey: Keys.fcmToken.rawValue)
        }
    }

    static var theme: String? {
        get {
            return UserDefaults.standard.string(forKey: Keys.theme.rawValue) ?? ""
        }
        set {
            return UserDefaults.standard.set(newValue, forKey: Keys.theme.rawValue)
        }
    }
    
    static var isDeviceJailbroken: Bool {

        guard TARGET_IPHONE_SIMULATOR != 1 else { return false }

        // Check 1 : existence of files that are common for jailbroken devices
        if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
        || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
        || FileManager.default.fileExists(atPath: "/bin/bash")
        || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
        || FileManager.default.fileExists(atPath: "/etc/apt")
        || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
        || UIApplication.shared.canOpenURL(URL(string:"cydia://package/com.example.package")!) {

            return true
        }

        // Check 2 : Reading and writing in system directories (sandbox violation)
        let stringToWrite = "Jailbreak Test"
        do {
            try stringToWrite.write(toFile:"/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
            // Device is jailbroken
            return true
        } catch {
            return false
        }
    }
}
