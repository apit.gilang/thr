//
//  NotificationPresenter.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/7/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol NotificationViewPresenter: class {
    init(view: NotificationView)
    // TODO: Declare view presenter methods
}

protocol NotificationView: class {
    func updateView()
    func onSuccessUpdateNotif()
    func onErrorRequestAPI(withMessage message: String)
}

class NotificationPresenter: NotificationViewPresenter {
    
    static func config(withNotificationViewController vc: NotificationViewController) {
        let presenter = NotificationPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: NotificationView
    
    var notifications: [NotificationData] = []
    
    required init(view: NotificationView) {
        self.view = view
    }
    
    func parseDummyData() {
        if let path = Bundle.main.path(forResource: "notification", ofType: "json") {
            let jsonData = NSData(contentsOfFile: path)
            let json = try! JSON(data: jsonData! as Data)
            for data in json["notifications"].arrayValue {
                self.notifications.append(NotificationData(json: data))
            }
            self.view.updateView()
        }
    }
    
    func updateNotification(withId id: Int) {
        THRApi.instance.request(ApiNotification.updateNotification(id: id), success: { (json) in
            self.view.onSuccessUpdateNotif()
        }) { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }
    }
    
    func getNotification() {
        THRApi.instance.request(ApiNotification.getNotification, success: { (json) in
            guard let json = json else {
                return
            }
            for data in json["notifications"].arrayValue {
                self.notifications.append(NotificationData(json: data))
            }
            self.view.updateView()
        }) { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }
    }
    
}
