//
//  FeelingCollectionViewCell.swift
//  THR
//
//  Created by Apit Aprida on 01/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class FeelingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var feelingLabel: UILabel!
}
