//
//  FormOffsitePresenter.swift
//  THR
//
//  Created by Apit Aprida on 06/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol FormOffsiteViewPresenter: class {
    init(view: FormOffsiteView)
    func submitOffsiteRequest(withParameter parameter: OffsiteRequestParamApi)
}

protocol FormOffsiteView: class {
    func onSuccessSubmitOffsite()
    func onErrorSubmitOffsite(withMessage message: String)
}

class FormOffsitePresenter: FormOffsiteViewPresenter {
    
    static func config(withFormOffsiteViewController vc: FormOffsiteViewController) {
        let presenter = FormOffsitePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: FormOffsiteView
    
    required init(view: FormOffsiteView) {
        self.view = view
    }
    
    func submitOffsiteRequest(withParameter parameter: OffsiteRequestParamApi) {
        THRApi.instance.request(ApiOffsiteRequest.submitOffsiteRequest(parameter: parameter), success: { (json) in
            self.view.onSuccessSubmitOffsite()
        }) { (error) in
            self.view.onErrorSubmitOffsite(withMessage: error)
        }
    }
}
