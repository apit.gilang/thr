//
//  ApiTodo.swift
//  THR
//
//  Created by Apit Gilang Aprida on 12/21/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiTodo: URLRequestConvertible {
    
    case getTodo
    case deleteTodo(id: Int)
    case createTodo(parameter: CreateTodoParamApi)
    case editTodo(id: Int, parameter: CreateTodoParamApi)
    
    var path: String {
        switch self {
        case .deleteTodo(let id):
            return "to_do_lists/\(id)"
        case .editTodo(let id, _):
            return "/to_do_lists/\(id)"
        default:
            return "/to_do_lists"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getTodo:
            return .get
        case .deleteTodo:
            return .delete
        case .createTodo:
            return .post
        case .editTodo:
            return .put
        }
    }
    
    var parameter: [String: AnyObject] {
        switch self {
        case .createTodo(let parameter), .editTodo(_, let parameter):
            return parameter.buildForParameters()
        default:
            return [:]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.thrHeaders(withToken: Settings.accessToken, secretKey: Settings.secretKey)
        return try HTTPMethod.get.rawValue == urlRequest.httpMethod ? URLEncoding.default.encode(urlRequest, with: self.parameter) : JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
