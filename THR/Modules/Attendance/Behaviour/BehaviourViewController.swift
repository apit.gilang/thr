//
//  BehaviourViewController.swift
//  THR
//
//  Created by Apit Aprida on 01/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import AssistantKit

// MARK: - SegueConstants

enum SegueConstantBehaviour {
    static let pushToFinished = "pushToFinished"
}

extension Array where Element: Equatable {

    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        guard let index = firstIndex(of: object) else {return}
        remove(at: index)
    }

}

struct Feels {
    var name: String
    var isSelected: Bool
}

enum FeelingType {
    case sad
    case random
    case flat
    case smile
    case laughing
}

enum feelsbtn {
    case positive
    case negative
}

// MARK: -  BehaviourViewController

class BehaviourViewController: ViewController {
    
    let columnLayout = ColumnFlowLayout (
        cellsPerRow: 3
    )
    
    // MARK: Properties
    @IBOutlet weak var sadImageView: UIImageView!
    @IBOutlet weak var randomImageView: UIImageView!
    @IBOutlet weak var flatImageView: UIImageView!
    @IBOutlet weak var smileImageView: UIImageView!
    @IBOutlet weak var laughingImageView: UIImageView!

    @IBOutlet weak var negativeBtn: UIButton!
    @IBOutlet weak var positiveBtn: UIButton!

    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var noteText: UITextView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var heightNoteText: NSLayoutConstraint!
    
    var presenter: BehaviourPresenter!
    
    var behaviorId: Int = 1
    var behaviorFeel: Bool = false
    var arrFeels: [Feels] = []
    var selectedFeels = [String]()
    var absenceData: AttendanceLocation?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        BehaviourPresenter.config(withBehaviourViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCollectionView()
        updateUiFelling(data: .flat)
        updateFellingBtn(feel: .negative)
        hideKeyboardWhenTappedAround()
        collectionView.reloadData()
        
        heightNoteText.constant = Device.screen == .inches_4_0 ? 80 : 145
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    private func updateUiFelling(data: FeelingType) {
        switch data {
        case .sad:
            sadImageView.isHighlighted = true
            randomImageView.isHighlighted = false
            flatImageView.isHighlighted = false
            smileImageView.isHighlighted = false
            laughingImageView.isHighlighted = false
        case .random:
            sadImageView.isHighlighted = false
            randomImageView.isHighlighted = true
            flatImageView.isHighlighted = false
            smileImageView.isHighlighted = false
            laughingImageView.isHighlighted = false
        case .flat:
            sadImageView.isHighlighted = false
            randomImageView.isHighlighted = false
            flatImageView.isHighlighted = true
            smileImageView.isHighlighted = false
            laughingImageView.isHighlighted = false
        case .smile:
            sadImageView.isHighlighted = false
            randomImageView.isHighlighted = false
            flatImageView.isHighlighted = false
            smileImageView.isHighlighted = true
            laughingImageView.isHighlighted = false
        case .laughing:
            sadImageView.isHighlighted = false
            randomImageView.isHighlighted = false
            flatImageView.isHighlighted = false
            smileImageView.isHighlighted = false
            laughingImageView.isHighlighted = true
        }
    }
    
    private func updateFellingBtn(feel: feelsbtn) {
        selectedFeels.removeAll()
        switch feel {
        case .negative:
            negativeBtn.backgroundColor = UIColor.appColor(.yellow)
            negativeBtn.setTitleColor(.white, for: .normal)
            positiveBtn.backgroundColor = .clear
            positiveBtn.setTitleColor(UIColor.appColor(.darkGray), for: .normal)
            arrFeels = presenter.negativeArr()
            collectionView.reloadData()
        case .positive:
            positiveBtn.backgroundColor = UIColor.appColor(.yellow)
            positiveBtn.setTitleColor(.white, for: .normal)
            negativeBtn.backgroundColor = .clear
            negativeBtn.setTitleColor(UIColor.appColor(.darkGray), for: .normal)
            arrFeels = presenter.positiveArr()
            collectionView.reloadData()
        }
    }
    
    func setCollectionView() {
        collectionView?.collectionViewLayout = columnLayout
        collectionView?.contentInsetAdjustmentBehavior = .always
        self.collectionView.reloadData()
    }
    
    @IBAction func behaviorBtnPressed(_ sender: UIButton) {
        if sender.tag == 1 {
            updateUiFelling(data: .sad)
        } else if sender.tag == 2 {
            updateUiFelling(data: .random)
        } else if sender.tag == 3 {
            updateUiFelling(data: .flat)
        } else if sender.tag == 4 {
            updateUiFelling(data: .smile)
        } else if sender.tag == 5 {
            updateUiFelling(data: .laughing)
        }
        behaviorId = sender.tag
    }
    
    @IBAction func feelingBtnPressed(_ sender: UIButton) {
        if sender.tag == 0 {
            updateFellingBtn(feel: .negative)
            behaviorFeel = false
        } else if sender.tag == 1 {
            updateFellingBtn(feel: .positive)
            behaviorFeel = true
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        let param = BehaviourParamApi()
        let sliceArr = selectedFeels.map { String($0) }
        let mergeArr = sliceArr.joined(separator: ", ")
        param.scale = behaviorId
        param.note = noteText.text
        param.isPositif = behaviorFeel
        param.emotion = mergeArr
        param.idAbsence = absenceData?.id
        presenter.submitBehaviour(withParameter: param)
    }
}

extension BehaviourViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFeels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FeelingCollectionViewCell.self), for: indexPath) as! FeelingCollectionViewCell
        cell.feelingLabel.text = arrFeels[indexPath.row].name.capitalized
        if arrFeels[indexPath.row].isSelected {
            cell.feelingLabel.textColor = .black
        } else {
            cell.feelingLabel.textColor = .lightGray
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = 40
        return CGSize(width: collectionView.frame.size.width / 3, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if arrFeels[indexPath.row].isSelected {
            arrFeels[indexPath.row].isSelected = false
            selectedFeels.remove(object: (arrFeels[indexPath.row].name))
        } else {
            arrFeels[indexPath.row].isSelected = true
            selectedFeels.append((arrFeels[indexPath.row].name))
        }
        collectionView.reloadData()
    }
}

// MARK: - BehaviourView

extension BehaviourViewController: BehaviourView {
    func onSuccessSubmitBehaviour() {
        performSegue(withIdentifier: SegueConstantBehaviour.pushToFinished, sender: nil)
    }
    
    func onErrorSubmitBehaviour(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
