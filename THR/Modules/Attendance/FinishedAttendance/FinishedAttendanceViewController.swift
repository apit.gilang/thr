//
//  FinishedAttendanceViewController.swift
//  THR
//
//  Created by Apit Aprida on 05/08/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantFinishedAttendance {
    // TODO: Add segue ids
}

// MARK: -  FinishedAttendanceViewController

class FinishedAttendanceViewController: ViewController {
    
    // MARK: Properties
    
    var presenter: FinishedAttendancePresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        FinishedAttendancePresenter.config(withFinishedAttendanceViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        navigationController?.popToRootViewController(animated: false)
    }
}

// MARK: - FinishedAttendanceView

extension FinishedAttendanceViewController: FinishedAttendanceView {
    // TODO: implement view methods
}
