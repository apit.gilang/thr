//
//  PageErrorViewController.swift
//  THR
//
//  Created by Apit Aprida on 29/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantPageError {
    // TODO: Add segue ids
}

// MARK: -  PageErrorViewController

class PageErrorViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var errorImage: UIImageView!
    @IBOutlet weak var messageErrorLabel: UILabel!
    
    var presenter: PageErrorPresenter!
    var message: String?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        PageErrorPresenter.config(withPageErrorViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageErrorLabel.text = message
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true)
    }
}

// MARK: - PageErrorView

extension PageErrorViewController: PageErrorView {
    // TODO: implement view methods
}
