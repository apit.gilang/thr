//
//  ApiLeave.swift
//  THR
//
//  Created by Apit Aprida on 10/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiLeave: URLRequestConvertible {
    
    case getLeaveType
    case getLeave
    case createLeave
    
    var path: String {
        switch self {
        case .getLeaveType:
            return "/leaves/leave_type"
        default:
            return "/leaves"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getLeaveType, .getLeave:
            return .get
        case .createLeave:
            return .post
        }
    }
    
    var parameter: [String: AnyObject] {
        return [:]
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.thrHeaders(withToken: Settings.accessToken, secretKey: Settings.secretKey)
        return try HTTPMethod.get.rawValue == urlRequest.httpMethod ? URLEncoding.default.encode(urlRequest, with: self.parameter) : JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
