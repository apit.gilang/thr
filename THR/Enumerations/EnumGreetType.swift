//
//  EnumGreetType.swift
//  THR
//
//  Created by Apit Aprida on 15/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

enum EnumGreetType: Int {
    case love = 1, chat, handShake, thumbsUp, smile
    
    var icon: UIImage {
        switch self {
        case .love:
            return UIImage(named: "ic_passion")!
        case .chat:
            return UIImage(named: "ic_love")!
        case .handShake:
            return UIImage(named: "ic_deal")!
        case .thumbsUp:
            return UIImage(named: "ic_like")!
        case .smile:
            return UIImage(named: "ic_smile")!
        }
    }
}
