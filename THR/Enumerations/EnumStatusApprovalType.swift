//
//  EnumStatusApprovalType.swift
//  THR
//
//  Created by Apit Aprida on 12/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

enum EnumStatusApprovalType: Int {
    case pending = 1, approved, rejected
    
    var statusName: String {
        switch self {
        case .pending:
            return "Pending"
        case .approved:
            return "Approved"
        case .rejected:
            return "Rejected"
        }
    }
    
    var statusColor: UIColor {
        switch self {
        case .pending:
            return UIColor.appColor(.yellow)
        case .approved:
            return UIColor.appColor(.blue)
        case .rejected:
            return UIColor.appColor(.red)
        }
    }
}
