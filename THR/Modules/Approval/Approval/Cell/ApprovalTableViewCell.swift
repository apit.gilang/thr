//
//  ApprovalTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class ApprovalTableViewCell: UITableViewCell {

    @IBOutlet weak var typeView: DesignableView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var approvalTitleLabel: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var approvalView: DesignableView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func setupContentApproval(withApproval approval: ApprovalData) {
        typeView.backgroundColor = approval.approvalableType?.typeColor
        typeLabel.text = approval.approvalableType?.typeName
        approvalTitleLabel.text = approval.userFullname
        dateLabel.text = approval.getDateToShow()
        timeLabel.text = approval.note
        dateView.isHidden = approval.approvalableType == .leave ? false : true
        timeView.isHidden = approval.approvalableType == .overtime ? false : true
    }
}
