//
//  Approval.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Approval {
    var approvals: [ApprovalData] = []
    var currentPage: Int?
    var totalPage: Int?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        currentPage = json["current_page"].int
        totalPage = json["total_page"].int
        
        for approval in json["approvals"].arrayValue {
            approvals.append(ApprovalData(json: approval))
        }
    }
}
