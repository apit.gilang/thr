//
//  FormLeaveViewController.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import BottomPopup
import AssistantKit
import NVActivityIndicatorView

// MARK: - SegueConstants

enum SegueConstantFormLeave {
    // TODO: Add segue ids
}

protocol FormLeaveDelegate {
    func didFinishedRequest()
}

// MARK: -  FormLeaveViewController

class FormLeaveViewController: CustomBottomPopupViewController {
    
    // MARK: Properties
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var attachmentView: DesignableView!
    @IBOutlet weak var attachmentImageView: UIImageView!
    @IBOutlet weak var resultAttachmentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var startDateView: UIView!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var startDateField: UITextField!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateView: UIView!
    @IBOutlet weak var endDateField: UITextField!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var startTimeField: UITextField!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeField: UITextField!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var tapToAddLabel: UILabel!
    @IBOutlet weak var reasonTextView: UITextView!
    
    var presenter: FormLeavePresenter!
    var source: EnumApprovalType?
    var formLeaveDelegae: FormLeaveDelegate?
    private var startTimePicker = UIDatePicker()
    private var endTimePicker = UIDatePicker()
    private var startDatePicker = UIDatePicker()
    private var endDatePicker = UIDatePicker()
    private var selectedItem = -1
    private var parameterOvertime = OvertimeParamApi()
    private var parameterLeave = LeaveParamApi()
    private var selectedLeaveType: LeaveType?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        FormLeavePresenter.config(withFormLeaveViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPicker()
        setupFormContent()
        reasonTextView.delegate = self
        customDateDelegate = self
    }
    
    override var cornerRadius: CGFloat {
        return 20
    }
    
    override var height: CGFloat {
        switch Device.screen {
        case .inches_4_0:
            return UIScreen.main.bounds.size.height * 0.9
        case .inches_4_7, .inches_5_5:
            return UIScreen.main.bounds.size.height * 0.8
        default:
            return UIScreen.main.bounds.size.height * 0.7
        }
    }
    
    private func setupFormContent() {
        switch source {
        case .leave:
            resultAttachmentView.isHidden = true
            timeView.isHidden = true
            attachmentView.isHidden = false
            collectionView.isHidden = false
            endDateView.isHidden = false
            startLabel.text = "Start"
            presenter.getLeaveType()
        default:
            resultAttachmentView.isHidden = true
            timeView.isHidden = false
            attachmentView.isHidden = true
            collectionView.isHidden = false
            endDateView.isHidden = true
            startLabel.text = "Date"
            presenter.getOvertimeType()
        }
    }
    
    private func setupPicker() {
        switch source {
        case .overtime:
            setupPicker(datePicker: startTimePicker, textField: startTimeField, mode: .time, type: .overtime)
            setupPicker(datePicker: endTimePicker, textField: endTimeField, mode: .time, type: .overtime)
            setupPicker(datePicker: startDatePicker, textField: startDateField, mode: .date, type: .overtime)
            setupPicker(datePicker: endDatePicker, textField: endDateField, mode: .date, type: .overtime)
        default:
            setupPicker(datePicker: startTimePicker, textField: startTimeField, mode: .time, type: .leave)
            setupPicker(datePicker: endTimePicker, textField: endTimeField, mode: .time, type: .leave)
            setupPicker(datePicker: startDatePicker, textField: startDateField, mode: .date, type: .leave)
            setupPicker(datePicker: endDatePicker, textField: endDateField, mode: .date, type: .leave)
        }
    }
    
    private func validateOvertimeForm() -> String {
        var error = ""
        if parameterOvertime.date == nil {
            error = "Date can't be empty."
        } else if parameterOvertime.startTime == nil {
            error = "Start time can't be empty."
        } else if parameterOvertime.endTime == nil {
            error = "End time can't be empty."
        } else if parameterOvertime.reason == nil {
            error = "Reason can't be empty."
        } else if endTimePicker.date.compare(.isEarlier(than: startTimePicker.date)) {
            error = "End time can't be earlier than start time"
        }
        return error
    }
    
    private func validateLeaveForm() -> String {
        var error = ""
        print(endDatePicker.date.compare(.isEarlier(than: startDatePicker.date)))
        if endDatePicker.date.compare(.isEarlier(than: startDatePicker.date)) {
           error = "End date can't be earlier than start date"
        } else if parameterLeave.leaveTypeId == nil {
            error = "Please choose leave type."
        } else if parameterLeave.fileUpload == nil && selectedLeaveType?.isAttachment == true {
            error = "Please choose attachment file."
        } else if parameterLeave.startDate == nil {
            error = "Start date can't be empty."
        } else if parameterLeave.endDate == nil {
            error = "End date can't be empty."
        } else if parameterLeave.reason == nil {
            error = "Reason can't be empty."
        }
        return error
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        reasonTextView.resignFirstResponder()
        switch source {
        case .leave:
            if validateLeaveForm() == "" {
                presenter.requestLeave(withParameter: parameterLeave)
            } else {
                alert.showBasicAlert("", message: validateLeaveForm(), controller: self)
            }
        default:
            if validateOvertimeForm() == "" {
                presenter.requestOvertime(withParameter: parameterOvertime)
            } else {
                alert.showBasicAlert("", message: validateOvertimeForm(), controller: self)
            }
        }
    }
    
    @IBAction func addAttachmentPressed(_ sender: Any) {
        showAttachmentOptions()
    }
    
    @IBAction func editAttachmentPressed(_ sender: Any) {
        showAttachmentOptions()
    }
    
    private func showAttachmentOptions() {
        let camera = UIAlertAction(title: "Camera", style: .default) { _ in
            self.openCamera()
        }
        let gallery = UIAlertAction(title: "Photo Library", style: .default) { _ in
            self.openPhotoLibrary()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let alert = UIAlertController(title: "", message: "Choose photo or camera for attachment", preferredStyle: .actionSheet)
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancel)
        self.present(alert, animated: true)
    }
    
    private func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}

extension FormLeaveViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[.editedImage] as? UIImage {
            attachmentView.isHidden = true
            resultAttachmentView.isHidden = false
            attachmentImageView.image = image
            parameterLeave.fileUpload = compressedImage(withImage: image)
        } else {
            alert.showBasicAlert("", message: "Unknown error", controller: self)
        }
        dismiss(animated: false)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: false)
    }
    
    func compressedImage(withImage image: UIImage) -> Data? {
        let rect = CGRect(x: 0, y: 0, width: image.size.width/2, height: image.size.height/2)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        
        // Compress image size
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let compressedImageData = resizedImage!.jpegData(compressionQuality: 0.1)
        return compressedImageData
    }
}

extension FormLeaveViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        tapToAddLabel.alpha = 0
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        tapToAddLabel.alpha = textView.text.isEmptyAfterTrim ? 1 : 0
        parameterOvertime.reason = textView.text.isEmptyAfterTrim ? nil : textView.text
        parameterLeave.reason = textView.text.isEmptyAfterTrim ? nil : textView.text
    }
}

extension FormLeaveViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.leaveTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: LeaveTypeCollectionViewCell.self), for: indexPath) as! LeaveTypeCollectionViewCell
        cell.typeLabel.text = presenter.leaveTypes[indexPath.row].name
        if selectedItem == indexPath.row {
            cell.roundedView.backgroundColor = UIColor.appColor(.cyan)
        } else {
            cell.roundedView.backgroundColor = UIColor.appColor(.lightGray).withAlphaComponent(0.7)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let title = presenter.leaveTypes[indexPath.row].name ?? ""
        let size: CGSize = title.size(withAttributes: nil)
        let height: CGFloat = 30
        return CGSize(width: size.width + 40, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedItem = indexPath.row
        parameterLeave.leaveTypeId = presenter.leaveTypes[selectedItem].id
        DispatchQueue.main.async {
            collectionView.reloadData()
        }
    }
}

extension FormLeaveViewController: CustomDatePickerDelegate {
    func didChangedPicker(withPicker picker: UIDatePicker) {
        if picker == startDatePicker {
            parameterOvertime.date = getDate(from: startDatePicker.date, format: "yyyy-MM-dd")
            parameterLeave.startDate = getDate(from: startDatePicker.date, format: "yyyy-MM-dd")
            startDateLabel?.text = getDate(from: startDatePicker.date)
        } else if picker == endDatePicker {
            parameterLeave.endDate = getDate(from: startTimePicker.date, format: "yyyy-MM-dd")
            endDateLabel?.text = getDate(from: endDatePicker.date)
        } else if picker == startTimePicker {
            parameterOvertime.startTime = getTime(from: startTimePicker.date, format: "HH:mm")
            startTimeLabel?.text = getTime(from: startTimePicker.date)
        } else if picker == endTimePicker {
            parameterOvertime.endTime = getTime(from: endTimePicker.date, format: "HH:mm")
            endTimeLabel?.text = getTime(from: endTimePicker.date)
        }
    }
    
    func didFinishedPicker(withPicker picker: UIDatePicker) {
        if picker == startDatePicker {
            parameterOvertime.date = getDate(from: startDatePicker.date, format: "yyyy-MM-dd")
            parameterLeave.startDate = getDate(from: startDatePicker.date, format: "yyyy-MM-dd")
            startDateLabel.text = getDate(from: startDatePicker.date)
        } else if picker == endDatePicker {
            parameterLeave.endDate = getDate(from: startTimePicker.date, format: "yyyy-MM-dd")
            endDateLabel.text = getDate(from: endDatePicker.date)
        } else if picker == startTimePicker {
            parameterOvertime.startTime = getTime(from: startTimePicker.date, format: "HH:mm")
            startTimeLabel.text = getTime(from: startTimePicker.date)
        } else if picker == endTimePicker {
            parameterOvertime.endTime = getTime(from: endTimePicker.date, format: "HH:mm")
            endTimeLabel.text = getTime(from: endTimePicker.date)
        }
    }
}

// MARK: - FormLeaveView

extension FormLeaveViewController: FormLeaveView {
    func onSuccessRequestOvertime() {
        dismiss(animated: true) {
            self.formLeaveDelegae?.didFinishedRequest()
        }
    }
    
    func onSuccessRequestLeave() {
        dismiss(animated: true) {
            self.formLeaveDelegae?.didFinishedRequest()
        }
    }
    
    func onSuccessGetLeaveType() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.collectionView.setNeedsDisplay()
        }
    }
    
    func onErrorRequestAPI(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
