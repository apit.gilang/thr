//
//  EnumShortCut.swift
//  THR
//
//  Created by Apit Aprida on 29/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

enum EnumShortCut: Int {
    case attendance = 1, overtime, leave, offsite, approval, performance, more
    
    var name: String {
        switch self {
        case .attendance:
            return "Attendance"
        case .overtime:
            return "Overtime"
        case .leave:
            return "Leave"
        case .offsite:
            return "Offsite Work Request"
        case .approval:
            return "Approval"
        case .performance:
            return "Performance"
        case .more:
            return "More"
        }
    }
    
    var imageName: String {
        switch self {
        case .offsite:
            return "ic_offsite"
        default:
            return "ic_\(name.lowercased())"
        }
    }
}
