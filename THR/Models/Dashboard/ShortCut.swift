//
//  ShortCut.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class ShortCut {
    var id: Int?
    var name: String?
    var image: UIImage?
    
    init(id: Int) {
        self.id = id
        name = EnumShortCut(rawValue: id)?.name
        image = UIImage(named: EnumShortCut(rawValue: id)?.imageName ?? "ic_more")
    }
}
