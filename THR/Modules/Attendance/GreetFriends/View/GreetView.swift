//
//  GreetView.swift
//  THR
//
//  Created by Apit Aprida on 14/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

protocol GreetViewDelegate {
    func passionButtonPressed()
    func loveButtonPressed()
    func dealButtonPressed()
    func likeButtonPressed()
    func smileButtonPressed()
}

class GreetView: UIView {

    @IBOutlet weak var passionButton: UIButton!
    @IBOutlet weak var passionImage: UIImageView!
    @IBOutlet weak var loveButton: UIButton!
    @IBOutlet weak var loveImage: UIImageView!
    @IBOutlet weak var dealButton: UIButton!
    @IBOutlet weak var dealImage: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var smileButton: UIButton!
    @IBOutlet weak var smileImage: UIImageView!

    var greetViewDelegate: GreetViewDelegate?
    
    var friend: Friend = Friend(json: nil) {
        didSet {
            let isHightlighted = friend.greetId != nil ? true : false
            switch friend.greetId {
            case .love:
                passionImage.isHighlighted = true
                loveImage.isHighlighted = false
                dealImage.isHighlighted = false
                likeImage.isHighlighted = false
                smileImage.isHighlighted = false
            case .chat:
                passionImage.isHighlighted = false
                loveImage.isHighlighted = true
                dealImage.isHighlighted = false
                likeImage.isHighlighted = false
                smileImage.isHighlighted = false
            case .handShake:
                passionImage.isHighlighted = false
                loveImage.isHighlighted = false
                dealImage.isHighlighted = true
                likeImage.isHighlighted = false
                smileImage.isHighlighted = false
            case .thumbsUp:
                passionImage.isHighlighted = false
                loveImage.isHighlighted = false
                dealImage.isHighlighted = false
                likeImage.isHighlighted = true
                smileImage.isHighlighted = false
            case .smile:
                passionImage.isHighlighted = false
                loveImage.isHighlighted = false
                dealImage.isHighlighted = false
                likeImage.isHighlighted = false
                smileImage.isHighlighted = true
            default:
                passionImage.isHighlighted = false
                loveImage.isHighlighted = false
                dealImage.isHighlighted = false
                likeImage.isHighlighted = false
                smileImage.isHighlighted = false
            }
        }
    }
    
    @IBAction func passionButtonPressed(_ sender: Any) {
        greetViewDelegate?.passionButtonPressed()
    }
    
    @IBAction func loveButtonPressed(_ sender: Any) {
        greetViewDelegate?.loveButtonPressed()
    }
    
    @IBAction func dealButtonPressed(_ sender: Any) {
        greetViewDelegate?.dealButtonPressed()
    }
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        greetViewDelegate?.likeButtonPressed()
    }
    
    @IBAction func smileButtonPressed(_ sender: Any) {
        greetViewDelegate?.smileButtonPressed()
    }
    
}
