//
//  OvertimeParamApi.swift
//  THR
//
//  Created by Apit Aprida on 08/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

class OvertimeParamApi {
    
    var date: String?
    var startTime: String?
    var endTime: String?
    var reason: String?
    
    func buildForParameters() -> [String: AnyObject] {
        var parameters: [String: AnyObject] = [:]
        parameters["overtime"] = ["date": date, "start_time": startTime, "end_time": endTime, "reason": reason] as AnyObject
        return parameters
    }
}
