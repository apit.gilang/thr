//
//  LoginPresenter.swift
//  THR
//
//  Created by Apit Aprida on 07/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol LoginViewPresenter: class {
    init(view: LoginView)
    func login(withUser user: String, password: String, companyCode: String)
}

protocol LoginView: class {
    func onSuccessLogin()
    func onSuccessSaveToken()
    func onErrorLogin(withMessage message: String)
}

class LoginPresenter: LoginViewPresenter {
    
    static func config(withLoginViewController vc: LoginViewController) {
        let presenter = LoginPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: LoginView
    
    required init(view: LoginView) {
        self.view = view
    }
    
    func login(withUser user: String, password: String, companyCode: String) {
        let parameter = LoginParamApi()
        parameter.username = user
        parameter.password = password
        parameter.companyCode = companyCode
        THRApi.instance.request(ApiLogin.login(param: parameter), success: { (json) in
            if let json = json {
                Settings.accessToken = json["token"].stringValue
                Settings.secretKey = json["secret_key"].stringValue
                Settings.userFullname = json["user"]["fullname"].string
                Settings.isRegisteredFace = json["user"]["data_train_frs"].boolValue
                self.saveFCMToken()
            } else {
                self.view.onErrorLogin(withMessage: "Unknown error")
            }
        }) { (error) in
            self.view.onErrorLogin(withMessage: error)
        }
    }
    
    func saveFCMToken() {
        THRApi.instance.request(ApiLogin.saveFcmToken) { (_) in
            self.view.onSuccessLogin()
        } error: { (error) in
            self.view.onErrorLogin(withMessage: error)
        }

    }
}
