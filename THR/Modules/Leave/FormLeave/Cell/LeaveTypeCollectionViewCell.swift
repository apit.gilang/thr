//
//  LeaveTypeCollectionViewCell.swift
//  THR
//
//  Created by Apit Aprida on 10/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class LeaveTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var roundedView: DesignableView!
    @IBOutlet weak var typeLabel: UILabel!
}
