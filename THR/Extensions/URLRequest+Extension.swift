//
//  URLRequest+Extension.swift
//  THR
//
//  Created by Apit Aprida on 07/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

extension URLRequest {
    mutating func thrHeaders(withToken token: String? = nil, secretKey: String? = nil) {
        if let token = token, let secretKey = secretKey {
            self.addValue("Content-Type", forHTTPHeaderField: "application/json")
            self.addValue(token, forHTTPHeaderField: ApiConstants.HttpHeader.token)
            self.addValue(secretKey, forHTTPHeaderField: ApiConstants.HttpHeader.secretKey)
        }
    }
}
