#!/bin/bash
APPFILE=$1
set -euo pipefail

# key is in ~/.appstoreconnect/private_keys
KEY="<~/.appstoreconnect/private_keys/AuthKey_39TRWWJ6UY.p8"
ISSUER="8d90931c-125d-423f-a67a-7794945a611b"
xcrun altool --upload-app --type ios --file $APPFILE --apiKey $KEY --apiIssuer $ISSUER
