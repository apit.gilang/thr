//
//  CreateTodoParamApi.swift
//  THR
//
//  Created by Apit Aprida on 16/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

class CreateTodoParamApi {
    
    var toDo: String?
    var isRemind: Bool?
    var date: String?
    var clock: String?
    
    func buildForParameters() -> [String: AnyObject] {
        var parameters: [String: AnyObject] = [:]
        parameters["to_do_list"] = [
            "to_do": toDo as AnyObject,
            "is_remind": isRemind as AnyObject,
            "date": date as AnyObject,
            "clock": clock as AnyObject
        ] as AnyObject
        return parameters
    }
}
