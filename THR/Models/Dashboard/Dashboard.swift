//
//  Dashboard.swift
//  THR
//
//  Created by Apit Aprida on 29/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Dashboard {
    var workingHour: WorkingHour?
    var shortcuts: [ShortCut] = []
    var performances: [Performance] = []
    var todoLists: [TodoList] = []
    var leave: Leave?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        let data = json["data"]
        workingHour = WorkingHour(json: data["attendance"])
        leave = Leave(json: data["leave"])
        
        for item in data["favorite_app"].arrayValue {
            shortcuts.append(ShortCut(id: item.intValue))
        }
        
        for statistic in data["performances"].arrayValue {
            performances.append(Performance(json: statistic))
        }
        
        for todo in data["to_do_lists"].arrayValue {
            todoLists.append(TodoList(json: todo))
        }
    }
    
    func getFilteredShortcut() -> [ShortCut] {
        var allShortCuts: [ShortCut] = []
        for id in 1..<7 {
            allShortCuts.append(ShortCut(id: id))
        }
        for allItem in allShortCuts {
            for apiItem in shortcuts {
                if allItem.id == apiItem.id {
                    let indexItem = allShortCuts.firstIndex { (item) -> Bool in
                        item.id == apiItem.id
                    }
                    guard let index = indexItem else {
                        return []
                    }
                    allShortCuts.remove(at: index)
                }
            }
        }
        return allShortCuts
    }
}
