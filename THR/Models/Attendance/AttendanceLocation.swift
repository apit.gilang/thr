//
//  AttendanceLocation.swift
//  THR
//
//  Created by Apit Aprida on 08/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class AttendanceLocation {
    var id: Int?
    var date: String?
    var checkin: String?
    var checkout: String?
    var locationCheckIn: [Location] = []
    var locationCheckOut: [Location] = []
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        let jsonData = json["data"]
        id = jsonData["id"].int
        date = jsonData["date"].string
        checkin = jsonData["checkin"].string
        checkout = jsonData["checkout"].string
        for data in jsonData["location"]["checkin"].arrayValue {
            locationCheckIn.append(Location(json: data))
        }
        for data in jsonData["location"]["checkout"].arrayValue {
            locationCheckOut.append(Location(json: data))
        }
    }
    
    func isCheckOut() -> Bool {
        if let startTime = checkin, let endTime = checkout {
            if (startTime.contains("--:--") && endTime.contains("--:--")) || (startTime.contains("00:00:00") && endTime.contains("00:00:00")) {
                return false
            } else if (!startTime.contains("--:--") && endTime.contains("--:--")) || !startTime.contains("00:00:00") && endTime.contains("00:00:00") {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
}
