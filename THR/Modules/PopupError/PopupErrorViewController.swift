//
//  PopupErrorViewController.swift
//  THR
//
//  Created by Apit Aprida on 05/08/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantPopupError {
    // TODO: Add segue ids
}

// MARK: -  PopupErrorViewController

class PopupErrorViewController: ViewController {
    
    // MARK: Properties
    
    var presenter: PopupErrorPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        PopupErrorPresenter.config(withPopupErrorViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - PopupErrorView

extension PopupErrorViewController: PopupErrorView {
    // TODO: implement view methods
}
