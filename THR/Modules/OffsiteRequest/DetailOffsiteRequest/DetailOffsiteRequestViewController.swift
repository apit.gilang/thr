//
//  DetailOffsiteRequestViewController.swift
//  THR
//
//  Created by Apit Aprida on 19/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantDetailOffsiteRequest {
    // TODO: Add segue ids
}

// MARK: -  DetailOffsiteRequestViewController

class DetailOffsiteRequestViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reasonLabel: UITextView!
    
    var presenter: DetailOffsiteRequestPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DetailOffsiteRequestPresenter.config(withDetailOffsiteRequestViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - DetailOffsiteRequestView

extension DetailOffsiteRequestViewController: DetailOffsiteRequestView {
    // TODO: implement view methods
}
