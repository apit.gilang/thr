//
//  ChangePasswordPresenter.swift
//  THR
//
//  Created by Apit Aprida on 29/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ChangePasswordViewPresenter: class {
    init(view: ChangePasswordView)
    func saveNewPassword(withParameter parameter: ChangePasswordParamApi)
}

protocol ChangePasswordView: class {
    func onSuccessChangePassword()
    func onErrorChangePassword(withMessage message: String)
}

class ChangePasswordPresenter: ChangePasswordViewPresenter {
    
    static func config(withChangePasswordViewController vc: ChangePasswordViewController) {
        let presenter = ChangePasswordPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: ChangePasswordView
    
    required init(view: ChangePasswordView) {
        self.view = view
    }
    
    func saveNewPassword(withParameter parameter: ChangePasswordParamApi) {
        THRApi.instance.request(ApiProfile.changePassword(parameter: parameter), success: { (json) in
            self.view.onSuccessChangePassword()
        }) { (error) in
            self.view.onErrorChangePassword(withMessage: error)
        }
    }
}
