//
//  SortByPresenter.swift
//  THR
//
//  Created by Apit Aprida on 12/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SortByViewPresenter: class {
    init(view: SortByView)
    // TODO: Declare view presenter methods
}

protocol SortByView: class {
    // TODO: Declare view methods
}

class SortByPresenter: SortByViewPresenter {
    
    static func config(withSortByViewController vc: SortByViewController) {
        let presenter = SortByPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: SortByView
    
    required init(view: SortByView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
