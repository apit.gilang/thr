//
//  OvertimeViewController.swift
//  THR
//
//  Created by Apit Aprida on 06/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantOvertime {
    static let presentToFormLeave = "presentToFormLeave"
    static let presentToSortBy = "presentToSortBy"
    static let presentToDetail = "presentToDetail"
}

// MARK: -  OvertimeViewController

class OvertimeViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var changeViewButton: UIButton!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var sortByButton: UIButton!
    
    var presenter: OvertimePresenter!
    var isGridView = false
    private var sortedOvertimes: [OvertimeData] = []
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        OvertimePresenter.config(withOvertimeViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.registerCells(withViewController: self)
        setupSearchButton()
        collectionView.isHidden = true
        presenter.getOvertime(isShowToast: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstantOvertime.presentToFormLeave == segue.identifier {
            if let vc = segue.destination as? FormLeaveViewController {
                vc.source = .overtime
                vc.formLeaveDelegae = self
            }
        } else if SegueConstantOvertime.presentToSortBy == segue.identifier {
            if let vc = segue.destination as? SortByViewController {
                vc.sortByDelegate = self
            }
        } else if SegueConstantOvertime.presentToDetail == segue.identifier {
            if let vc = segue.destination as? DetailApprovalViewController {
                if let overtime = sender as? OvertimeData {
                    vc.overtime = overtime
                }
            }
        }
    }
    
    @IBAction func changeViewButtonPressed(_ sender: Any) {
        isGridView = !isGridView
        changeViewButton.setImage(isGridView ? UIImage(named: "ic_list") : UIImage(named: "ic_grid"), for: .normal)
        collectionView.isHidden = !isGridView
        tableView.isHidden = isGridView
        collectionView.reloadData()
    }
    
    @IBAction func plusButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: SegueConstantOvertime.presentToFormLeave, sender: nil)
    }
    
    @IBAction func sortByButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: SegueConstantOvertime.presentToSortBy, sender: nil)
    }
    
    fileprivate func setupSearchButton() {
        let searchImage = UIImage(named: "ic_search")?.withRenderingMode(.alwaysOriginal)
        let searchBarItem = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handleSearch))
        navigationItem.setRightBarButton(searchBarItem, animated: false)
    }
    
    @objc func handleSearch() {
        
    }
    
    private func setupView(isEmptyData: Bool) {
        if isEmptyData {
            tableView.isHidden = true
            collectionView.isHidden = true
            emptyView.isHidden = false
        } else {
            collectionView.isHidden = !isGridView
            tableView.isHidden = isGridView
            emptyView.isHidden = true
            
            tableView.reloadData()
            collectionView.reloadData()
            collectionView.layoutIfNeeded()
        }
    }
}

extension OvertimeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedOvertimes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RequestAttendanceTableViewCell.self), for: indexPath) as! RequestAttendanceTableViewCell
        DispatchQueue.main.async {
            cell.setupContentOvertime(withData: self.sortedOvertimes[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueConstantOvertime.presentToDetail, sender: sortedOvertimes[indexPath.row])
    }
}

extension OvertimeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sortedOvertimes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RequestAttendanceCollectionViewCell.self), for: indexPath) as! RequestAttendanceCollectionViewCell
        DispatchQueue.main.async {
            cell.setupContentOvertime(withData: self.sortedOvertimes[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width / 2
        return CGSize(width: width - 28, height: width + 25)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueConstantOvertime.presentToDetail, sender: sortedOvertimes[indexPath.row])
    }
}

extension OvertimeViewController: SortByDelegate {
    func didSelectSort(withSort sort: SortBy) {
        sortByButton.setTitle("\(sort.name ?? "") by me", for: .normal)
        
        guard let overtime = presenter.overtime else {
            return
        }
        sortedOvertimes.removeAll()
        if sort.id == "submit" {
            sortedOvertimes = overtime.overtimes
        } else if sort.id == "date" {
            sortedOvertimes = overtime.sortedLastDate
        } else if sort.id == "pending" {
            sortedOvertimes = overtime.sortedPending
        } else if sort.id == "approve" {
            sortedOvertimes = overtime.sortedApprove
        } else if sort.id == "reject" {
            sortedOvertimes = overtime.sortedReject
        } else {
            sortedOvertimes = []
        }
        
        setupView(isEmptyData: sortedOvertimes.count == 0)
    }
}

extension OvertimeViewController: FormLeaveDelegate {
    func didFinishedRequest() {
        presenter.getOvertime(isShowToast: true)
    }
}

// MARK: - OvertimeView

extension OvertimeViewController: OvertimeView {
    func onSuccessGetOvertime(isShowToast: Bool) {
        if let overtimes = presenter.overtime?.overtimes {
            sortedOvertimes = overtimes
            setupView(isEmptyData: overtimes.count == 0)
            if isShowToast {
                showToast(message: "Add Overtime Success")
            }
        }
    }
    
    func onErrorGetOvertime(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
