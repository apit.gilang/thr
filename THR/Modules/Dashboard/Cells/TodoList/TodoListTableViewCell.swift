//
//  TodoListTableViewCell.swift
//  GreenHcm
//
//  Created by Apit Aprida on 12/03/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class TodoListTableViewCell: UITableViewCell {

    @IBOutlet weak var todoView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var todoNoteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
