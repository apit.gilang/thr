//
//  CustomTabbarViewController.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/28/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import ESTabBarController_swift
import CoreLocation

// MARK: - SegueConstants

enum SegueConstantCustomTabbar {
    static let pushToAttendance = "pushToAttendance"
    static let pushToEditProfile = "pushToEditProfile"
    static let pushToNotification = "pushToNotification"
}

// MARK: -  CustomTabbarViewController

class CustomTabbarViewController: ESTabBarController, UITabBarControllerDelegate {
    
    // MARK: Properties
    let attendanceIndex = 1
    let profileIndex = 2
    
    var presenter: CustomTabbarPresenter!
    let locationManager = CLLocationManager()
    var latitude: Double?
    var longitude: Double?
    var notificationBarItem = UIBarButtonItem()
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CustomTabbarPresenter.config(withCustomTabbarViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        setupLocation()
        presenter.setupCustomTabbar(withController: self)
        presenter.setupDashboardNavigation(withController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(didReceivedWorkingHour(_:)), name: .didReceivedWorkingHour, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didReceivedNotificationCount(_:)), name: .didReceivedNotificationCount, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationBarItem.removeBadge()
        NotificationCenter.default.removeObserver(self, name: .didReceivedWorkingHour, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didReceivedNotificationCount, object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstantCustomTabbar.pushToAttendance == segue.identifier {
            if let vc = segue.destination as? AttendanceViewController {
                vc.locationManager = locationManager
            }
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if tabBarController.selectedIndex == attendanceIndex {
            navigationController?.navigationBar.barTintColor = UIColor.appColor(.blue)
            navigationController?.navigationBar.barStyle = .black
            navigationController?.navigationBar.isTranslucent = false
            tabBarController.selectedIndex = 0
            performSegue(withIdentifier: SegueConstantCustomTabbar.pushToAttendance, sender: nil)
        } else if tabBarController.selectedIndex == profileIndex {
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.navigationBar.isTranslucent = true
            title = "Profile"
            
            let settingImage = UIImage(named: "ic_setting")?.withRenderingMode(.alwaysOriginal)
            let settingBarItem = UIBarButtonItem(image: settingImage, style: .plain, target: self, action: #selector(handleSetting))
            navigationItem.setRightBarButton(settingBarItem, animated: false)
            navigationItem.setLeftBarButton(UIBarButtonItem(image: UIImage(), style: .plain, target: nil, action: nil), animated: false)
        } else {
            title = ""
            let notificationImage = UIImage(named: "ic_notification")?.withRenderingMode(.alwaysOriginal)
            notificationBarItem = UIBarButtonItem(image: notificationImage, style: .plain, target: self, action: #selector(handlingNotification))
            let iconImage = UIImage(named: "ic_thr_white")?.withRenderingMode(.alwaysOriginal)
            let imageView = UIImageView(image: iconImage)
            imageView.frame = CGRect(x: 0, y: 0, width: 84, height: 20)
            let iconBarItem = UIBarButtonItem(customView: imageView)
            navigationItem.setLeftBarButton(iconBarItem, animated: false)
            navigationItem.setRightBarButton(notificationBarItem, animated: false)
            presenter.setupDashboardNavigation(withController: self)
        }
    }
    
    @objc func didReceivedWorkingHour(_ notification: Notification) {
        if let workingHour = notification.object as? AttendanceLocation {
            self.viewControllers?[attendanceIndex].tabBarItem.title = workingHour.isCheckOut() ? "Clock Out" : "Clock In"
        }
    }
    
    @objc func didReceivedNotificationCount(_ notification: Notification) {
        if let notifCount = notification.object as? Int {
            if notifCount > 0 {
                notificationBarItem.addBadge(number: notifCount)
            } else {
                notificationBarItem.removeBadge()
            }
        }
    }
    
    private func setupLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
}

extension CustomTabbarViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        latitude = location.latitude
        longitude = location.longitude
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
}

// MARK: - CustomTabbarView

extension CustomTabbarViewController: CustomTabbarView {
    @objc func handlingNotification() {
        notificationBarItem.removeBadge()
        performSegue(withIdentifier: SegueConstantCustomTabbar.pushToNotification, sender: nil)
    }
    
    @objc func handleSetting() {
        notificationBarItem.removeBadge()
        performSegue(withIdentifier: SegueConstantCustomTabbar.pushToEditProfile, sender: nil)
    }
}
