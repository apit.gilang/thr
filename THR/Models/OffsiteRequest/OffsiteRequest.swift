//
//  OffsiteRequest.swift
//  THR
//
//  Created by Apit Aprida on 18/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class OffsiteRequest {
    var totalPage: Int?
    var currentPage: Int?
    var offsiteAttendances: [OffsiteRequestData] = []
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        totalPage = json["total_page"].int
        currentPage = json["current_page"].int
        for data in json["offsite_attendances"].arrayValue {
            offsiteAttendances.append(OffsiteRequestData(json: data))
        }
    }
}

struct OffsiteRequestData {
    var id: Int?
    var date: String?
    var reason: String?
    var latitude: Double?
    var longtitude: Double?
    var is_lock_location: Bool?
    var address_name: String?
    var status: EnumStatusApprovalType?
    var company_id: Int?
    var user_id: Int?
    var radius: Double?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        date = json["date"].string
        reason = json["reason"].string
        latitude = json["latitude"].double
        longtitude = json["longtitude"].double
        is_lock_location = json["is_lock_location"].bool
        address_name = json["address_name"].string
        status = EnumStatusApprovalType(rawValue: json["status"].intValue)
        company_id = json["company_id"].int
        user_id = json["user_id"].int
        radius = json["radius"].double
    }
    
    func getDateToShow() -> String {
        var result = ""
        if let start = Date(fromString: date ?? "", format: .isoDate){
            result = "\(start.toString(dateStyle: .medium, timeStyle: .none))"
        }
        return result
    }
}
