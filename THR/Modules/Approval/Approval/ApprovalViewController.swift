//
//  ApprovalViewController.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantApproval {
    static let presentToDetailApproval = "presentToDetailApproval"
}

// MARK: -  ApprovalViewController

class ApprovalViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyDataView: UIView!
    @IBOutlet weak var filterView: DesignableView!
    
    var presenter: ApprovalPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ApprovalPresenter.config(withApprovalViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.getApproval(withType: .all)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstantApproval.presentToDetailApproval == segue.identifier {
            if let vc = segue.destination as? DetailApprovalViewController {
                if let approval = sender as? ApprovalData {
                    vc.approval = approval
                    vc.delegate = self
                }
            }
        }
    }
    
    @IBAction func sortButtonPressed(_ sender: Any) {
    }
    
    @IBAction func filterButtonPressed(_ sender: Any) {
    }
}

extension ApprovalViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.approval?.approvals.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ApprovalTableViewCell.self), for: indexPath) as! ApprovalTableViewCell
        if let approval = presenter.approval?.approvals[indexPath.row] {
            DispatchQueue.main.async {
                cell.setupContentApproval(withApproval: approval)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 134
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueConstantApproval.presentToDetailApproval, sender: presenter.approval?.approvals[indexPath.row])
    }
}

extension ApprovalViewController: DetailApprovalDelegate {
    func approvalSucceded() {
        // todo update data
    }
}

// MARK: - ApprovalView

extension ApprovalViewController: ApprovalView {
    func updateView() {
        if let approvals = presenter.approval?.approvals, approvals.count > 0 {
            tableView.reloadData()
        } else {
            tableView.isHidden = true
            filterView.isHidden = true
            emptyDataView.isHidden = false
        }
    }
    
    func onSuccessGetApproval() {
        if let approvals = presenter.approval?.approvals, approvals.count > 0 {
            tableView.reloadData()
        } else {
            tableView.isHidden = true
            filterView.isHidden = true
            emptyDataView.isHidden = false
        }
    }
    
    func onErrorGetApproval(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
