//
//  AttendancePresenter.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/28/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol AttendanceViewPresenter: class {
    init(view: AttendanceView)
    func getLocation()
}

protocol AttendanceView: class {
    func onSuccessGetLocation()
    func onErrorGetLocation(withMessage message: String)
}

class AttendancePresenter: AttendanceViewPresenter {
    
    static func config(withAttendanceViewController vc: AttendanceViewController) {
        let presenter = AttendancePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: AttendanceView
    
    var attendanceLocation: AttendanceLocation?
    
    required init(view: AttendanceView) {
        self.view = view
    }
    
    func getLocation() {
        THRApi.instance.request(ApiAttendance.getAttendance, success: { (json) in
            self.attendanceLocation = AttendanceLocation(json: json)
            self.view.onSuccessGetLocation()
        }) { (error) in
            self.view.onErrorGetLocation(withMessage: error)
        }
    }
}
