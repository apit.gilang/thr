//
//  LeaveType.swift
//  THR
//
//  Created by Apit Aprida on 10/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

struct LeaveType {
    var name: String?
    var id: Int?
    var permittedLeave: Int?
    var isAttachment: Bool?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        name = json["name"].string
        id = json["id"].int
        permittedLeave = json["permitted_leave"].int
        isAttachment = json["is_attachment"].bool
    }
}
