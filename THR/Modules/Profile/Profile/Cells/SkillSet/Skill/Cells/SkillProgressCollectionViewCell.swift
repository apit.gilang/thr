//
//  SkillProgressCollectionViewCell.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class SkillProgressCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var progressIndicatorBar: MBCircularProgressBarView!
    @IBOutlet weak var skillLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
