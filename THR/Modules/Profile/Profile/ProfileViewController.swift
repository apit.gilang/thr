//
//  ProfileViewController.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/28/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantProfile {
    // TODO: Add segue ids
}

enum EnumProfileSection: String {
    case headline
    case about
    case history
    case skill
    case contact
    case resume
}

// MARK: -  ProfileViewController

class ProfileViewController: ViewController {
    
    let sections: [EnumProfileSection] = [.headline, .about, .history, .skill, .contact, /*.resume*/]
    
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: ProfilePresenter!
    var isExpands = [false, false, false, false, false, false]
    var totalCollectionHeight: CGFloat = 0
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ProfilePresenter.config(withProfileViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.registerCell(withProfileViewController: self)
        presenter.getProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl.addTarget(self, action: #selector(reloadProfile), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc func reloadProfile() {
        presenter.getProfile()
    }
}

extension ProfileViewController {
    fileprivate func setupExpandingCell(_ indexPath: IndexPath, _ tableView: UITableView) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderProfileTableViewCell.self), for: indexPath) as! HeaderProfileTableViewCell
            cell.setupContent(withSection: sections[indexPath.section])
            if isExpands[indexPath.section] {
                cell.arrowDownButton.setImage(UIImage(named: "ic_edit"), for: .normal)
            } else {
                cell.arrowDownButton.setImage(UIImage(named: "ic_arrow_down"), for: .normal)
            }
            return cell
        } else {
            switch sections[indexPath.section] {
            case .about:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AboutTableViewCell.self), for: indexPath) as! AboutTableViewCell
                if let user = presenter.profile?.user {
                    cell.setupAbout(withData: user)
                }
                return cell
            case .skill:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SkillSetTableViewCell.self), for: indexPath) as! SkillSetTableViewCell
                if let profile = presenter.profile {
                    cell.layoutIfNeeded()
                    cell.totalCollectionHeight = totalCollectionHeight
                    cell.profile = profile
                    cell.tableView.reloadData()
                }
                return cell
            case .contact:
                if indexPath.row == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AboutTableViewCell.self), for: indexPath) as! AboutTableViewCell
                    if let user = presenter.profile?.user {
                        cell.setupContact(withData: user)
                    }
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SocialMediaTableViewCell.self), for: indexPath) as! SocialMediaTableViewCell
                    return cell
                }
            case .history:
                let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HistoryTableViewCell.self), for: indexPath) as! HistoryTableViewCell
                if let histories = presenter.profile?.histories {
                    cell.setupContentHistory(withHistory: histories[indexPath.row - 1])
                }
                return cell
            default:
                return UITableViewCell()
            }
        }
    }
    
    fileprivate func estimatedHeght(_ indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableView.automaticDimension
        } else {
            let languageCount: CGFloat = 3
            let estimatedLanguageHeight: CGFloat = 120
            
            let itemPerRowSkillCount = 3
            let collectionCellHeight: CGFloat = 90
            let skillCount = presenter.profile?.skills.count ?? 0
            var estimateRow: CGFloat = 0
            var spaceToBottom: CGFloat = 10
            for i in 0..<skillCount {
                if i % itemPerRowSkillCount == 0 {
                    estimateRow += 1
                    spaceToBottom += 10
                }
            }
            let headerSkilllHeight: CGFloat = 44 * 2
            let estimateSkillCollectionHeight: CGFloat = collectionCellHeight * estimateRow
            totalCollectionHeight = estimateSkillCollectionHeight + headerSkilllHeight + spaceToBottom
            return (languageCount * estimatedLanguageHeight) + totalCollectionHeight
        }
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isExpands[section] {
            switch sections[section] {
            case .history:
                if presenter.profile?.histories.count ?? 0 > 0 {
                    return presenter.profile?.histories.count ?? 0 + 1
                } else {
                    return 1
                }
            case .contact:
                return 3
            case .skill:
                guard let profile = presenter.profile else { return 1 }
                if profile.languages.count > 0 || profile.skills.count > 0 {
                    return 2
                } else {
                    return 1
                }
            case .headline, .resume:
                return 1
            case .about:
                return 2
            }
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .headline:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeadlineTableViewCell.self), for: indexPath) as! HeadlineTableViewCell
            if let user = presenter.profile?.user {
                cell.setupHeadline(withData: user)
            }
            return cell
        case .resume:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ResumeButtonTableViewCell.self), for: indexPath) as! ResumeButtonTableViewCell
            return cell
        default:
            return setupExpandingCell(indexPath, tableView)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch sections[indexPath.section] {
        case .resume:
            return 120
        default:
            if isExpands[indexPath.section] && sections[indexPath.section] == .skill {
                return estimatedHeght(indexPath)
            }
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            isExpands[indexPath.section] = !isExpands[indexPath.section]
            tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
        }
    }
}

// MARK: - ProfileView

extension ProfileViewController: ProfileView {
    func updateView() {
        tableView.reloadData()
    }
    func onSuccessGetProfile() {
        refreshControl.endRefreshing()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func onErrorGetProfile(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
