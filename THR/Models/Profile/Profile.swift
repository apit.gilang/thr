//
//  Profile.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Profile {
    var user: User?
    var contact: Contact?
    var socialMedia: SocialMedia?
    var addresses: [Address] = []
    var histories: [History] = []
    var skills: [Skill] = []
    var languages: [Language] = []
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        user = User(json: json["user"])
        socialMedia = SocialMedia(json: json["social_media"])
        contact = Contact(json: json["contact"])
        
        for address in json["address"].arrayValue {
            addresses.append(Address(json: address))
        }
        
        for history in json["historical"].arrayValue {
            histories.append(History(json: history))
        }
        
        for skill in json["skill"].arrayValue {
            skills.append(Skill(json: skill))
        }
        
        for language in json["language"].arrayValue {
            languages.append(Language(json: language))
        }
    }
}
