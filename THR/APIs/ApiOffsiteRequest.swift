//
//  ApiOffsiteRequest.swift
//  THR
//
//  Created by Apit Aprida on 18/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiOffsiteRequest: URLRequestConvertible {
    
    case getOffsiteRequest
    case submitOffsiteRequest(parameter: OffsiteRequestParamApi)
    
    var path: String {
        return "/offsite_attendances"
    }
    
    var method: HTTPMethod {
        switch self {
        case .submitOffsiteRequest:
            return .post
        default:
            return .get
        }
    }
    
    var parameter: [String: AnyObject] {
        switch self {
        case .submitOffsiteRequest(let param):
            return param.buildForParameters()
        default:
            return [:]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.thrHeaders(withToken: Settings.accessToken, secretKey: Settings.secretKey)
        return try HTTPMethod.get.rawValue == urlRequest.httpMethod ? URLEncoding.default.encode(urlRequest, with: self.parameter) : JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
