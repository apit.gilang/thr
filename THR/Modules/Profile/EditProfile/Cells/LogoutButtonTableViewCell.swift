//
//  LogoutButtonTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 29/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

protocol LogoutButtonDelegate {
    func didLogout()
}

class LogoutButtonTableViewCell: UITableViewCell {

    var logoutButtonDelegate: LogoutButtonDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    @IBAction func logoutButtonPressed(_ sender: Any) {
        logoutButtonDelegate?.didLogout()
    }
    
}
