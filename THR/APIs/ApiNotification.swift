//
//  ApiNotification.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/20/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiNotification: URLRequestConvertible {
    
    case getNotification
    case updateNotification(id: Int)
    
    var path: String {
        switch self {
        case .updateNotification(let id):
            return "/notifications/\(id)/update_read"
        default:
            return "/notifications"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .updateNotification:
            return .put
        default:
            return .get
        }
    }
    
    var parameter: [String: AnyObject] {
        return [:]
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.thrHeaders(withToken: Settings.accessToken, secretKey: Settings.secretKey)
        return try HTTPMethod.get.rawValue == urlRequest.httpMethod ? URLEncoding.default.encode(urlRequest, with: self.parameter) : JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
