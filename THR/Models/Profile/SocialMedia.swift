//
//  SocialMedia.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class SocialMedia {
    var id: Int?
    var userId: Int?
    var facebook: String?
    var linkedin: String?
    var twitter: String?
    var path: String?
    var instagram: String?
    var facebookId: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        userId = json["user_id"].int
        facebook = json["facebook"].string
        linkedin = json["linkedin"].string
        twitter = json["twitter"].string
        path = json["path"].string
        instagram = json["instagram"].string
        facebookId = json["facebook_id"].string
    }
}
