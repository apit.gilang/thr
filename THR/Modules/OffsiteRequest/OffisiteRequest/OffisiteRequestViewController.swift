//
//  OffisiteRequestViewController.swift
//  THR
//
//  Created by Apit Aprida on 06/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantOffisiteRequest {
    static let presentToFormOffsite = "presentToFormOffsite"
    static let presentToDetail = "presentToDetail"
}

// MARK: -  OffisiteRequestViewController

class OffisiteRequestViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    
    var presenter: OffisiteRequestPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        OffisiteRequestPresenter.config(withOffisiteRequestViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: String(describing: RequestAttendanceTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: RequestAttendanceTableViewCell.self))
        presenter.getOffsiteRequest(isShowToast: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstantOffisiteRequest.presentToFormOffsite == segue.identifier {
            if let vc = segue.destination as? FormOffsiteViewController {
                vc.formOffsiteDelegate = self
            }
        } else if SegueConstantOffisiteRequest.presentToDetail == segue.identifier {
            if let vc = segue.destination as? DetailApprovalViewController {
                if let offsite = sender as? OffsiteRequestData {
                    vc.offsite = offsite
                }
            }
        }
    }
    
    private func setupView(isEmptyData: Bool) {
        if isEmptyData {
            tableView.isHidden = true
            emptyView.isHidden = false
        } else {
            tableView.isHidden = false
            emptyView.isHidden = true
            tableView.reloadData()
        }
    }
}

extension OffisiteRequestViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.offsiteRequest?.offsiteAttendances.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RequestAttendanceTableViewCell.self), for: indexPath) as! RequestAttendanceTableViewCell
        if let data = presenter.offsiteRequest?.offsiteAttendances {
            DispatchQueue.main.async {
                cell.setupContentOffsite(withData: data[indexPath.row])
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueConstantLeave.presentToDetail, sender: presenter.offsiteRequest?.offsiteAttendances[indexPath.row])
    }
}

extension OffisiteRequestViewController: FormOffsiteRequestDelegate {
    func didFinishedRequest() {
        presenter.getOffsiteRequest(isShowToast: true)
    }
}

// MARK: - OffisiteRequestView

extension OffisiteRequestViewController: OffisiteRequestView {
    func onSuccessGetOffsiteRequest(isShowToast: Bool) {
        if let offsite = presenter.offsiteRequest?.offsiteAttendances {
            setupView(isEmptyData: offsite.count == 0)
            if isShowToast {
                showToast(message: "Add Offsite Attendance Success")
            }
        }
    }
    
    func onErrorGetOffsiteRequest(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
