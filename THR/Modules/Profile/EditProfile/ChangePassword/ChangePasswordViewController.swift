//
//  ChangePasswordViewController.swift
//  THR
//
//  Created by Apit Aprida on 29/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantChangePassword {
    // TODO: Add segue ids
}

// MARK: -  ChangePasswordViewController

class ChangePasswordViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var oldPasswordField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    
    var presenter: ChangePasswordPresenter!
    var parameter = ChangePasswordParamApi()
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ChangePasswordPresenter.config(withChangePasswordViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        oldPasswordField.delegate = self
        newPasswordField.delegate = self
        confirmPasswordField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        presenter.saveNewPassword(withParameter: parameter)
    }
}

extension ChangePasswordViewController {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == oldPasswordField && textField.text?.isEmptyAfterTrim == false {
            parameter.currentPassword = textField.text
        }
        if textField == newPasswordField && textField.text?.isEmptyAfterTrim == false {
            parameter.password = textField.text
        }
        if textField == confirmPasswordField && textField.text?.isEmptyAfterTrim == false {
            parameter.passwordConfirmation = textField.text
        }
    }
}

// MARK: - ChangePasswordView

extension ChangePasswordViewController: ChangePasswordView {
    func onSuccessChangePassword() {
        navigationController?.popViewController(animated: true)
    }
    
    func onErrorChangePassword(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
