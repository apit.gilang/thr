//
//  OvertimePresenter.swift
//  THR
//
//  Created by Apit Aprida on 06/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol OvertimeViewPresenter: class {
    init(view: OvertimeView)
    // Setup View
    func registerCells(withViewController vc: OvertimeViewController)
    // Setup Data
    func getOvertime(isShowToast: Bool)
}

protocol OvertimeView: class {
    func onSuccessGetOvertime(isShowToast: Bool)
    func onErrorGetOvertime(withMessage message: String)
}

class OvertimePresenter: OvertimeViewPresenter {
    
    static func config(withOvertimeViewController vc: OvertimeViewController) {
        let presenter = OvertimePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: OvertimeView
    var overtime: Overtime?
    
    required init(view: OvertimeView) {
        self.view = view
    }
    
//  Setup View
    func registerCells(withViewController vc: OvertimeViewController) {
        vc.tableView.register(UINib(nibName: String(describing: RequestAttendanceTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: RequestAttendanceTableViewCell.self))
        vc.collectionView.register(UINib(nibName: String(describing: RequestAttendanceCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: RequestAttendanceCollectionViewCell.self))
    }
    
//  Setup Data
    func getOvertime(isShowToast: Bool) {
        THRApi.instance.request(ApiOvertime.getOvertime, success: { (json) in
            self.overtime = Overtime(json: json)
            self.view.onSuccessGetOvertime(isShowToast: isShowToast)
        }) { (error) in
            self.view.onErrorGetOvertime(withMessage: error)
        }
    }
}
