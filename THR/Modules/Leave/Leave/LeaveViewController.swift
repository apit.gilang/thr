//
//  LeaveViewController.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantLeave {
    static let presentToFormLeave = "presentToFormLeave"
    static let presentToSortBy = "presentToSortBy"
    static let presentToDetail = "presentToDetail"
}

// MARK: -  LeaveViewController

class LeaveViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var changeViewButton: UIButton!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var sortByButton: UIButton!
    
    var presenter: LeavePresenter!
    var isGridView = false
    var sortedLeaves: [LeaveData] = []
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        LeavePresenter.config(withLeaveViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchButton()
        presenter.registerCells(withViewController: self)
        collectionView.isHidden = true
        presenter.getLeave(isShowToast: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstantLeave.presentToFormLeave == segue.identifier {
            if let vc = segue.destination as? FormLeaveViewController {
                vc.source = .leave
                vc.formLeaveDelegae = self
            }
        } else if SegueConstantLeave.presentToSortBy == segue.identifier {
            if let vc = segue.destination as? SortByViewController {
                vc.sortByDelegate = self
            }
        } else if SegueConstantOvertime.presentToDetail == segue.identifier {
            if let vc = segue.destination as? DetailApprovalViewController {
                if let leave = sender as? LeaveData {
                    vc.leave = leave
                }
            }
        }
    }
    
    @IBAction func plusButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: SegueConstantLeave.presentToFormLeave, sender: nil)
    }
    
    @IBAction func changeViewButtonPressed(_ sender: Any) {
        isGridView = !isGridView
        changeViewButton.setImage(isGridView ? UIImage(named: "ic_list") : UIImage(named: "ic_grid"), for: .normal)
        collectionView.isHidden = !isGridView
        tableView.isHidden = isGridView
        collectionView.reloadData()
    }
    
    @IBAction func sortByButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: SegueConstantLeave.presentToSortBy, sender: nil)
    }
    
    fileprivate func setupSearchButton() {
        let searchImage = UIImage(named: "ic_search")?.withRenderingMode(.alwaysOriginal)
        let searchBarItem = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handleSearch))
        navigationItem.setRightBarButton(searchBarItem, animated: false)
    }
    
    @objc func handleSearch() {
        
    }
    
    private func setupView(isEmptyData: Bool) {
        if isEmptyData {
            tableView.isHidden = true
            collectionView.isHidden = true
            emptyView.isHidden = false
        } else {
            collectionView.isHidden = !isGridView
            tableView.isHidden = isGridView
            emptyView.isHidden = true
            
            tableView.reloadData()
            collectionView.reloadData()
            collectionView.layoutIfNeeded()
        }
    }
}

extension LeaveViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedLeaves.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RequestAttendanceTableViewCell.self), for: indexPath) as! RequestAttendanceTableViewCell
        DispatchQueue.main.async {
            cell.setupContentLeave(withData: self.sortedLeaves[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueConstantLeave.presentToDetail, sender: sortedLeaves[indexPath.row])
    }
}

extension LeaveViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.leave?.leaves.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RequestAttendanceCollectionViewCell.self), for: indexPath) as! RequestAttendanceCollectionViewCell
        DispatchQueue.main.async {
            cell.setupContentLeave(withData: self.sortedLeaves[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width / 2
        return CGSize(width: width - 28, height: width + 25)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueConstantLeave.presentToDetail, sender: sortedLeaves[indexPath.row])
    }
}

extension LeaveViewController: SortByDelegate {
    func didSelectSort(withSort sort: SortBy) {
        sortByButton.setTitle("\(sort.name ?? "") by me", for: .normal)
        guard let leave = presenter.leave else {
            return
        }
        if sort.id == "submit" {
            sortedLeaves = leave.leaves
        } else if sort.id == "date" {
            sortedLeaves = leave.sortedLastDate
        } else if sort.id == "pending" {
            sortedLeaves = leave.sortedPending
        } else if sort.id == "approve" {
            sortedLeaves = leave.sortedApprove
        } else if sort.id == "reject" {
            sortedLeaves = leave.sortedReject
        } else {
            sortedLeaves = []
        }
        
        setupView(isEmptyData: sortedLeaves.count == 0)
    }
}

extension LeaveViewController: FormLeaveDelegate {
    func didFinishedRequest() {
        presenter.getLeave(isShowToast: true)
    }
}

// MARK: - LeaveView

extension LeaveViewController: LeaveView {
    func onSuccessGetLeave(isShowToast: Bool) {
        if let leaves = presenter.leave?.leaves {
            sortedLeaves = leaves
            setupView(isEmptyData: leaves.count == 0)
            if isShowToast {
                showToast(message: "Add Leave Success")
            }
        }
    }
    
    func onErrorGetLeave(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
