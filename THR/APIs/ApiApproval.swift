//
//  ApiApproval.swift
//  THR
//
//  Created by Apit Aprida on 16/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiApproval: URLRequestConvertible {
    
    case getApproval(type: EnumApprovalType)
    case submitApproval(id: Int, type: EnumStatusApprovalType)
    
    var path: String {
        switch self {
        case .submitApproval(let id, _):
            return "/approvals/\(id)"
        default:
            return "/approvals"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .submitApproval:
            return .put
        default:
            return .get
        }
    }
    
    var parameter: [String: AnyObject] {
        switch self {
        case .getApproval(let type):
            return ["type": type.typeName as AnyObject]
        case .submitApproval(_, let type):
            return ["status": type.rawValue as AnyObject]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.thrHeaders(withToken: Settings.accessToken, secretKey: Settings.secretKey)
        return try HTTPMethod.get.rawValue == urlRequest.httpMethod ? URLEncoding.default.encode(urlRequest, with: self.parameter) : JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
