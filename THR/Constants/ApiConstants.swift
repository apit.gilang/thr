//
//  ApiConstants.swift
//  THR
//
//  Created by Apit Aprida on 30/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

struct ApiConstants {
//    static let BASE_URL = "https://app.thr.co.id/api/v1"
    static let BASE_URL = "https://app.epployee.id/api/v1"
    
    struct GoogleMaps {
        static let apiKey = "AIzaSyBHlCvUCJoATyW4IDaCRpzz03cFxRAHln8"
    }
    
    struct HttpHeader {
        static let token = "token"
        static let secretKey = "secret-key"
    }
}
