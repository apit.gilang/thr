//
//  FaceScanningViewController.swift
//  THR
//
//  Created by Apit Aprida on 30/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import CoreLocation
import CoreImage
import AssistantKit

// MARK: - SegueConstants

enum SegueConstantFaceScanning {
    // TODO: Add segue ids
    static let pushToConfirmation = "pushToConfirmation"
}

// MARK: - FaceScanningViewController

class FaceScanningViewController: ViewController {

    // MARK: Properties
    @IBOutlet weak var cameraView: DesignableView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var absenceInButton: UIButton!
    @IBOutlet weak var separatorAbsenceButton: UIView!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var centerCameraViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var captureButtonView: UIView!
    @IBOutlet weak var cancelButtonView: UIView!
    
    var captureSession =  AVCaptureSession()
    var currentCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    var image: UIImage?

    var presenter: FaceScanningPresenter!
    var parameter = AttendanceParamApi()
    var absenceData: AttendanceLocation?
    var isRegisteringFace = false

    // MARK: Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        FaceScanningPresenter.config(withFaceScanningViewController: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
        setupPreviewLayer()
        startRunningCaptureSession()
        if Device.screen == .inches_4_0 {
            centerCameraViewConstraint.constant = -50
        }  else if Device.screen == .inches_4_7 {
            centerCameraViewConstraint.constant = -30
        }else {
            centerCameraViewConstraint.constant = 0
        }
        
        if isRegisteringFace {
            absenceInButton.isHidden = true
            cancelButtonView.isHidden = true
            separatorAbsenceButton.isHidden = true
        }
        
        UIView.animate(withDuration: 1.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: [.repeat], animations: {
            self.animationView.transform = CGAffineTransform(scaleX: 2, y: 2)
        }) { (_) in
            UIView.animate(withDuration: 0.5) {
                self.animationView.transform = .identity
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueConstantFaceScanning.pushToConfirmation {
            if let vc = segue.destination as? ConfirmationViewController {
                vc.parameter = parameter
                vc.absenceData = absenceData
            }
        }
    }

    @IBAction func caputeBtnPressed(_ sender: UIButton) {
        capturePhoto()
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func capturePhoto() {
        let setting = AVCapturePhotoSettings()
        self.photoOutput?.capturePhoto(with: setting, delegate: self)
    }
}

extension FaceScanningViewController {
    private func setupCaptureSession() {
       captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }

    private func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.front)
        let devices = deviceDiscoverySession.devices

        for device in devices {
            if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
        }

        currentCamera = frontCamera
    }

    private func setupInputOutput() {
        do {
            guard let camera = currentCamera else { return }
            let captureDeviceInput = try AVCaptureDeviceInput(device: camera)
            captureSession.addInput(captureDeviceInput)
            photoOutput = AVCapturePhotoOutput()
            if #available(iOS 11.0, *) {
                photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
                captureSession.addOutput(photoOutput!)
            } else {
                // Fallback on earlier versions
            }
        } catch {
            print(error)
        }
    }

    private func setupPreviewLayer() {
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        let rootLayer: CALayer = cameraView.layer
        rootLayer.masksToBounds = true
        cameraPreviewLayer?.frame = rootLayer.bounds
        rootLayer.addSublayer(cameraPreviewLayer!)
    }

    private func startRunningCaptureSession() {
        captureSession.startRunning()
    }
    
    func detectFace(withImage image: UIImage) {
        let personciImage = CIImage(cgImage: image.cgImage!)
        let options: [String: Any] = [CIDetectorAccuracy: CIDetectorAccuracyHigh, CIDetectorSmile: true, CIDetectorEyeBlink: true, CIDetectorImageOrientation: NSNumber(value: 5) as NSNumber]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: options)
        let faces = faceDetector?.features(in: personciImage, options: options)

        if let face = faces?.first as? CIFaceFeature {
            print("found bounds are \(face.bounds)")

            if face.hasSmile {
                print("face is smiling")
            }

            if face.hasLeftEyePosition {
                print("Left eye bounds are \(face.leftEyePosition)")
            }

            if face.hasRightEyePosition {
                print("Right eye bounds are \(face.rightEyePosition)")
            }
            parameter.image = compressedImage(withImage: image)
            let bcf = ByteCountFormatter()
            bcf.allowedUnits = [.useKB]
            bcf.countStyle = .file
            let string = bcf.string(fromByteCount: Int64(parameter.image?.count ?? 0))
            print("actual size of image in KB: \(string)")
            if isRegisteringFace {
                guard let image = compressedImage(withImage: image) else {
                    return
                }
                presenter.registerFace(withImage: image)
            } else {
                presenter.requestAttendance(withParamter: parameter, isCheckout: absenceData?.isCheckOut() ?? false)
            }
        } else {
            animationView.layer.removeAllAnimations()
            let tryAgain = UIAlertAction(title: "Try again", style: .default) { (_) in
                self.captureSession.startRunning()
            }
            alert.showAlert("No Face!", message: "No face was detected", actions: [tryAgain], controller: self)
        }
    }
    
    func compressedImage(withImage image: UIImage) -> Data? {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = 500
        let maxWidth: Float = 500
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        
        // Compress image size
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        let compressedImageData = resizedImage!.mediumQualityJPEGNSData
        UIGraphicsEndImageContext()
        return compressedImageData
    }
}

// MARK: - FaceScanningView

extension FaceScanningViewController: FaceScanningView {
    func onSuccessRequestLeave() {
        animationView.layer.removeAllAnimations()
        performSegue(withIdentifier: SegueConstantFaceScanning.pushToConfirmation, sender: nil)
    }
    
    func onSuccessRegisterFace() {
        animationView.layer.removeAllAnimations()
        performSegue(withIdentifier: SegueConstantFaceScanning.pushToConfirmation, sender: nil)
    }
    
    func onErrorRequestAPI(withMessage message: String) {
        showPageError(withMessage: message)
    }
}

extension FaceScanningViewController: AVCapturePhotoCaptureDelegate {
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        captureSession.stopRunning()
        if let imageData = photo.fileDataRepresentation() {
            image = UIImage(data: imageData)
            detectFace(withImage: image ?? UIImage())
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        AudioServicesDisposeSystemSoundID(1108)
    }
}
