//
//  RequestAttendanceCollectionViewCell.swift
//  THR
//
//  Created by Apit Aprida on 06/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class RequestAttendanceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var statusView: DesignableView!
    @IBOutlet weak var statusLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupContentOvertime(withData data: OvertimeData) {
        timeLabel.text = data.getTimeToShow()
        reasonLabel.text = data.reason
        statusView.backgroundColor = data.status?.statusColor
        statusLabel.text = data.status?.statusName
    }
    
    func setupContentLeave(withData data: LeaveData) {
        timeLabel.text = data.getDateToShow()
        reasonLabel.text = data.leaveType
        statusView.backgroundColor = data.status?.statusColor
        statusLabel.text = data.status?.statusName
    }
}
