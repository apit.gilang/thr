//
//  Language.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Language {
    var id: Int?
    var user_id: Int?
    var name: String?
    var reading: Int?
    var writing: Int?
    var speaking: Int?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        user_id = json["user_id"].int
        name = json["name"].string
        reading = json["reading"].int
        writing = json["writing"].int
        speaking = json["speaking"].int
    }
    
    func getReadingProgress() -> Float {
        let value = Double(reading ?? 0)
        return Float(value / 10.0)
    }
    
    func getWrittingProgress() -> Float {
        let value = Double(writing ?? 0)
        return Float(value / 10.0)
    }
    
    func getSpeakingProgress() -> Float {
        let value = Double(speaking ?? 0)
        return Float(value / 10.0)
    }
}
