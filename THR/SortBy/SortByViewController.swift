//
//  SortByViewController.swift
//  THR
//
//  Created by Apit Aprida on 12/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import BottomPopup

// MARK: - SegueConstants

enum SegueConstantSortBy {
    // TODO: Add segue ids
}

struct SortBy {
    var id: String?
    var name: String?
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}

protocol SortByDelegate {
    func didSelectSort(withSort sort: SortBy)
}

// MARK: -  SortByViewController

class SortByViewController: BottomPopupViewController {
    
    let sortBy: [SortBy] = [
                            SortBy(id: "submit", name: "Last Submitted"),
                            SortBy(id: "date", name: "Last Date"),
                            SortBy(id: "pending", name: "Last Status (Pending)"),
                            SortBy(id: "approve", name: "Last Status (Approve)"),
                            SortBy(id: "reject", name: "Last Status (Reject)")]
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: SortByPresenter!
    var sortByDelegate: SortByDelegate?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        SortByPresenter.config(withSortByViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var popupTopCornerRadius: CGFloat {
        return 20
    }
    
    override var popupHeight: CGFloat {
        return UIScreen.main.bounds.size.height * 0.5
    }
}

extension SortByViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortBy.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SortByTableViewCell.self), for: indexPath) as! SortByTableViewCell
        cell.sortTitleLabel.text = sortBy[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            self.sortByDelegate?.didSelectSort(withSort: self.sortBy[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

// MARK: - SortByView

extension SortByViewController: SortByView {
    // TODO: implement view methods
}
