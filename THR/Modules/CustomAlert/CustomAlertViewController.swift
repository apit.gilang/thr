//
//  CustomAlertViewController.swift
//  THR
//
//  Created by Apit Aprida on 29/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantCustomAlert {
    static let pushToFaceScan = "pushToFaceScan"
}

// MARK: -  CustomAlertViewController

class CustomAlertViewController: ViewController {
    
    // MARK: Properties
    
    var presenter: CustomAlertPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CustomAlertPresenter.config(withCustomAlertViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? FaceScanningViewController {
            vc.isRegisteringFace = true
        }
    }
    
    @IBAction func startButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: SegueConstantCustomAlert.pushToFaceScan, sender: nil)
    }
}

// MARK: - CustomAlertView

extension CustomAlertViewController: CustomAlertView {
    // TODO: implement view methods
}
