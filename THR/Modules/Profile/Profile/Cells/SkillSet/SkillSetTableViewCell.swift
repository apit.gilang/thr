//
//  SkillSetTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

enum EnumSkillSet: String {
    case language, skill
}

class SkillSetTableViewCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    var sections: [EnumSkillSet] = [.language, .skill]
    var profile: Profile?
    var totalCollectionHeight: CGFloat = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: String(describing: SkillTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SkillTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: LanguageTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: LanguageTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: HeaderSkillTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: HeaderSkillTableViewCell.self))
    }
}

extension SkillSetTableViewCell: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .language:
            return profile?.languages.count ?? 0
        default:
            return profile?.skills.count ?? 0 > 0 ? 1 : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .language:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LanguageTableViewCell.self), for: indexPath) as! LanguageTableViewCell
            if let language = profile?.languages[indexPath.row] {
                cell.nameLabel.text = language.name
                cell.readProgress.progress = language.getReadingProgress()
                cell.speakProgress.progress = language.getSpeakingProgress()
                cell.writeProgress.progress = language.getWrittingProgress()
            }
            return cell
        case .skill:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SkillTableViewCell.self), for: indexPath) as! SkillTableViewCell
            if let skills = profile?.skills {
                cell.skills = skills
                cell.collectionView.layoutIfNeeded()
                cell.collectionView.reloadData()
                cell.collectionViewHeight.constant = totalCollectionHeight
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderSkillTableViewCell.self)) as! HeaderSkillTableViewCell
        cell.skillTitleLabel.text = sections[section].rawValue.capitalized
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch sections[section] {
        case .language:
            return profile?.languages.count ?? 0 > 0 ? 44 : 0
        default:
            return profile?.skills.count ?? 0 > 0 ? 44 : 0
        }
    }
}
