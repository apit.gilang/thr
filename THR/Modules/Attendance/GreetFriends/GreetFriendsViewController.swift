//
//  GreetFriendsViewController.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/30/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import Popover
import AssistantKit

// MARK: - SegueConstants

enum SegueConstantGreetFriends {
    static let pushToFinished = "pushToFinished"
}

// MARK: -  GreetFriendsViewController

class GreetFriendsViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter: GreetFriendsPresenter!
    var greetView = GreetView()
    var selectedIndex = -1
    let popOver = Popover()
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        GreetFriendsPresenter.config(withGreetFriendsViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.getFriends()
        greetView = Bundle.main.loadNibNamed(String(describing: GreetView.self), owner: self, options: nil)?.first as! GreetView
        greetView.greetViewDelegate = self
        popOver.didDismissHandler = {
            self.collectionView.reloadItems(at: [IndexPath(item: self.selectedIndex, section: 0)])
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        navigationItem.setHidesBackButton(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        let filterFriends = presenter.friends.filter({ friend -> Bool in
            friend.greetId != nil
        })
        let param = GreetFriendsParamApi()
        param.friends = filterFriends
        presenter.greetFriends(parameter: param)
    }
}

extension GreetFriendsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.friends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FriendsCollectionViewCell.self), for: indexPath) as! FriendsCollectionViewCell
        let friend = presenter.friends[indexPath.row]
        cell.friendName.text = friend.fullname
        if let image = friend.image, let url = URL(string: image) {
            DispatchQueue.main.async {
                cell.friendImage.sd_setImage(with: url, placeholderImage: UIImage(named: "ic_friend_avatar"))
                cell.friendImage.layer.cornerRadius = cell.friendImage.frame.size.width / 2
            }
        }
        cell.greetTypeIcon.image = friend.greetId?.icon
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 3
        switch Device.screen {
        case .inches_4_0:
            return CGSize(width: width - 16, height: width + 16)
        case .inches_4_7, .inches_5_5:
            return CGSize(width: width - 16, height: width + 8)
        default:
            return CGSize(width: width - 16, height: width)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! FriendsCollectionViewCell
        selectedIndex = indexPath.row
        greetView.frame = CGRect(x: 0, y: 0, width: 100, height: 130)
        greetView.friend = presenter.friends[indexPath.row]
        if selectedIndex == indexPath.row && view.contains(greetView) {
            popOver.dismiss()
        } else {
            popOver.show(greetView, fromView: cell.friendImage)
        }
    }
}

extension GreetFriendsViewController: GreetViewDelegate {
    func passionButtonPressed() {
        presenter.friends[selectedIndex].greetId = .love
        greetView.friend = presenter.friends[selectedIndex]
    }
    
    func loveButtonPressed() {
        presenter.friends[selectedIndex].greetId = .chat
        greetView.friend = presenter.friends[selectedIndex]
    }
    
    func dealButtonPressed() {
        presenter.friends[selectedIndex].greetId = .handShake
        greetView.friend = presenter.friends[selectedIndex]
    }
    
    func likeButtonPressed() {
        presenter.friends[selectedIndex].greetId = .thumbsUp
        greetView.friend = presenter.friends[selectedIndex]
    }
    
    func smileButtonPressed() {
        presenter.friends[selectedIndex].greetId = .smile
        greetView.friend = presenter.friends[selectedIndex]
    }
}

// MARK: - GreetFriendsView

extension GreetFriendsViewController: GreetFriendsView {
    func onSuccessGetFriends() {
        collectionView.reloadData()
    }
    
    func onSuccessGreetFriends() {
        performSegue(withIdentifier: SegueConstantGreetFriends.pushToFinished, sender: nil)
    }
    
    func onErrorGetFriends(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
