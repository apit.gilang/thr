//
//  ConfirmationViewController.swift
//  THR
//
//  Created by Apit Aprida on 30/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import AssistantKit

// MARK: - SegueConstants

enum SegueConstantConfirmation {
    static let pushToGreetFriends = "pushToGreetFriends"
    static let pushToBehaviour = "pushToBehaviour"
}

// MARK: -  ConfirmationViewController

class ConfirmationViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var centerPreviewImageConstant: NSLayoutConstraint!
    @IBOutlet weak var spaceNameConstant: NSLayoutConstraint!
    @IBOutlet weak var resultImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var registrationConfirmationView: UIView!
    @IBOutlet weak var registrationResultImage: UIImageView!
    @IBOutlet weak var registrationNameLabel: UILabel!
    @IBOutlet weak var topSpaceStackView: NSLayoutConstraint!
    @IBOutlet weak var bottomSpaceStackView: NSLayoutConstraint!
    
    var presenter: ConfirmationPresenter!
    var parameter = AttendanceParamApi()
    var absenceData: AttendanceLocation?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ConfirmationPresenter.config(withConfirmationViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let data = parameter.image, let image = UIImage(data: data) {
            registrationResultImage.image = image
        }
        if !Settings.isRegisteredFace {
            registrationConfirmationView.isHidden = false
        }
        if let data = parameter.image, let image = UIImage(data: data) {
            resultImage.image = image
        }
        if let name = Settings.userFullname {
            let names = name.split(separator: " ")
            nameLabel.text = "Hello \(names.first ?? "")"
            registrationNameLabel.text = "Hello \(names.first ?? "")"
        } else {
            nameLabel.text = "Hello"
            registrationNameLabel.text = "Hello"
        }
        if Device.screen == .inches_4_0 {
            centerPreviewImageConstant.constant = -50
            topSpaceStackView.constant = 52
            bottomSpaceStackView.constant = 20
//            spaceNameConstant.constant = 20
        } else if Device.screen == .inches_4_7 {
            centerPreviewImageConstant.constant = -30
            topSpaceStackView.constant = 62
            bottomSpaceStackView.constant = 30
//            spaceNameConstant.constant = 40
        } else {
            centerPreviewImageConstant.constant = 0
            topSpaceStackView.constant = 72
            bottomSpaceStackView.constant = 42
//            spaceNameConstant.constant = 60
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstantConfirmation.pushToBehaviour == segue.identifier {
            if let vc = segue.destination as? BehaviourViewController {
                vc.absenceData = absenceData
            }
        }
    }
    
    @IBAction func registrationNextButtonPressed(_ sender: Any) {
        Settings.isRegisteredFace = true
        let storyboard = UIStoryboard(name: "CustomTabbar", bundle: nil)
        let rootView = storyboard.instantiateViewController(withIdentifier: "CustomTabbarViewController") as! CustomTabbarViewController
        appDelegate.window?.rootViewController = UINavigationController(rootViewController: rootView)
        appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        if absenceData?.isCheckOut() == true {
            performSegue(withIdentifier: SegueConstantConfirmation.pushToBehaviour, sender: nil)
        } else {
            performSegue(withIdentifier: SegueConstantConfirmation.pushToGreetFriends, sender: nil)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - ConfirmationView

extension ConfirmationViewController: ConfirmationView {
    func onSuccessRequestLeave() {
        if absenceData?.isCheckOut() == true {
            performSegue(withIdentifier: SegueConstantConfirmation.pushToBehaviour, sender: nil)
        } else {
            performSegue(withIdentifier: SegueConstantConfirmation.pushToGreetFriends, sender: nil)
        }
    }
    
    func onErrorRequestAPI(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
