//
//  LeaveParamApi.swift
//  THR
//
//  Created by Apit Aprida on 10/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

class LeaveParamApi {
    
     var leaveTypeId: Int?
     var userInputLeave: String?
     var startDate: String?
     var endDate: String?
     var reason: String?
     var fileUpload: Data?
}
