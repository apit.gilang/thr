//
//  NotificationTableViewCell.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/9/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var notifImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupNotificationView(notification: NotificationData) {
        nameLabel.text = notification.fromUserFullname
        detailsLabel.text = notification.message
        if let image = notification.fromUserImage, let url = URL(string: image) {
            DispatchQueue.main.async {
                self.notifImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "img_no_data"))
                self.notifImageView.layer.cornerRadius = self.notifImageView.frame.size.width / 2
            }
        }
    }
}
