//
//  TodoList.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class TodoList {
    var id: Int?
    var todo: String?
    var isRemind: Bool?
    var date: String?
    var clock: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        todo = json["to_do"].string
        isRemind = json["is_remind"].bool
        date = json["date"].string
        clock = json["clock"].string
    }
}
