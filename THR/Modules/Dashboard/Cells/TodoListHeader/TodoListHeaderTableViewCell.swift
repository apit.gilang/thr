//
//  TodoListHeaderTableViewCell.swift
//  GreenHcm
//
//  Created by Apit Aprida on 12/03/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

protocol TodoListHeaderDelegate {
    func didPressCreateTodoList()
}

class TodoListHeaderTableViewCell: UITableViewCell {

    var delegate: TodoListHeaderDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onCreateButtonPressed(_ sender: Any) {
        delegate?.didPressCreateTodoList()
    }
}
