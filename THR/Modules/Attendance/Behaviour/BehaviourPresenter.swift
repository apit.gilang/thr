//
//  BehaviourPresenter.swift
//  THR
//
//  Created by Apit Aprida on 01/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol BehaviourViewPresenter: class {
    init(view: BehaviourView)
    func submitBehaviour(withParameter parameter: BehaviourParamApi)
}

protocol BehaviourView: class {
    func onSuccessSubmitBehaviour()
    func onErrorSubmitBehaviour(withMessage message: String)
}

class BehaviourPresenter: BehaviourViewPresenter {
    
    static func config(withBehaviourViewController vc: BehaviourViewController) {
        let presenter = BehaviourPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: BehaviourView
    
    required init(view: BehaviourView) {
        self.view = view
    }
    
    func negativeArr() -> [Feels] {

        let ex = Feels(name: "Exhausted", isSelected: false)
        let ash = Feels(name: "ashamed", isSelected: false)
        let tir = Feels(name: "tired", isSelected: false)
        let dem = Feels(name: "demoralized", isSelected: false)
        let dis = Feels(name: "disgusted", isSelected: false)
        let je = Feels(name: "jealous", isSelected: false)
        let eg = Feels(name: "egocentric", isSelected: false)
        let ax = Feels(name: "acxious", isSelected: false)

        let arrData = [ex, ash, tir, dem, dis, je, eg, ax]

        return arrData

    }

    func positiveArr() -> [Feels] {

        let hap = Feels(name: "happy", isSelected: false)
        let enj = Feels(name: "enjoy", isSelected: false)
        let lov = Feels(name: "love", isSelected: false)
        let fan = Feels(name: "fantastic", isSelected: false)
        let mag = Feels(name: "magnificient", isSelected: false)
        let unb = Feels(name: "unbeatable", isSelected: false)
        let pls = Feels(name: "pleased", isSelected: false)
        let chr = Feels(name: "cheerful", isSelected: false)
        let sat = Feels(name: "satidfied", isSelected: false)

        let arrData = [hap, enj, lov, fan, mag, unb, pls, chr, sat]

        return arrData

    }
    
    func submitBehaviour(withParameter parameter: BehaviourParamApi) {
        THRApi.instance.request(ApiAttendance.submitBehaviour(parameter: parameter), success: { (json) in
            self.view.onSuccessSubmitBehaviour()
        }) { (error) in
            self.view.onErrorSubmitBehaviour(withMessage: error)
        }
    }
}
