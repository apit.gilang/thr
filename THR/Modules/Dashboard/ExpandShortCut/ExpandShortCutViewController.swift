//
//  ExpandShortCutViewController.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/29/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import BottomPopup

// MARK: - SegueConstants

enum SegueConstantExpandShortCut {
    // TODO: Add segue ids
}

// MARK: -  ExpandShortCutViewController

protocol ExpandShortCutDelegate {
    func didSelectItem(withShortCut shortCut: ShortCut)
}

class ExpandShortCutViewController: BottomPopupViewController {
    
    // MARK: Properties
    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter: ExpandShortCutPresenter!
    var cornerRadius: CGFloat?
    var shortCuts: [ShortCut] = []
    var delegate: ExpandShortCutDelegate?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ExpandShortCutPresenter.config(withExpandShortCutViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: String(describing: ExpandShortCutCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ExpandShortCutCollectionViewCell.self))
    }
    
    override var popupTopCornerRadius: CGFloat {
        return 50
    }
    
    override var popupHeight: CGFloat {
        return UIScreen.main.bounds.size.height * 0.6
    }
}

extension ExpandShortCutViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shortCuts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ExpandShortCutCollectionViewCell.self), for: indexPath) as! ExpandShortCutCollectionViewCell
        cell.setupContentShortCut(withShortCut: shortCuts[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 3
        let height: CGFloat = width + 20
        return CGSize(width: width - 16, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dismiss(animated: false) {
            self.delegate?.didSelectItem(withShortCut: self.shortCuts[indexPath.row])
        }
    }
}

// MARK: - ExpandShortCutView

extension ExpandShortCutViewController: ExpandShortCutView {
    // TODO: implement view methods
}
