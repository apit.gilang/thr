//
//  UIColor+Extension.swift
//  THR
//
//  Created by Apit Aprida on 29/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

enum AssetColor {
    case blue
    case lightBlue
    case darkGray
    case lightGray
    case orange
    case yellow
    case cyan
    case whiteBlue
    case red
}

extension UIColor {
    static func appColor(_ name: AssetColor) -> UIColor {
        switch name {
        case .blue:
            return UIColor(named: "blue")!
        case .lightBlue:
            return UIColor(named: "lightBlue")!
        case .darkGray:
            return UIColor(named: "darkGray")!
        case .lightGray:
            return UIColor(named: "lightGray")!
        case .orange:
            return UIColor(named: "orange")!
        case .yellow:
            return UIColor(named: "yellow")!
        case .cyan:
            return UIColor(named: "cyan")!
        case .whiteBlue:
            return UIColor(named: "whiteBlue")!
        case .red:
            return UIColor(named: "red")!
        }
    }
}
