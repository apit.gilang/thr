//
//  ViewController.swift
//  THR
//
//  Created by Apit Aprida on 28/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ViewController: UIViewController, UITextFieldDelegate {

    var alert = AlertHelper()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func showToast(message: String) {
        let gifImageView = UIImageView()
        gifImageView.image = UIImage.gifImageWithName("ic_success")
        let toastLabel = UILabel()
        let font = UIFont.systemFont(ofSize: 16)
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 20
        toastLabel.clipsToBounds  =  true
        let sizeMessage = message.size(withAttributes: [NSAttributedString.Key.font: font])
        view.addSubview(gifImageView)
        view.addSubview(toastLabel)
        gifImageView.translatesAutoresizingMaskIntoConstraints = false
        gifImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        gifImageView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        gifImageView.centerXAnchor.constraint(equalToSystemSpacingAfter: view.centerXAnchor, multiplier: 1).isActive = true
        gifImageView.centerYAnchor.constraint(equalToSystemSpacingBelow: view.centerYAnchor, multiplier: 1).isActive = true
        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        toastLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        toastLabel.widthAnchor.constraint(equalToConstant: sizeMessage.width + 20).isActive = true
        toastLabel.centerXAnchor.constraint(equalToSystemSpacingAfter: view.centerXAnchor, multiplier: 1).isActive = true
        toastLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        UIView.animate(withDuration: 5.0, delay: 0.5, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
            gifImageView.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
            gifImageView.removeFromSuperview()
        })
    }
    
    func showPageError(withMessage message: String) {
        UIApplication.shared.endIgnoringInteractionEvents()
        let errorVc = UIStoryboard(name: "PageError", bundle: nil).instantiateViewController(withIdentifier: "PageErrorViewController") as! PageErrorViewController
        errorVc.modalPresentationStyle = .fullScreen
        errorVc.message = message
        appDelegate.window?.makeKeyAndVisible()
        appDelegate.window?.rootViewController?.present(errorVc, animated: true, completion: nil)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}

