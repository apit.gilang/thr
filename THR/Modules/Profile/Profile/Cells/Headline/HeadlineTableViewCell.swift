//
//  AboutTableViewCell.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/1/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class HeadlineTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userFullnameLabel: UILabel!
    @IBOutlet weak var userAddressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func setupHeadline(withData data: User) {
        userFullnameLabel.text = data.fullname
        userAddressLabel.text = data.identityCardAddress
        if let image = data.imageMedium, let url = URL(string: image) {
            DispatchQueue.main.async {
                self.userImage.sd_setImage(with: url, placeholderImage: UIImage(named: "img_no_data"))
                self.userImage.layer.cornerRadius = self.userImage.frame.size.width / 2
            }
        }
    }
    
}
