//
//  ExpandShortCutPresenter.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/29/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ExpandShortCutViewPresenter: class {
    init(view: ExpandShortCutView)
    // TODO: Declare view presenter methods
}

protocol ExpandShortCutView: class {
    // TODO: Declare view methods
}

class ExpandShortCutPresenter: ExpandShortCutViewPresenter {
    
    static func config(withExpandShortCutViewController vc: ExpandShortCutViewController) {
        let presenter = ExpandShortCutPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: ExpandShortCutView
    
    required init(view: ExpandShortCutView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
