//
//  AttendanceViewController.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/28/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import CoreLocation
import AssistantKit

// MARK: - SegueConstants

enum SegueConstantAttendance {
    static let pushToFaceScanning = "pushToFaceScanning"
}

// MARK: -  AttendanceViewController

class AttendanceViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var startTimeValueLabel: UILabel!
    @IBOutlet weak var endTimeValueLabel: UILabel!
    @IBOutlet weak var checkInLabel: UILabel!
    
    var presenter: AttendancePresenter!
    var locationManager = CLLocationManager()
    var marker: GMSMarker!
    var parameter = AttendanceParamApi()
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        AttendancePresenter.config(withAttendanceViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.getLocation()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstantAttendance.pushToFaceScanning == segue.identifier {
            if let vc = segue.destination as? FaceScanningViewController {
                vc.parameter = parameter
                vc.absenceData = presenter.attendanceLocation
            }
        }
    }
    
    @IBAction func checkInButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: SegueConstantAttendance.pushToFaceScanning, sender: nil)
    }
    
    func reverseGeocode(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            self.addressLabel.text = lines.joined(separator: "\n")
            
            let labelHeight = self.addressView.frame.size.height
            var topInset: CGFloat = 0
            if #available(iOS 11.0, *) {
                topInset = self.mapView.safeAreaInsets.top
            } else {
                // Fallback on earlier versions
            }
            self.mapView.padding = UIEdgeInsets(
                top: topInset,
                left: 0,
                bottom: labelHeight,
                right: 0)
            
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func drawCircle(position: CLLocationCoordinate2D, radius: Double) {
        let circle = GMSCircle(position: position, radius: radius)
        circle.strokeColor = UIColor.appColor(.blue)
        circle.fillColor = UIColor.appColor(.lightBlue).withAlphaComponent(0.5)
        circle.map = mapView
    }
    
    fileprivate func setupMapView() {
        addressView.addShadow(location: .top, color: .darkGray, opacity: 0.2, radius: 6)
        locationManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        // Draw circle for checkin location
        if let location = presenter.attendanceLocation {
            if location.isCheckOut() {
                for location in location.locationCheckOut {
                    let position = CLLocationCoordinate2D(latitude: location.latitude ?? 0, longitude: location.longtitude ?? 0)
                    drawCircle(position: position, radius: location.radius ?? 0)
                }
                checkInLabel.text = "Clock Out"
            } else {
                for location in location.locationCheckIn {
                    let position = CLLocationCoordinate2D(latitude: location.latitude ?? 0, longitude: location.longtitude ?? 0)
                    drawCircle(position: position, radius: location.radius ?? 0)
                }
                checkInLabel.text = "Clock In"
            }
            startTimeValueLabel.text = location.checkin
            endTimeValueLabel.text = location.checkout
        }
    }
}

extension AttendanceViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        marker = GMSMarker(position: position)
        marker.map = mapView
        marker.icon = UIImage(named: "ic_attendance_location")
        reverseGeocode(coordinate: position)
        parameter.latitude = location.coordinate.latitude
        parameter.longtitude = location.coordinate.longitude
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

// MARK: - AttendanceView

extension AttendanceViewController: AttendanceView {
    func onSuccessGetLocation() {
        parameter.id = presenter.attendanceLocation?.id
        setupMapView()
    }
    
    func onErrorGetLocation(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
