//
//  ProfilePresenter.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/28/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ProfileViewPresenter: class {
    init(view: ProfileView)
    // Setup data
    func parseDummyData()
    func getProfile()
    // Setup view
    func registerCell(withProfileViewController vc: ProfileViewController)
}

protocol ProfileView: class {
    func updateView()
    func onSuccessGetProfile()
    func onErrorGetProfile(withMessage message: String)
}

class ProfilePresenter: ProfileViewPresenter {
    
    static func config(withProfileViewController vc: ProfileViewController) {
        let presenter = ProfilePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: ProfileView
    
    var profile: Profile?
    
    required init(view: ProfileView) {
        self.view = view
    }
    
// MARK: Setup data
    func getProfile() {
        THRApi.instance.request(ApiProfile.getProfile, success: { (json) in
            self.profile = Profile(json: json)
            self.view.onSuccessGetProfile()
        }) { (error) in
            self.view.onErrorGetProfile(withMessage: error)
        }
    }
    
    func parseDummyData() {
        if let path = Bundle.main.path(forResource: "profile", ofType: "json") {
            let jsonData = NSData(contentsOfFile: path)
            self.profile = try! Profile(json: JSON(data: jsonData! as Data))
            self.view.updateView()
        }
    }
    
// MARK: Setup view
    
    func registerCell(withProfileViewController vc: ProfileViewController) {
        vc.tableView.backgroundColor = .clear
        vc.tableView.register(UINib(nibName: String(describing: HeadlineTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: HeadlineTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: HeaderProfileTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: HeaderProfileTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: ResumeButtonTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ResumeButtonTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: AboutTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: AboutTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: HistoryTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: HistoryTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: SkillSetTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SkillSetTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: SocialMediaTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SocialMediaTableViewCell.self))
    }
}
