//
//  ApprovalPresenter.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ApprovalViewPresenter: class {
    init(view: ApprovalView)
// MARK: Setup data
    func parseDummyData()
    func getApproval(withType type: EnumApprovalType)
}

protocol ApprovalView: class {
    func updateView()
    func onSuccessGetApproval()
    func onErrorGetApproval(withMessage message: String)
}

class ApprovalPresenter: ApprovalViewPresenter {
    
    static func config(withApprovalViewController vc: ApprovalViewController) {
        let presenter = ApprovalPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: ApprovalView
    var approval: Approval?
    
    required init(view: ApprovalView) {
        self.view = view
    }
    
// MARK: Setup data
    func parseDummyData() {
        if let path = Bundle.main.path(forResource: "approval", ofType: "json") {
            let jsonData = NSData(contentsOfFile: path)
            self.approval = try! Approval(json: JSON(data: jsonData! as Data))
            self.view.updateView()
        }
    }
    
    func getApproval(withType type: EnumApprovalType) {
        THRApi.instance.request(ApiApproval.getApproval(type: type), success: { (json) in
            self.approval = Approval(json: json)
            self.view.onSuccessGetApproval()
        }) { (error) in
            self.view.onErrorGetApproval(withMessage: error)
        }
    }
}
