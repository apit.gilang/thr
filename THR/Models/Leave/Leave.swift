//
//  Leave.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Leave {
    var available: Bool?
    var taken: Int?
    var left: Int?
    var qouta: Int?
    var percent: Int?
    var color: String?
    var startDate: String?
    var endDate: String?
    var currentPage: Int?
    var totalPage: Int?
    var leaves: [LeaveData] = []
    
    var sortedLastDate: [LeaveData] {
        return leaves.reversed()
    }
    
    var sortedPending: [LeaveData] {
        return sortedLastDate.sorted(by: { s1, _ -> Bool in
            guard let status1 = s1.status?.rawValue else { return false }
            return status1 == 1
        })
    }
    
    var sortedReject: [LeaveData] {
        return sortedLastDate.filter { (overtime) -> Bool in
            overtime.status?.rawValue == 3
        }
    }
    
    var sortedApprove: [LeaveData] {
        return sortedLastDate.filter { (overtime) -> Bool in
            overtime.status?.rawValue == 2
        }
    }
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        available = json["available"].bool
        taken = json["taken"].int
        left = json["left"].int
        qouta = json["qouta"].int
        percent = json["percent"].int
        color = json["color"].string
        startDate = json["start_date"].string
        endDate = json["end_date"].string
        currentPage = json["current_page"].int
        totalPage = json["total_page"].int
        
        for leave in json["leaves"].arrayValue {
            leaves.append(LeaveData(json: leave))
        }
    }
    
    func getLeaveAmountDate() -> String {
        var result = ""
        if let start = Date(fromString: startDate ?? "", format: .isoDate), let end = Date(fromString: endDate ?? "", format: .isoDate) {
            result = "Valid from \(start.toString(dateStyle: .medium, timeStyle: .none)) to \(end.toString(dateStyle: .medium, timeStyle: .none))"
        }
        return result
    }
    
    func getLeaveProgress() -> Float {
        let quota = Double(self.qouta ?? 0)
        let left = Double(self.left ?? 0)
        return Float(quota / left)
    }
}
