//
//  Overtime.swift
//  THR
//
//  Created by Apit Aprida on 08/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Overtime {
    var currentPage: Int?
    var totalPage: Int?
    var overtimes: [OvertimeData] = []
    
    var sortedLastDate: [OvertimeData] {
        return overtimes.reversed()
    }
    
    var sortedPending: [OvertimeData] {
        return sortedLastDate.sorted(by: { s1, _ -> Bool in
            guard let status1 = s1.status?.rawValue else { return false }
            return status1 == 1
        })
    }
    
    var sortedReject: [OvertimeData] {
        return sortedLastDate.filter { (overtime) -> Bool in
            overtime.status?.rawValue == 3
        }
    }
    
    var sortedApprove: [OvertimeData] {
        return sortedLastDate.filter { (overtime) -> Bool in
            overtime.status?.rawValue == 2
        }
    }
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        currentPage = json["current_page"].int
        totalPage = json["total_page"].int
        for overtime in json["overtimes"].arrayValue {
            overtimes.append(OvertimeData(json: overtime))
        }
    }
}
