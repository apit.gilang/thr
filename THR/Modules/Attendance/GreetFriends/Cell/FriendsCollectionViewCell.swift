//
//  FriendsCollectionViewCell.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/30/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class FriendsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var friendImage: UIImageView!
    @IBOutlet weak var friendName: UILabel!
    @IBOutlet weak var greetTypeIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
