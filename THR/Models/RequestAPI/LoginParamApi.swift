//
//  LoginParamApi.swift
//  THR
//
//  Created by Apit Aprida on 07/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

class LoginParamApi {
    
    var username: String?
    var password: String?
    var companyCode: String?
    
    func buildForParameters() -> [String: AnyObject] {
        var parameters: [String: AnyObject] = [:]
        parameters["user"] = ["login": username, "password": password] as AnyObject
        parameters["code"] = companyCode as AnyObject
        return parameters
    }
}
