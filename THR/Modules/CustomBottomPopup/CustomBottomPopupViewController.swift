//
//  CustomBottomViewController.swift
//  THR
//
//  Created by Apit Aprida on 19/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import BottomPopup

protocol CustomDatePickerDelegate {
    func didChangedPicker(withPicker picker: UIDatePicker)
    func didFinishedPicker(withPicker picker: UIDatePicker)
}

class CustomBottomPopupViewController: BottomPopupViewController {

    var height: CGFloat {
        return UIScreen.main.bounds.size.height * 0.4
    }
    
    var cornerRadius: CGFloat {
        return 6
    }
    var alert = AlertHelper()
    
    var customDateDelegate: CustomDatePickerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override var popupHeight: CGFloat {
        return height
    }
    
    override var popupTopCornerRadius: CGFloat {
        return cornerRadius
    }
    
    func getSelectedDate(from date: String, format: String? = "yyyy-MM-dd") -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: date) ?? Date()
    }
    
    func getSelectedTime(from time: String) -> Date {
        let dateFormatter = DateFormatter()
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        dateFormatter.calendar = Calendar(identifier: .gregorian)

        let iso8601String = dateFormatter.date(from: time)
        return iso8601String ?? Date()
    }

    func getDate(from date: Date, format: String? = "dd MMM yyyy") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    func getTime(from date: Date, format: String? = "hh:mm a") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    func showPageError(withMessage message: String) {
        UIApplication.shared.endIgnoringInteractionEvents()
        let errorVc = UIStoryboard(name: "PageError", bundle: nil).instantiateViewController(withIdentifier: "PageErrorViewController") as! PageErrorViewController
        errorVc.modalPresentationStyle = .fullScreen
        errorVc.message = message
        appDelegate.window?.makeKeyAndVisible()
        appDelegate.window?.rootViewController?.present(errorVc, animated: true, completion: nil)
    }
}

// MARK: Setup date picker
extension CustomBottomPopupViewController {
    func maxLeaveDate() -> Date {
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        components.month = 11
        return calendar.date(byAdding: components, to: currentDate)!
    }
    
    func minLeaveDate() -> Date {
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        components.month = -1
        return calendar.date(byAdding: components, to: currentDate)!
    }
    
    func maxOvertimeDate() -> Date {
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        components.day = 1
        return calendar.date(byAdding: components, to: currentDate)!
    }
    
    func minOvertimeDate() -> Date {
        let calendar = Calendar(identifier: .gregorian)
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        components.day = -1
        return calendar.date(byAdding: components, to: currentDate)!
    }
    
    func setupPicker(datePicker: UIDatePicker, textField: UITextField, mode: UIDatePicker.Mode, type: EnumApprovalType) {
        datePicker.datePickerMode = mode
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
        datePicker.addTarget(self, action: #selector(didChangedStartDate(_:)), for: .valueChanged)
        switch type {
        case .overtime:
            datePicker.minimumDate = minOvertimeDate()
            datePicker.maximumDate = maxOvertimeDate()
        default:
            datePicker.minimumDate = minLeaveDate()
            datePicker.maximumDate = maxLeaveDate()
        }
        datePicker.locale = Locale(identifier: "en_US")
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(didFinishedStartDate(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.setItems([spaceButton, doneButton], animated: false)

        textField.inputAccessoryView = toolbar
        textField.inputView = datePicker
    }
    
    @objc func didChangedStartDate(_ picker: UIDatePicker) {
        customDateDelegate?.didChangedPicker(withPicker: picker)
    }
    
    @objc func didFinishedStartDate(_ picker: UIDatePicker) {
//        customDateDelegate?.didFinishedPicker(withPicker: picker)
        view.endEditing(true)
    }
}
