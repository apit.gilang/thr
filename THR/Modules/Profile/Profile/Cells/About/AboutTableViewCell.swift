//
//  AboutTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 01/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class AboutTableViewCell: UITableViewCell {

    @IBOutlet weak var introductionLabel: UILabel!
    @IBOutlet weak var creativeTitleLabel: UILabel!
    @IBOutlet weak var creativeValueLabel: UILabel!
    @IBOutlet weak var motivationTitleLabel: UILabel!
    @IBOutlet weak var motivationValueLabel: UILabel!
    @IBOutlet weak var punctualTitleLabel: UILabel!
    @IBOutlet weak var punctualValueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func setupAbout(withData data: User) {
        introductionLabel.isHidden = false
        introductionLabel.text = data.about
        creativeTitleLabel.text = "Creative"
        creativeValueLabel.text = data.creative
        motivationTitleLabel.text = "Motivation"
        motivationValueLabel.text = data.motivated
        punctualTitleLabel.text = "Punctual"
        punctualValueLabel.text = data.punctual
    }
    
    func setupContact(withData data: User) {
        introductionLabel.isHidden = true
        creativeTitleLabel.text = "Address"
        creativeValueLabel.text = data.identityCardAddress
        motivationTitleLabel.text = "Phone"
        motivationValueLabel.text = data.phoneNumber
        punctualTitleLabel.text = "Email"
        punctualValueLabel.text = data.email
    }
    
}
