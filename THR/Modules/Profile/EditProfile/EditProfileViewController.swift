//
//  EditProfileViewController.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantEditProfile {
    static let presentToComingSoon = "presentToComingSoon"
    static let pushToChangePassword = "pushToChangePassword"
}

enum EnumEditProfileSection {
    case changeAvatar, changeAccount, changePassword, logout
}

// MARK: -  EditProfileViewController

class EditProfileViewController: ViewController {
    
    // MARK: Properties
    let sections: [EnumEditProfileSection] = [.changeAvatar, .changeAccount, .changePassword, .logout]
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: EditProfilePresenter!
    var avatarImage: UIImage?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        EditProfilePresenter.config(withEditProfileViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.registerCells(withViewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    private func showImageOptions() {
        let camera = UIAlertAction(title: "Camera", style: .default) { _ in
            self.openCamera()
        }
        let gallery = UIAlertAction(title: "Photo Library", style: .default) { _ in
            self.openPhotoLibrary()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let alert = UIAlertController(title: "", message: "Choose photo or camera for your avatar", preferredStyle: .actionSheet)
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancel)
        self.present(alert, animated: true)
    }
    
    private func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[.editedImage] as? UIImage {
            avatarImage = image
            presenter.changeAvatar(withImage: compressedImage(withImage: image))
        } else {
            alert.showBasicAlert("", message: "Unknown error", controller: self)
        }
        dismiss(animated: false)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: false)
    }
    
    func compressedImage(withImage image: UIImage) -> Data? {
        let rect = CGRect(x: 0, y: 0, width: image.size.width/2, height: image.size.height/2)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        
        // Compress image size
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let compressedImageData = resizedImage!.jpegData(compressionQuality: 0.1)
        return compressedImageData
    }
}

extension EditProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .changeAccount:
            return 4
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .changeAvatar:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ChangeAvatarTableViewCell.self), for: indexPath) as! ChangeAvatarTableViewCell
            if let image = avatarImage {
                cell.changeAvatarButton.setImage(image, for: .normal)
            }
            cell.changeAvatarDelegate = self
            return cell
        case .logout:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LogoutButtonTableViewCell.self), for: indexPath) as! LogoutButtonTableViewCell
            cell.logoutButtonDelegate = self
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderProfileTableViewCell.self), for: indexPath) as! HeaderProfileTableViewCell
            cell.arrowDownButton.setImage(UIImage(named: "ic_arrow_right"), for: .normal)
            if (sections[indexPath.section] == .changeAccount && indexPath.row == 3) || sections[indexPath.section] == .changePassword {
                cell.headerView.addShadow(location: .bottom, color: UIColor.appColor(.lightGray), opacity: 0.2, radius: 5)
            }
            if sections[indexPath.section] == .changePassword {
                cell.headerTitleLabel.text = "Change Password"
                cell.headerImage.image = UIImage(named: "ic_password")
            } else {
                cell.headerTitleLabel.text = presenter.titles[indexPath.row]
                cell.headerImage.image = presenter.icons[indexPath.row]
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = sections[indexPath.section]
        if section == .changeAccount {
            performSegue(withIdentifier: SegueConstantEditProfile.presentToComingSoon, sender: nil)
        } else if section == .changePassword {
            performSegue(withIdentifier: SegueConstantEditProfile.pushToChangePassword, sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderSkillTableViewCell.self)) as! HeaderSkillTableViewCell
        cell.skillTitleLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        cell.skillTitleLabel.textColor = UIColor.appColor(.darkGray)
        switch sections[section] {
        case .changeAvatar:
            return UIView()
        case .changeAccount:
            cell.skillTitleLabel.text = "Account"
        case .changePassword:
            cell.skillTitleLabel.text = "Password"
        default:
            return UIView()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch sections[section] {
        case .changeAvatar:
            return 0
        default:
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderSkillTableViewCell.self)) as! HeaderSkillTableViewCell
        cell.skillTitleLabel.isHidden = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch sections[section] {
        case .changeAccount:
            return 30
        default:
            return 0
        }
    }
}

extension EditProfileViewController: ChangeAvatarDelegate, LogoutButtonDelegate {
    func didChangeAvatar() {
        showImageOptions()
    }
    
    func didLogout() {
        presenter.removeToken()
    }
}

// MARK: - EditProfileView

extension EditProfileViewController: EditProfileView {
    func onSuccessRemoveToken() {
        Settings.accessToken = ""
        Settings.secretKey = ""
        Settings.userFullname = ""
        let controller = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: String(describing: LoginViewController.self)) as! LoginViewController
        appDelegate.window?.rootViewController = UINavigationController(rootViewController: controller)
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func onSuccessChangeAvatar() {
        tableView.reloadSections(IndexSet(IndexPath(row: 0, section: 0)), with: .none)
    }
    
    func onErrorChangeAvatar(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
