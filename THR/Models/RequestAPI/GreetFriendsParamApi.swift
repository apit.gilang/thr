//
//  GreetFriendsParamApi.swift
//  THR
//
//  Created by Apit Aprida on 15/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

class GreetFriendsParamApi {
    
    var friends: [Friend] = []
    
    func buildForParameters() -> [String: AnyObject] {
        var parameters: [String: AnyObject] = [:]
        var array = [[String: AnyObject]]()
        for friend in friends {
            array.append(["user_company_id": friend.id as AnyObject, "greet_type": friend.greetId?.rawValue as AnyObject])
        }
        parameters["greeting"] = array as AnyObject
        return parameters
    }
}
