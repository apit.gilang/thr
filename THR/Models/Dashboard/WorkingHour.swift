//
//  WorkingHour.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class WorkingHour {
    var todayCheckin: String?
    var todayCheckout: String?
    var work: String?
    var late: String?
    var overtime: String?
    var checkinFormatted: String {
        var result = ""
        if let dateString = todayCheckin, let date = Date(fromString: dateString, format: .isoDateTimeMilliSec) {
            result = date.toString(format: .custom("HH:mm"))
        }
        return result
    }
    var checkoutFormatted: String {
        var result = ""
        if let dateString = todayCheckout, let date = Date(fromString: dateString, format: .isoDateTimeMilliSec) {
            result = date.toString(format: .custom("HH:mm"))
        }
        return result
    }
    
    init(json: JSON? = nil) {
        guard let json = json else {
            return
        }
        let weekly = json["weekly"]
        let today = json["today"]
        
        todayCheckin = today["checkin"].string
        todayCheckout = today["checkout"].string
        work = weekly["work"].string
        late = weekly["late"].string
        overtime = weekly["overtime"].string
    }
    
    func isCheckOut() -> Bool {
        if let startTime = todayCheckin, let endTime = todayCheckout {
            if (startTime.contains("--:--") && endTime.contains("--:--")) || (startTime.contains("00:00:00") && endTime.contains("00:00:00")) {
                return false
            } else if (!startTime.contains("--:--") && endTime.contains("--:--")) || !startTime.contains("00:00:00") && endTime.contains("00:00:00") {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
}
