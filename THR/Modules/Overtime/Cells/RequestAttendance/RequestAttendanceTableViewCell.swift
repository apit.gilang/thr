//
//  RequestAttendanceTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 06/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class RequestAttendanceTableViewCell: UITableViewCell {

    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var leaveCountView: UIView!
    @IBOutlet weak var leaveCountLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var statusView: DesignableView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var typeImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func setupContentOvertime(withData data: OvertimeData) {
        dateView.isHidden = true
        leaveCountView.isHidden = true
        timeLabel.text = data.getTimeToShow()
        reasonLabel.text = data.reason
        statusView.backgroundColor = data.status?.statusColor
        statusLabel.text = data.status?.statusName
        typeImageView.image = UIImage(named: "ic_overtime")
    }
    
    func setupContentLeave(withData data: LeaveData) {
        timeView.isHidden = true
        dateLabel.text = data.getDateToShow()
        leaveCountLabel.text = data.getLeaveCount()
        reasonLabel.text = data.leaveType
        statusView.backgroundColor = data.status?.statusColor
        statusLabel.text = data.status?.statusName
        typeImageView.image = UIImage(named: "ic_approval_list")
    }
    
    func setupContentOffsite(withData data: OffsiteRequestData) {
        timeView.isHidden = true
        leaveCountView.isHidden = true
        dateLabel.text = data.getDateToShow()
        reasonLabel.text = data.reason
        statusView.backgroundColor = data.status?.statusColor
        statusLabel.text = data.status?.statusName
        typeImageView.image = UIImage(named: "ic_offsite")
    }
}
