//
//  ShortCutCollectionViewCell.swift
//  GreenHcm
//
//  Created by Apit Aprida on 11/03/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class ShortCutCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var shortCutView: DesignableView!
    @IBOutlet weak var shortCutImage: UIImageView!
    @IBOutlet weak var shortCutLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var newFrame = layoutAttributes.frame
        // note: don't change the width
        newFrame.size.height = ceil(size.height)
        layoutAttributes.frame = newFrame
        return layoutAttributes
    }

}
