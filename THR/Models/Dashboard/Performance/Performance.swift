//
//  Performance.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Performance {
    var id: Int?
    var date: String?
    var result: Double?
    var companyId: Int?
    var userId: Int?
    var statisticData: [StatisticData] = []
    var absence: AbsenceData?

    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        date = json["date"].string
        result = json["result"].double
        companyId = json["company_id"].int
        userId = json["user_id"].int
        for degree in json["degree_360"] {
            statisticData.append(StatisticData(json: degree.1, color: degree.0))
        }
        absence = AbsenceData(json: json["absence"])
    }
}
