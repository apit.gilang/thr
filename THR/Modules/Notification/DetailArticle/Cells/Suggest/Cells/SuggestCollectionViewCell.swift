//
//  SuggestCollectionViewCell.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/17/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class SuggestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var myLabel: UILabel!
    @IBOutlet var myImageView: UIImageView!
    @IBOutlet var myTextField: UITextField!
    
    
    static let identifier = "SuggestCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "SuggestCollectionViewCell", bundle: nil )
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    public func configure(with suggest: Suggest) {
//        self.myLabel.text = suggest.text
//        self.myImageView.image = UIImage(named: suggest.imageName)
//    }

}
