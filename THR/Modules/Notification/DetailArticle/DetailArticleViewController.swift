//
//  DetailArticleViewController.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/15/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantDetailArticle {
    // TODO: Add segue ids
}

enum EnumDetailArticleSection {
    case detail
    case love
    case suggest
    case contentComment
    case addComment
    case headerSuggest
    case headerComment
    case sentComment
}

// MARK: -  DetailArticleViewController

class DetailArticleViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: DetailArticlePresenter!
    var sections: [EnumDetailArticleSection] = [.detail, .headerSuggest, .suggest, .headerComment, .addComment, .contentComment ]
    var isLoved = false
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DetailArticlePresenter.config(withDetailArticleViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.registerCells(viewController: self)
        
    }
}

extension DetailArticleViewController: UITableViewDataSource, UITableViewDelegate, LoveButtonDelegate, SentCommentButtonDeletage {
    func sentCommentButton(comment: String) {
        print(comment)
    }
    
    func didPressLoveButton() {
        isLoved = !isLoved
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .contentComment:
            return 3 //comment.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .detail:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DetailArticleTableViewCell.self), for: indexPath) as! DetailArticleTableViewCell
            cell.loveButtonDelegate = self
            if isLoved {
                cell.loveImage.image = UIImage(named: "ic_love_red")
            } else {
                cell.loveImage.image = UIImage(named: "ic_love_gray")
            }
            return cell
        case .suggest:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SuggestTableViewCell.self), for: indexPath) as! SuggestTableViewCell
            return cell
        case .headerSuggest:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderSkillTableViewCell.self), for: indexPath) as! HeaderSkillTableViewCell
            cell.skillTitleLabel.text = "Suggestion for you"
            return cell
        case .headerComment:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderSkillTableViewCell.self), for: indexPath) as! HeaderSkillTableViewCell
            cell.skillTitleLabel.text = "Comment"
            return cell
        case .addComment:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddCommentTableViewCell.self), for: indexPath) as! AddCommentTableViewCell
            cell.sentCommentButtonDelegate = self
            return cell
        case .contentComment:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ContentCommentTableViewCell.self), for: indexPath) as! ContentCommentTableViewCell
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

// MARK: - DetailArticleView

extension DetailArticleViewController: DetailArticleView {
    // TODO: implement view methods
}
