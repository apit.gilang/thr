//
//  LoginViewController.swift
//  THR
//
//  Created by Apit Aprida on 07/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantLogin {
    // TODO: Add segue ids
}

// MARK: -  LoginViewController

class LoginViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var companyCodeTextField: UITextField!
    
    var presenter: LoginPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        LoginPresenter.config(withLoginViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        companyCodeTextField.delegate = self
        if Settings.isDeviceJailbroken {
            let closeApp = UIAlertAction(title: "Close", style: .default) { (_) in
                exit(0)
            }
            alert.showAlert("", message: "We can't run our apps because your device is jailbroken", actions: [closeApp], controller: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        var username: String?
        if let data = usernameTextField.text {
            if data.isEmptyAfterTrim {
                alert.showBasicAlert("", message: "Mohon masukkan Nama Akun Anda", controller: self)
            } else {
                username = data
            }
        } else {
            alert.showBasicAlert("", message: "Mohon masukkan Nama Akun Anda", controller: self)
        }

        var password: String?
        if let data = passwordTextField.text {
            if data.isEmpty {
                alert.showBasicAlert("", message: "Mohon masukkan password Anda", controller: self)
            } else {
                password = data
            }
        } else {
            alert.showBasicAlert("", message: "Mohon masukkan Nama Akun Anda", controller: self)
        }

        var companyCode: String?
        if let data = companyCodeTextField.text {
            if data.isEmpty {
                alert.showBasicAlert("", message: "Mohon masukkan Kode Perusahaan Anda", controller: self)
            } else {
                companyCode = data
            }
        } else {
            alert.showBasicAlert("", message: "Mohon masukkan Kode Perusahaan Anda", controller: self)
        }

        // Check validation username
        if let validUsername = username {
            if let validPassword = password {
                if let validCode = companyCode {
                    presenter.login(withUser: validUsername, password: validPassword, companyCode: validCode)
                } else {
                    companyCodeTextField.becomeFirstResponder()
                }
            } else {

                passwordTextField.becomeFirstResponder()
            }
        } else {

            usernameTextField.becomeFirstResponder()
        }
    }
}

// MARK: - LoginView

extension LoginViewController: LoginView {
    func onSuccessLogin() {
        let storyboard = UIStoryboard(name: "CustomTabbar", bundle: nil)
        let rootView = storyboard.instantiateViewController(withIdentifier: "CustomTabbarViewController") as! CustomTabbarViewController
        appDelegate.window?.rootViewController = UINavigationController(rootViewController: rootView)
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func onSuccessSaveToken() {
        print("success register notification")
    }
    
    func onErrorLogin(withMessage message: String) {
        showPageError(withMessage: message)
    }
}
