//
//  CreateTodoListViewController.swift
//  THR
//
//  Created by Apit Aprida on 05/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import BottomPopup
import NVActivityIndicatorView

// MARK: - SegueConstants

enum SegueConstantCreateTodoList {
    // TODO: Add segue ids
}

protocol CreateTodoDelegate {
    func didFinishedCreateTodo()
}

// MARK: -  CreateTodoListViewController

class CreateTodoListViewController: CustomBottomPopupViewController {
    
    // MARK: Properties
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var todoTextView: UITextView!
    @IBOutlet weak var reminderSwitch: UISwitch!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var timeField: UITextField!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    private var timePicker = UIDatePicker()
    private var datePicker = UIDatePicker()
    var presenter: CreateTodoListPresenter!
    var parameter = CreateTodoParamApi()
    var todoDelegate: CreateTodoDelegate?
    var todo: TodoList?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CreateTodoListPresenter.config(withCreateTodoListViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        todoTextView.delegate = self
        customDateDelegate = self
        setupPicker(datePicker: datePicker, textField: dateField, mode: .date, type: .leave)
        setupPicker(datePicker: timePicker, textField: timeField, mode: .time, type: .leave)
        if let todo = todo {
            setupEditTodo(todo: todo)
        } else {
            deleteButton.isHidden = true
        }
    }
    
    override var height: CGFloat {
        return UIScreen.main.bounds.size.height * 0.7
    }
    
    override var cornerRadius: CGFloat {
        return 40
    }
    
    @IBAction func createButtonPressed(_ sender: Any) {
        todoTextView.resignFirstResponder()
        if validateTodo() == "" {
            parameter.isRemind = reminderSwitch.isOn
            if let _ = todo {
                presenter.updateTodo(with: todo?.id ?? 0, parameter: parameter)
            } else {
                presenter.createTodo(withParameter: parameter)
            }
        } else {
            alert.showBasicAlert("", message: validateTodo(), controller: self)
        }
    }
    
    @IBAction func deleteButtonPressed() {
        presenter.deleteTodo(with: todo?.id ?? 0)
    }
    
    private func setupEditTodo(todo: TodoList) {
        guard let dateString = todo.date, let detail = todo.todo, let time = todo.clock, let isRemind = todo.isRemind else {
            return
        }
        datePicker.date = getSelectedDate(from: dateString)
        dateLabel.text = getDate(from: getSelectedDate(from: dateString))
        timePicker.date = getSelectedTime(from: time)
        timeLabel.text = getTime(from: getSelectedTime(from: time))
        todoTextView.text = detail
        reminderSwitch.isOn = isRemind
        placeHolderLabel.alpha = 0
        parameter.date = getDate(from: datePicker.date, format: "yyyy-MM-dd")
        parameter.clock = getTime(from: timePicker.date, format: "HH:mm")
        parameter.toDo = detail
        createButton.setTitle("Update", for: .normal)
        deleteButton.isHidden = false
    }
    
    func validateTodo() -> String {
        var error = ""
        if parameter.toDo == nil {
            error = "Your todo still empty."
        } else if parameter.date == nil {
            error = "Date can't be empty."
        } else if parameter.clock == nil {
            error = "Time can't be empty."
        }
        return error
    }
}

extension CreateTodoListViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        placeHolderLabel.alpha = 0
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        placeHolderLabel.alpha = textView.text.isEmptyAfterTrim ? 1 : 0
        parameter.toDo = textView.text.isEmptyAfterTrim ? nil : textView.text
    }
}

extension CreateTodoListViewController: CustomDatePickerDelegate {
    func didChangedPicker(withPicker picker: UIDatePicker) {
        if picker == datePicker {
            parameter.date = getDate(from: datePicker.date, format: "yyyy-MM-dd")
            dateLabel?.text = getDate(from: datePicker.date)
        } else if picker == timePicker {
            parameter.clock = getTime(from: timePicker.date, format: "HH:mm")
            timeLabel?.text = getTime(from: timePicker.date)
        }
    }
    
    func didFinishedPicker(withPicker picker: UIDatePicker) {
        if picker == datePicker {
            parameter.date = getDate(from: datePicker.date, format: "yyyy-MM-dd")
            dateLabel.text = getDate(from: datePicker.date)
        } else if picker == timePicker {
            parameter.clock = getTime(from: timePicker.date, format: "HH:mm")
            timeLabel.text = getTime(from: timePicker.date)
        }
    }
}

// MARK: - CreateTodoListView

extension CreateTodoListViewController: CreateTodoListView {
    func onSuccessCreateTodo() {
        dismiss(animated: true) {
            self.todoDelegate?.didFinishedCreateTodo()
        }
    }
    
    func onErrorCreateTodo(withMessage message: String) {
        dismiss(animated: true) {
            self.showPageError(withMessage: message)
        }
    }
}
