//
//  ApiAttendance.swift
//  THR
//
//  Created by Apit Aprida on 08/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiAttendance: URLRequestConvertible {
    
    case getAttendance
    case checkin
    case checkout(id: Int)
    case getFriends
    case greetFriends(parameter: GreetFriendsParamApi)
    case submitBehaviour(parameter: BehaviourParamApi)
    
    var path: String {
        switch self {
        case .checkout(let id):
            return "/attendances/\(id)"
        case .getFriends:
            return "/attendances/greeting_list"
        case .greetFriends:
            return "/attendances/greeting"
        case .submitBehaviour(let parameter):
            return "/attendances/\(parameter.idAbsence ?? 0)/behavior"
        default:
            return "/attendances"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getAttendance, .getFriends:
            return .get
        case .checkin, .greetFriends:
            return .post
        case .checkout, .submitBehaviour:
            return .put
        }
    }
    
    var parameter: [String: AnyObject] {
        switch self {
        case .greetFriends(let parameter):
            return parameter.buildForParameters()
        case .submitBehaviour(let parameter):
            return parameter.buildForParameters()
        default:
            return [:]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.thrHeaders(withToken: Settings.accessToken, secretKey: Settings.secretKey)
        return try HTTPMethod.get.rawValue == urlRequest.httpMethod ? URLEncoding.default.encode(urlRequest, with: self.parameter) : JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
