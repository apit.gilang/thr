//
//  AttendanceParamApi.swift
//  THR
//
//  Created by Apit Aprida on 13/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

class AttendanceParamApi {
    
    var image: Data?
    var latitude: Double?
    var longtitude: Double?
    var id: Int?
}
