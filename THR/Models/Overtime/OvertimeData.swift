//
//  OvertimeData.swift
//  THR
//
//  Created by Apit Aprida on 12/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

struct OvertimeData {
    var id: Int?
    var companyId: Int?
    var userId: String?
    var startTime: String?
    var endTime: String?
    var reason: String?
    var status: EnumStatusApprovalType?
    var dateRequest: String?
    var date: String {
        var result = ""
        if let dateString = startTime {
            if let date = Date(fromString: dateString, format: .isoDateTimeMilliSec) {
                result = date.toString(dateStyle: .long, timeStyle: .none)
            }
        }
        return result
    }
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        companyId = json["company_id"].int
        userId = json["user_id"].string
        startTime = json["start_time"].string
        endTime = json["end_time"].string
        reason = json["reason"].string
        status = EnumStatusApprovalType(rawValue: json["status"].int ?? 0)
        dateRequest = json["date_request"].string
    }
    
    func getTimeToShow(forDetail isTrue: Bool? = false) -> String {
        var result = ""
        if let startString = startTime, let endString = endTime {
            if let start = Date(fromString: startString, format: .isoDateTimeMilliSec), let end = Date(fromString: endString, format: .isoDateTimeMilliSec) {
                let totalTimeInMinutes = end.timeIntervalSince(start) / 3600
                if isTrue == true {
                    result = String(format: "%.02f Hours", totalTimeInMinutes)
                } else {
                    result = "\(start.toString(format: .custom("HH:mm"))) - \(end.toString(format: .custom("HH:mm")))"
                }
            }
        }
        return result
    }
}
