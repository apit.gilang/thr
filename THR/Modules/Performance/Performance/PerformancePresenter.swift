//
//  PerformancePresenter.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/29/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol PerformanceViewPresenter: class {
    init(view: PerformanceView)
    // TODO: Declare view presenter methods
}

protocol PerformanceView: class {
    // TODO: Declare view methods
}

class PerformancePresenter: PerformanceViewPresenter {
    
    static func config(withPerformanceViewController vc: PerformanceViewController) {
        let presenter = PerformancePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: PerformanceView
    
    required init(view: PerformanceView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
