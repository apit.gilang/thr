//
//  CustomAlertPresenter.swift
//  THR
//
//  Created by Apit Aprida on 29/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol CustomAlertViewPresenter: class {
    init(view: CustomAlertView)
    // TODO: Declare view presenter methods
}

protocol CustomAlertView: class {
    // TODO: Declare view methods
}

class CustomAlertPresenter: CustomAlertViewPresenter {
    
    static func config(withCustomAlertViewController vc: CustomAlertViewController) {
        let presenter = CustomAlertPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: CustomAlertView
    
    required init(view: CustomAlertView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
