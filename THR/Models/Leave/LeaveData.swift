//
//  LeaveDaga.swift
//  THR
//
//  Created by Apit Aprida on 12/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class LeaveData {
    var id: Int?
    var userId: Int?
    var companyId: Int?
    var reason: String?
    var status: EnumStatusApprovalType?
    var startDate: String?
    var endDate: String?
    var leaveCount: Int?
    var leaveType: String?
    var userInputLeave: Int?
    var imageThumb: String?
    var imageMedium: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        userId = json["user_id"].int
        companyId = json["company_id"].int
        reason = json["reason"].string
        status = EnumStatusApprovalType(rawValue: json["status"].intValue)
        startDate = json["start_date"].string
        endDate = json["end_date"].string
        leaveCount = json["leave_count"].int
        leaveType = json["leave_type"].string
        userInputLeave = json["user_input_leave"].int
        imageThumb = json["image"]["thumb"].string
        imageMedium = json["image"]["medium"].string
    }
    
    func getDateToShow() -> String {
        var result = ""
        if let start = Date(fromString: startDate ?? "", format: .isoDate), let end = Date(fromString: endDate ?? "", format: .isoDate) {
            result = "\(start.toString(dateStyle: .medium, timeStyle: .none)) - \(end.toString(dateStyle: .medium, timeStyle: .none))"
        }
        return result
    }
    
    func getLeaveCount() -> String {
        var result = ""
        if let start = Date(fromString: startDate ?? "", format: .isoDate), let end = Date(fromString: endDate ?? "", format: .isoDate) {
            result = "\(end.interval(ofComponent: .day, fromDate: start)) Day"
        }
        return result
    }
}

extension Date {
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {

        let currentCalendar = Calendar.current

        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }

        return (end - start) + 1
    }
}
