//
//  ApiLogin.swift
//  THR
//
//  Created by Apit Aprida on 07/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiLogin: URLRequestConvertible {
    
    case login(param: LoginParamApi)
    case saveFcmToken
    case deleteToken
    
    var path: String {
        switch self {
        case .saveFcmToken:
            return "/notifications/save_fcm_token"
        case .deleteToken:
            return "/notifications/remove_fcm_token"
        default:
            return "/sessions"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .deleteToken:
            return .put
        default:
            return .post
        }
    }
    
    var parameter: [String: AnyObject] {
        switch self {
        case .login(let param):
            return param.buildForParameters()
        case .saveFcmToken, .deleteToken:
            return ["fcm_token": Settings.fcmToken as AnyObject, "device_id": UIDevice.current.identifierForVendor?.uuidString as AnyObject]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        if path == ApiLogin.saveFcmToken.path || path == ApiLogin.deleteToken.path {
            urlRequest.thrHeaders(withToken: Settings.accessToken, secretKey: Settings.secretKey)
        }
        return try JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
