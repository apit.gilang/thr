//
//  ApiDashboard.swift
//  THR
//
//  Created by Apit Aprida on 07/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiDashboard: URLRequestConvertible {
    
    case getDashboard
    case createTodo(parameter: CreateTodoParamApi)
    
    var path: String {
        switch self {
        case .createTodo:
            return "/to_do_lists"
        default:
            return "/dashboards"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .createTodo:
            return .post
        default:
            return .get
        }
    }
    
    var parameter: [String: AnyObject] {
        switch self {
        case .createTodo(let parameter):
            return parameter.buildForParameters()
        default:
            return [:]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.thrHeaders(withToken: Settings.accessToken, secretKey: Settings.secretKey)
        return try HTTPMethod.get.rawValue == urlRequest.httpMethod ? URLEncoding.default.encode(urlRequest, with: self.parameter) : JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
