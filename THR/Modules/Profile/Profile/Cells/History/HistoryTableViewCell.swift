//
//  HistoryTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var circleSeparatorView: DesignableView!
    @IBOutlet weak var timeImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var skillImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func setupContentHistory(withHistory history: History) {
        circleSeparatorView.backgroundColor = history.getColor()
        timeImage.image = history.getTimeImage()
        dateLabel.text = history.getDate()
        dateLabel.textColor = history.getColor()
        typeLabel.text = history.getType().capitalized
        placeNameLabel.text = history.name
        noteLabel.text = history.note
        skillImage.image = history.getSkillImage()
    }
}
