//
//  InfoNotificationTableViewCell.swift
//  GreenHcm
//
//  Created by Apit Aprida on 06/03/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

protocol InfoNotificationDelegate {
    func didPressReadNotification()
}

class NewNotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var notificationLabel: UILabel!
    var delegate: InfoNotificationDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func setupBadgeInformation(withNotifications notifications: [NotificationData]) {
        let unreadNotifCount = notifications.filter({ (notification) -> Bool in
            notification.isRead == false
        })
        notificationLabel.text = "\(unreadNotifCount.count)"
    }
    
    @IBAction func onReadButtonPressed(_ sender: Any) {
        delegate?.didPressReadNotification()
    }
}
