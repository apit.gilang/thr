//
//  GreetFriendsPresenter.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/30/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol GreetFriendsViewPresenter: class {
    init(view: GreetFriendsView)
    func getFriends()
    func greetFriends(parameter: GreetFriendsParamApi)
}

protocol GreetFriendsView: class {
    func onSuccessGetFriends()
    func onSuccessGreetFriends()
    func onErrorGetFriends(withMessage message: String)
}

class GreetFriendsPresenter: GreetFriendsViewPresenter {
    
    static func config(withGreetFriendsViewController vc: GreetFriendsViewController) {
        let presenter = GreetFriendsPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: GreetFriendsView
    var friends: [Friend] = []
    
    required init(view: GreetFriendsView) {
        self.view = view
    }
    
    func getFriends() {
        THRApi.instance.request(ApiAttendance.getFriends, success: { (json) in
            self.friends = []
            guard let json = json else {
                return
            }
            for friend in json["greeting_list"].arrayValue {
                self.friends.append(Friend(json: friend))
            }
            self.view.onSuccessGetFriends()
        }) { (error) in
            self.view.onErrorGetFriends(withMessage: error)
        }
    }
    
    func greetFriends(parameter: GreetFriendsParamApi) {
        THRApi.instance.request(ApiAttendance.greetFriends(parameter: parameter), success: { (json) in
            self.view.onSuccessGreetFriends()
        }) { (error) in
            self.view.onErrorGetFriends(withMessage: error)
        }
    }
}
