//
//  ComingSoonPresenter.swift
//  THR
//
//  Created by Apit Aprida on 29/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ComingSoonViewPresenter: class {
    init(view: ComingSoonView)
    // TODO: Declare view presenter methods
}

protocol ComingSoonView: class {
    // TODO: Declare view methods
}

class ComingSoonPresenter: ComingSoonViewPresenter {
    
    static func config(withComingSoonViewController vc: ComingSoonViewController) {
        let presenter = ComingSoonPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: ComingSoonView
    
    required init(view: ComingSoonView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
