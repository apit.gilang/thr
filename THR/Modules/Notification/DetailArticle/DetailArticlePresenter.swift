//
//  DetailArticlePresenter.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/15/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol DetailArticleViewPresenter: class {
    init(view: DetailArticleView)
    // TODO: Declare view presenter methods
}

protocol DetailArticleView: class {
    // TODO: Declare view methods
}

class DetailArticlePresenter: DetailArticleViewPresenter {
    
    static func config(withDetailArticleViewController vc: DetailArticleViewController) {
        let presenter = DetailArticlePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: DetailArticleView
    
    required init(view: DetailArticleView) {
        self.view = view
    }
    
    func registerCells(viewController vc: DetailArticleViewController) {
        vc.tableView.register(UINib(nibName: String(describing: DetailArticleTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: DetailArticleTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: SuggestTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: SuggestTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: HeaderSkillTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: HeaderSkillTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: AddCommentTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: AddCommentTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: ContentCommentTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ContentCommentTableViewCell.self))
    }
    // TODO: Implement view presenter methods
}
