//
//  Location.swift
//  THR
//
//  Created by Apit Aprida on 12/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Location {
    var name: String?
    var latitude: Double?
    var longtitude: Double?
    var radius: Double?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        name = json["name"].string
        latitude = json["latitude"].double
        longtitude = json["longtitude"].double
        radius = json["radius"].double
    }
}
