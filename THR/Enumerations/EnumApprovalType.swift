//
//  EnumApprovalType.swift
//  THR
//
//  Created by Apit Aprida on 12/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

enum EnumApprovalType: String {
    case all, overtime, leave, approval, offsiteAttendance = "offsiteattendance"
    
    var typeColor: UIColor {
        switch self {
        case .leave:
            return UIColor.appColor(.orange)
        default:
            return UIColor.appColor(.blue)
        }
    }
    
    var typeName: String {
        switch self {
        case .offsiteAttendance:
            return "Offsite Attendance"
        case .all:
            return ""
        default:
            return self.rawValue.capitalized
        }
    }
}
