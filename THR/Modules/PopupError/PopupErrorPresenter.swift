//
//  PopupErrorPresenter.swift
//  THR
//
//  Created by Apit Aprida on 05/08/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol PopupErrorViewPresenter: class {
    init(view: PopupErrorView)
    // TODO: Declare view presenter methods
}

protocol PopupErrorView: class {
    // TODO: Declare view methods
}

class PopupErrorPresenter: PopupErrorViewPresenter {
    
    static func config(withPopupErrorViewController vc: PopupErrorViewController) {
        let presenter = PopupErrorPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: PopupErrorView
    
    required init(view: PopupErrorView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
