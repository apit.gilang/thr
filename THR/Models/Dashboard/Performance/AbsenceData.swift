//
//  AbsenceData.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class AbsenceData {
    var late: Int?
    var result: Int?
    var work: Int?
    var overtime: Int?

    init(json: JSON?) {
        guard let json = json else {
            return
        }
        late = json["late"].int
        result = json["result"].int
        work = json["work"].int
        overtime = json["overtime"].int
    }
}
