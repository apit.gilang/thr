//
//  LeaveAmountTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 16/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class LeaveAmountTableViewCell: UITableViewCell {

    @IBOutlet weak var validDateLabel: UILabel!
    @IBOutlet weak var leaveTakesLabel: UILabel!
    @IBOutlet weak var leftoverLeaveLabel: UILabel!
    @IBOutlet weak var leaveProgress: UIProgressView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupLeaveContent(withData data: Leave) {
        validDateLabel.text = data.getLeaveAmountDate()
        leaveTakesLabel.text = "\(data.taken ?? 0)"
        leftoverLeaveLabel.text = "\(data.left ?? 0)"
        leaveProgress.progress = data.getLeaveProgress()
    }
    
}
