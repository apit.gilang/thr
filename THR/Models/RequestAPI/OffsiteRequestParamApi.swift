//
//  OffsiteRequestParamApi.swift
//  THR
//
//  Created by Apit Aprida on 17/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

class OffsiteRequestParamApi {
    
    var date: String?
    var addressName: String?
    var reason: String?
    var latitude: Double?
    var longtitude: Double?
    var radius: Double?
    var isLockLocation: Bool?
    
    func buildForParameters() -> [String: AnyObject] {
        var parameters: [String: AnyObject] = [:]
        parameters["offsite_attendance"] = [
            "date": date as AnyObject,
            "address_name": addressName as AnyObject,
            "reason": reason as AnyObject,
            "latitude": latitude as AnyObject,
            "longtitude": longtitude as AnyObject,
            "radius": radius as AnyObject,
            "is_lock_location": isLockLocation as AnyObject
        ] as AnyObject
        return parameters
    }
}
