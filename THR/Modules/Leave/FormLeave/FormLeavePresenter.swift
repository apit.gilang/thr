//
//  FormLeavePresenter.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol FormLeaveViewPresenter: class {
    init(view: FormLeaveView)
    func requestOvertime(withParameter param: OvertimeParamApi)
    func requestLeave(withParameter param: LeaveParamApi)
    func getLeaveType()
    func getOvertimeType()
}

protocol FormLeaveView: class {
    func onSuccessRequestOvertime()
    func onSuccessGetLeaveType()
    func onSuccessRequestLeave()
    func onErrorRequestAPI(withMessage message: String)
}

class FormLeavePresenter: FormLeaveViewPresenter {
    
    static func config(withFormLeaveViewController vc: FormLeaveViewController) {
        let presenter = FormLeavePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: FormLeaveView
    var leaveTypes: [LeaveType] = []
    
    required init(view: FormLeaveView) {
        self.view = view
    }
    
    func requestOvertime(withParameter param: OvertimeParamApi) {
        THRApi.instance.request(ApiOvertime.createOvertime(param: param), success: { (json) in
            self.view.onSuccessRequestOvertime()
        }) { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }
    }
    
    func requestLeave(withParameter param: LeaveParamApi) {
        THRApi.instance.uploadImage(ApiLeave.createLeave, multipartFormData: { (multipartFormData) in
            if let fileUpload = param.fileUpload {
                multipartFormData.append(fileUpload, withName: "leave[file_upload]", fileName: "images.jpg", mimeType: "image/jpg")
            }
            multipartFormData.append("\(param.leaveTypeId!)".data(using: .utf8)!, withName: "leave[leave_type_id]")
            multipartFormData.append("\(param.startDate!)".data(using: .utf8)!, withName: "leave[start_date]")
            multipartFormData.append("\(param.endDate!)".data(using: .utf8)!, withName: "leave[end_date]")
            multipartFormData.append("\(param.reason!)".data(using: .utf8)!, withName: "leave[reason]")
        }, success: { (json) in
            self.view.onSuccessRequestLeave()
        }) { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }
    }
    
    func getLeaveType() {
        THRApi.instance.request(ApiLeave.getLeaveType, success: { (json) in
            guard let json = json else {
                return
            }
            for type in json["leave_type"].arrayValue {
                self.leaveTypes.append(LeaveType(json: type))
            }
            self.view.onSuccessGetLeaveType()
        }) { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }
    }
    
    func getOvertimeType() {
        THRApi.instance.request(ApiOvertime.getOvertimeType, success: { (json) in
            guard let json = json else {
                return
            }
            for type in json["overtime_type"].arrayValue {
                self.leaveTypes.append(LeaveType(json: type))
            }
            self.view.onSuccessGetLeaveType()
        }) { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }
    }
}
