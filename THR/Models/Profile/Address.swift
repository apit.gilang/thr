//
//  Address.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Address {
    var id: Int?
    var userId: Int?
    var provinceId: Int?
    var cityId: Int?
    var kecamatan: String?
    var rt: Int?
    var rw: Int?
    var postalCode: Int?
    var street: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        userId = json["user_id"].int
        provinceId = json["province_id"].int
        cityId = json["city_id"].int
        kecamatan = json["kecamatan"].string
        rt = json["rt"].int
        rw = json["rw"].int
        postalCode = json["postal_code"].int
        street = json["street"].string
    }
}
