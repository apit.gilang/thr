//
//  EnumNotificationType.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/8/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

enum EnumNotificationType: String {
    case notif = "Notifikasi"
    case info = "Info"
}
