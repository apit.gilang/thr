//
//  PushNotificationInfo.swift
//  THR
//
//  Created by Apit Gilang Aprida on 12/22/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

protocol ShowAsAlertMessage {
    func isActionOpenWeb() -> Bool
    func isApproval() -> Bool
    func isOvertime() -> Bool
    func getUrl() -> String
    func getTitle() -> String
}

protocol ShowAsNotification: ShowAsAlertMessage {
    func getAlertTitle() -> String
    func getAlertMessage() -> String
}

class PushNotificationInfo: ShowAsAlertMessage, ShowAsNotification {

    var clickAction: String?
    var url: String?
    var title: String?
    var alertTitle: String?
    var alertMessage: String?
    var type: String?

    init(userInfo: [AnyHashable: Any] = [:]) {

        // Parse click action
        if let tempClickAction = userInfo["click_action"] as? String {
            clickAction = tempClickAction
        }

        // Parse URL
        if let tempUrl = userInfo["url"] as? String {
            url = tempUrl
        }

        // Parse title
        if let tempTitleData = userInfo["title"] as? String {
            title = tempTitleData
        }

        if let tempType = userInfo["notifable_type"] as? String {
            type = tempType
        }

        // Parse APS
        if let info = userInfo["aps"] as? NSDictionary {

            if let alert = info["alert"] as? NSDictionary {

                // Parse alert title
                if let tempTitleAlert = alert["title"] as? String {
                    alertTitle = tempTitleAlert
                }

                // Parse alert message
                if let tempMessageAlert = alert["body"] as? String {
                    alertMessage = tempMessageAlert
                }
            }
        }
    }

    func isActionOpenWeb() -> Bool {
        return "OPEN_WEB" == clickAction
    }

    func isApproval() -> Bool {
        return "Approval" == type
    }

    func isOvertime() -> Bool {
        return "Overtime" == type
    }

    func getUrl() -> String {
        return url ?? ""
    }

    func getTitle() -> String {
        return title ?? ""
    }

    func getAlertTitle() -> String {
        return alertTitle ?? ""
    }

    func getAlertMessage() -> String {
        return alertMessage ?? ""
    }
}
