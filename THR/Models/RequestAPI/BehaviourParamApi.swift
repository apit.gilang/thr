//
//  BehaviourParamApi.swift
//  THR
//
//  Created by Apit Aprida on 15/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

class BehaviourParamApi {
    
    var scale: Int?
    var note: String?
    var isPositif: Bool?
    var emotion: String?
    var idAbsence: Int?

    func buildForParameters() -> [String: AnyObject] {
        var parameters: [String: AnyObject] = [:]
        parameters["attendance"] = [
            "behavior_scale": scale as AnyObject,
            "behavior_note": note as AnyObject,
            "behavior_is_positif": isPositif as AnyObject,
            "behavior_emotion": emotion as AnyObject] as AnyObject

        return parameters
    }
}
