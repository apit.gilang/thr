//
//  InfoTableViewCell.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/9/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
