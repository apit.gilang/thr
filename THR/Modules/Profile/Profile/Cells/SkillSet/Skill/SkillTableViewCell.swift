//
//  SkillTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class SkillTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var skills: [Skill] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: String(describing: SkillProgressCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: SkillProgressCollectionViewCell.self))
    }
}

extension SkillTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return skills.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SkillProgressCollectionViewCell.self), for: indexPath) as! SkillProgressCollectionViewCell
        let skill = skills[indexPath.row]
        cell.progressIndicatorBar.value = CGFloat(skill.percentage ?? 0)
        cell.skillLabel.text = skill.name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let spaceProgress: CGFloat = 16
        let width: CGFloat = (collectionView.frame.size.width / 3) - spaceProgress
        let height: CGFloat = 90
        return CGSize(width: width, height: height)
    }
}
