//
//  LanguageTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var readProgress: UIProgressView!
    @IBOutlet weak var writeProgress: UIProgressView!
    @IBOutlet weak var speakProgress: UIProgressView!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
}
