//
//  ApiProfile.swift
//  THR
//
//  Created by Apit Aprida on 15/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiProfile: URLRequestConvertible {
    
    case getProfile
    case changeAvatar
    case registerFace
    case changePassword(parameter: ChangePasswordParamApi)
    
    var path: String {
        switch self {
        case .changeAvatar:
            return "/users/update_avatar"
        case .registerFace:
            return "/users/register_face"
        case .changePassword:
            return "/users/change_password"
        default:
            return "/users"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .changeAvatar:
            return .patch
        case .registerFace:
            return .post
        case .changePassword:
            return .put
        default:
            return .get
        }
    }
    
    var parameter: [String: AnyObject] {
        switch self {
        case .changePassword(let parameter):
            return parameter.buildForParameters()
        default:
            return [:]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.thrHeaders(withToken: Settings.accessToken, secretKey: Settings.secretKey)
        return try HTTPMethod.get.rawValue == urlRequest.httpMethod ? URLEncoding.default.encode(urlRequest, with: self.parameter) : JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
