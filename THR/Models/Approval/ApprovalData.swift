//
//  ApprovalData.swift
//  THR
//
//  Created by Apit Aprida on 12/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class ApprovalData {
    var id: Int?
    var executor: String?
    var approvalableType: EnumApprovalType?
    var approvalableId: Int?
    var userId: Int?
    var companyId: Int?
    var start: String?
    var end: String?
    var reason: String?
    var status: EnumStatusApprovalType?
    var userFullname: String?
    var note: String?
    var date: String?
    var image: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        executor = json["executor"].string
        approvalableType = EnumApprovalType(rawValue: json["approvalable_type"].string?.lowercased() ?? "")
        approvalableId = json["approvalable_id"].int
        userId = json["user_id"].int
        companyId = json["company_id"].int
        start = json["start"].string
        end = json["end"].string
        reason = json["reason"].string
        status = EnumStatusApprovalType(rawValue: json["status"].int ?? 0)
        userFullname = json["user_fullname"].string
        note = json["note"].string
        date = json["date"].string
        image = json["image"].string
    }
    
    func getDateToShow() -> String {
        var result = ""
        if let startString = start, let endString = end {
            if let start = Date(fromString: startString, format: .isoDateTimeMilliSec), let end = Date(fromString: endString, format: .isoDateTimeMilliSec) {
                result = "\(start.toString(format: .custom("dd MMM yyyy"))) - \(end.toString(format: .custom("dd MMM yyyy")))"
            }
        }
        return result
    }
}
