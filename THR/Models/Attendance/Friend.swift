//
//  Friend.swift
//  THR
//
//  Created by Apit Aprida on 14/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Friend {
    var id: Int?
    var fullname: String?
    var image: String?
    var greetId: EnumGreetType?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        fullname = json["fullname"].string
        image = json["image"].string
    }
}
