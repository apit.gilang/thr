//
//  EditProfilePresenter.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol EditProfileViewPresenter: class {
    init(view: EditProfileView)
    func registerCells(withViewController vc: EditProfileViewController)
    func changeAvatar(withImage image: Data?)
}

protocol EditProfileView: class {
    func onSuccessChangeAvatar()
    func onSuccessRemoveToken()
    func onErrorChangeAvatar(withMessage message: String)
}

class EditProfilePresenter: EditProfileViewPresenter {
    
    static func config(withEditProfileViewController vc: EditProfileViewController) {
        let presenter = EditProfilePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: EditProfileView
    let icons: [UIImage?] = [UIImage(named: "ic_about"), UIImage(named: "ic_history"), UIImage(named: "ic_skill"), UIImage(named: "ic_contact")]
    let titles: [String] = ["About", "History", "Skill", "Contact"]
    
    required init(view: EditProfileView) {
        self.view = view
    }
    
    func changeAvatar(withImage image: Data?) {
        THRApi.instance.uploadImage(ApiProfile.changeAvatar, multipartFormData: { (multipartFormData) in
            if let image = image {
                multipartFormData.append(image, withName: "user[image]", fileName: "images.jpg", mimeType: "image/jpg")
            }
        }, success: { (_) in
            self.view.onSuccessChangeAvatar()
        }) { (error) in
            self.view.onErrorChangeAvatar(withMessage: error)
        }
    }
    
    func registerCells(withViewController vc: EditProfileViewController) {
        vc.tableView.register(UINib(nibName: String(describing: HeaderProfileTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: HeaderProfileTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: HeaderSkillTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: HeaderSkillTableViewCell.self))
    }
    
    func removeToken() {
        THRApi.instance.request(ApiLogin.deleteToken) { (_) in
            self.view.onSuccessRemoveToken()
        } error: { (error) in
            self.view.onErrorChangeAvatar(withMessage: error)
        }

    }
}
