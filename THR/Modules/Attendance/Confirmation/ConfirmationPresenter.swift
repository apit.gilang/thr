//
//  ConfirmationPresenter.swift
//  THR
//
//  Created by Apit Aprida on 30/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ConfirmationViewPresenter: class {
    init(view: ConfirmationView)
    func requestAttendance(withParamter param: AttendanceParamApi, isCheckout: Bool)
}

protocol ConfirmationView: class {
    func onSuccessRequestLeave()
    func onErrorRequestAPI(withMessage message: String)
}

class ConfirmationPresenter: ConfirmationViewPresenter {
    
    static func config(withConfirmationViewController vc: ConfirmationViewController) {
        let presenter = ConfirmationPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: ConfirmationView
    
    required init(view: ConfirmationView) {
        self.view = view
    }
    
    func requestAttendance(withParamter param: AttendanceParamApi, isCheckout: Bool) {
        if isCheckout {
            print("Start request \(Date().toString(format: .isoDateTimeSec))")
            THRApi.instance.uploadImage(ApiAttendance.checkout(id: param.id ?? 0), multipartFormData: { (multipartFormData) in
                multipartFormData.append(param.image!, withName: "attendance[image]", fileName: "images.jpg", mimeType: "image/jpg")
                multipartFormData.append("\(param.longtitude!)".data(using: .utf8)!, withName: "attendance[longtitude]")
                multipartFormData.append("\(param.latitude!)".data(using: .utf8)!, withName: "attendance[latitude]")
            }, success: { (json) in
                print("End request \(Date().toString(format: .isoDateTimeSec))")
                self.view.onSuccessRequestLeave()
            }) { (error) in
                print("End request \(Date().toString(format: .isoDateTimeSec))")
                self.view.onErrorRequestAPI(withMessage: error)
            }
        } else {
            print("Start request \(Date().toString(format: .isoDateTimeSec))")
            THRApi.instance.uploadImage(ApiAttendance.checkin, multipartFormData: { (multipartFormData) in
                multipartFormData.append(param.image!, withName: "attendance[image]", fileName: "images.jpg", mimeType: "image/jpg")
                multipartFormData.append("\(param.longtitude!)".data(using: .utf8)!, withName: "attendance[longtitude]")
                multipartFormData.append("\(param.latitude!)".data(using: .utf8)!, withName: "attendance[latitude]")
            }, success: { (json) in
                self.view.onSuccessRequestLeave()
                print("End request \(Date().toString(format: .isoDateTimeSec))")
            }) { (error) in
                self.view.onErrorRequestAPI(withMessage: error)
                print("End request \(Date().toString(format: .isoDateTimeSec))")
            }
        }
    }
}
