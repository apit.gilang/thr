//
//  HeaderProfileTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 01/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class HeaderProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var arrowDownButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func setupContent(withSection section: EnumProfileSection) {
        headerImage.image = UIImage(named: "ic_\(section.rawValue)")
        headerTitleLabel.text = section.rawValue.capitalized
        if section == .contact {
            headerView.addShadow(location: .bottom, color: UIColor.appColor(.lightGray), opacity: 0.2, radius: 5)
        } else {
            headerView.layer.masksToBounds = true
        }
    }
    
}
