//
//  DashboardViewController.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/28/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import Charts

// MARK: - SegueConstants

enum SegueConstantDashboard {
    static let presentToExpandShortCut = "presentToExpandShortCut"
    static let pushToPerformance = "pushToPerformance"
    static let pushToApproval = "pushToApproval"
    static let pushToLeave = "pushToLeave"
    static let presentToCreateTodo = "presentToCreateTodo"
    static let pushToOffsiteRequest = "pushToOffsiteRequest"
    static let pushToOvertime = "pushToOvertime"
    static let presentToAlert = "presentToAlert"
    static let pushToNotification = "pushToNotification"
}

// MARK: -  DashboardViewController

class DashboardViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: DashboardPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DashboardPresenter.config(withDashboardViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.registerCells(withController: self)
        
        if Settings.isDeviceJailbroken {
            let closeApp = UIAlertAction(title: "Close", style: .default) { (_) in
                exit(0)
            }
            alert.showAlert("", message: "We can't run our apps because your device is jailbroken", actions: [closeApp], controller: self)
        } else {
            if Settings.accessToken != "" {
                reloadDashboard()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl.addTarget(self, action: #selector(reloadDashboard), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstantDashboard.presentToExpandShortCut == segue.identifier {
            if let vc = segue.destination as? ExpandShortCutViewController {
                vc.shortCuts = presenter.hiddenShortCuts
                vc.delegate = self
            }
        } else if SegueConstantDashboard.presentToCreateTodo == segue.identifier {
            if let vc = segue.destination as? CreateTodoListViewController {
                vc.todoDelegate = self
                if let todo = sender as? TodoList {
                    vc.todo = todo
                }
            }
        } else if SegueConstantDashboard.pushToPerformance == segue.identifier {
            if let vc = segue.destination as? PerformanceViewController {
                vc.dashboard = presenter.dashboard
            }
        }
    }
    
    @objc func reloadDashboard() {
        presenter.getDashboardData()
    }
    
    fileprivate func navigateToView(_ item: ShortCut) {
        switch EnumShortCut(rawValue: item.id ?? 0) {
        case .more:
            performSegue(withIdentifier: SegueConstantDashboard.presentToExpandShortCut, sender: nil)
        case .performance:
            performSegue(withIdentifier: SegueConstantDashboard.pushToPerformance, sender: nil)
        case .attendance:
            tabBarController?.performSegue(withIdentifier: SegueConstantCustomTabbar.pushToAttendance, sender: nil)
        case .approval:
            performSegue(withIdentifier: SegueConstantDashboard.pushToApproval, sender: nil)
        case .offsite:
            performSegue(withIdentifier: SegueConstantDashboard.pushToOffsiteRequest, sender: nil)
        case .overtime:
            performSegue(withIdentifier: SegueConstantDashboard.pushToOvertime, sender: nil)
        case .leave:
            performSegue(withIdentifier: SegueConstantDashboard.pushToLeave, sender: nil)
        case .none:
            print("Unexpected error")
        }
    }
}

extension DashboardViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.dashboardSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch presenter.dashboardSections[section] {
        case .todoList:
            return presenter.todoLists.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch presenter.dashboardSections[indexPath.section] {
        case .newNotification:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewNotificationTableViewCell.self), for: indexPath) as! NewNotificationTableViewCell
            cell.setupBadgeInformation(withNotifications: presenter.notifications)
            cell.delegate = self
            return cell
        case .workingHour:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WorkingHourTableViewCell.self), for: indexPath) as! WorkingHourTableViewCell
            presenter.setupWorkingHourCell(withCell: cell)
            return cell
        case .shortCut:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ShortCutTableViewCell.self), for: indexPath) as! ShortCutTableViewCell
            presenter.setupShortCutCell(withCell: cell)
            cell.delegate = self
            cell.collectionView.reloadData()
            return cell
        case .statistic:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: StatisticTableViewCell.self), for: indexPath) as! StatisticTableViewCell
            if let performances = presenter.dashboard?.performances, performances.count > 0 {
                cell.setupChartData(withData: performances)
                cell.noDataImage.isHidden = true
            } else {
                cell.noDataImage.isHidden = false
            }
            return cell
        case .todoListHeader:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TodoListHeaderTableViewCell.self), for: indexPath) as! TodoListHeaderTableViewCell
            cell.delegate = self
            return cell
        case .todoList:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TodoListTableViewCell.self), for: indexPath) as! TodoListTableViewCell
            cell.todoNoteLabel.text = presenter.todoLists[indexPath.row].todo
            cell.todoView.addShadow(location: .bottom, color: UIColor.appColor(.lightGray), opacity: 0.2, radius: 5)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if presenter.dashboardSections[indexPath.section] == .todoList {
            performSegue(withIdentifier: SegueConstantDashboard.presentToCreateTodo, sender: presenter.todoLists[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch presenter.dashboardSections[section] {
        case .todoList:
            return 34
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch presenter.dashboardSections[indexPath.section] {
        case .todoList:
            return 80
        default:
            return UITableView.automaticDimension
        }
    }
}

// MARK: Cells delegate

extension DashboardViewController: TodoListHeaderDelegate {
    func didPressCreateTodoList() {
        performSegue(withIdentifier: SegueConstantDashboard.presentToCreateTodo, sender: nil)
    }
}

extension DashboardViewController: ShortCutDelegate {
    func didSelectShortCut(withShorCut item: ShortCut) {
        navigateToView(item)
    }
}

extension DashboardViewController: ExpandShortCutDelegate {
    func didSelectItem(withShortCut shortCut: ShortCut) {
        navigateToView(shortCut)
    }
}

extension DashboardViewController: CreateTodoDelegate {
    func didFinishedCreateTodo() {
        presenter.getTodoLists()
    }
}

extension DashboardViewController: InfoNotificationDelegate {
    func didPressReadNotification() {
        performSegue(withIdentifier: SegueConstantDashboard.pushToNotification, sender: nil)
    }
}

// MARK: - DashboardView

extension DashboardViewController: DashboardView {
    func onSuccessGetDashboard() {
        presenter.getNotification()
        presenter.getLocation()
        refreshControl.endRefreshing()
    }
    
    func onSuccessGetLocation() {
        NotificationCenter.default.post(name: .didReceivedWorkingHour, object: presenter.attendanceLocation)
    }
    
    func onSuccesGetNotification() {
        let newNotifications = presenter.notifications.filter { (notification) -> Bool in
            notification.isNewNotif == true
        }
        UIApplication.shared.applicationIconBadgeNumber = newNotifications.count
        NotificationCenter.default.post(name: .didReceivedNotificationCount, object: newNotifications.count)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        if !Settings.isRegisteredFace {
            performSegue(withIdentifier: SegueConstantDashboard.presentToAlert, sender: nil)
        }
    }
    
    func onSuccessGetTodo() {
        if presenter.todoLists.count > 0 {
            if let section = presenter.dashboardSections.firstIndex(where: { (section) -> Bool in
                section == .todoList
            }) {
                tableView.reloadSections(IndexSet(integer: section), with: .automatic)
            }
        }
    }
    
    func onErrorRequestAPI(withMessage message: String) {
        refreshControl.endRefreshing()
        showPageError(withMessage: message)
    }
}
