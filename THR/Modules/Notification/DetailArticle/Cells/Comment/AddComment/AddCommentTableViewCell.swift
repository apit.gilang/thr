//
//  AddCommentTableViewCell.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/19/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

protocol SentCommentButtonDeletage{
    func sentCommentButton(comment: String)
}

class AddCommentTableViewCell: UITableViewCell {
    @IBOutlet weak var sentButton: UIButton!
    @IBOutlet weak var commentField: UITextField!
    
    var sentCommentButtonDelegate: SentCommentButtonDeletage?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func sentCommentButton(_ sender: Any) {
        sentCommentButtonDelegate?.sentCommentButton(comment: commentField.text ?? "")
    }
    
    
}
