//
//  N0tificationPresenter.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/11/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol N0tificationViewPresenter: class {
    init(view: N0tificationView)
    // TODO: Declare view presenter methods
}

protocol N0tificationView: class {
    // TODO: Declare view methods
}

class N0tificationPresenter: N0tificationViewPresenter {
    
    static func config(withN0tificationViewController vc: N0tificationViewController) {
        let presenter = N0tificationPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: N0tificationView
    
    required init(view: N0tificationView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
