//
//  User.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {
    var id: Int?
    var email: String?
    var username: String?
    var phoneNumber: String?
    var fullname: String?
    var companyId: Int?
    var roleId: Int?
    var isActive: Bool?
    var isReadValue: Bool?
    var shiftId: Int?
    var lastActiveAt: String?
    var nik: Int?
    var isTakeTour: Bool?
    var structureId: Int?
    var fcmToken: String?
    var attendanceCheckinLocation: [Int] = []
    var attendanceCheckoutLocation: [Int] = []
    var joinDate: String?
    var customField: String?
    var employeeStatus: Int?
    var resignDate: String?
    var employmentType: Int?
    var isOnline: Bool?
    var isLock: Bool?
    var pin: String?
    var isUsePin: Bool?
    var timeLock: Int?
    var skin: String?
    var status: Int?
    var gender: Int?
    var placeOfBirthday: String?
    var dateOfBirthday: String?
    var about: String?
    var creative: String?
    var motivated: String?
    var punctual: String?
    var religion: String?
    var nationality: String?
    var taxAddress: String?
    var identityCardNumber: String?
    var identityCardAddress: String?
    var shortCuts: [ShortCut] = []
    var imageThumb: String?
    var imageMedium: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        email = json["email"].string
        username = json["username"].string
        phoneNumber = json["phone_number"].string
        fullname = json["fullname"].string
        companyId = json["company_id"].int
        roleId = json["role_id"].int
        isActive = json["is_active"].bool
        isReadValue = json["is_read_value"].bool
        shiftId = json["shift_id"].int
        lastActiveAt = json["last_active_at"].string
        nik = json["nik"].int
        isTakeTour = json["is_take_tour"].bool
        structureId = json["structure_id"].int
        fcmToken = json["fcm_token"].string
        joinDate = json["join_date"].string
        customField = json["custom_field"].string
        employeeStatus = json["employee_status"].int
        resignDate = json["resign_date"].string
        employmentType = json["employment_type"].int
        isOnline = json["is_online"].bool
        isLock = json["is_lock"].bool
        pin = json["pin"].string
        isUsePin = json["is_use_pin"].bool
        timeLock = json["time_lock"].int
        skin = json["skin"].string
        status = json["status"].int
        gender = json["gender"].int
        placeOfBirthday = json["place_of_birthday"].string
        dateOfBirthday = json["date_of_birthday"].string
        about = json["about"].string
        creative = json["creative"].string
        motivated = json["motivated"].string
        punctual = json["punctual"].string
        religion = json["religion"].string
        nationality = json["nationality"].string
        taxAddress = json["tax_address"].string
        identityCardNumber = json["identity_card_number"].string
        identityCardAddress = json["identity_card_address"].string
        imageThumb = json["image"]["thumb"].string
        imageMedium = json["image"]["medium"].string
        
        for location in json["attendance_checkin_location"].arrayValue {
            attendanceCheckinLocation.append(location.intValue)
        }
        
        for location in json["attendance_checkout_location"].arrayValue {
            attendanceCheckoutLocation.append(location.intValue)
        }
        
        for shortCut in json["fav_module_on_app"].arrayValue {
            shortCuts.append(ShortCut(id: shortCut.intValue))
        }
    }
}
