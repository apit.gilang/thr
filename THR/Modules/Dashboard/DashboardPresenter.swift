//
//  DashboardPresenter.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/28/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

enum EnumDashboardSection {
    case newNotification
    case workingHour
    case shortCut
    case statistic
    case todoListHeader
    case todoList
}

protocol DashboardViewPresenter: class {
    init(view: DashboardView)
    // Setup data
    func getDashboardData()
    func getLocation()
    func getNotification()
    // Setup view
    func registerCells(withController vc: DashboardViewController)
    func setupShortCutCell(withCell cell: ShortCutTableViewCell)
    func setupWorkingHourCell(withCell cell: WorkingHourTableViewCell)
}

protocol DashboardView: class {
    func onSuccessGetDashboard()
    func onSuccessGetLocation()
    func onSuccesGetNotification()
    func onSuccessGetTodo()
    
    func onErrorRequestAPI(withMessage message: String)
}

class DashboardPresenter: DashboardViewPresenter {
    
    static func config(withDashboardViewController vc: DashboardViewController) {
        let presenter = DashboardPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: DashboardView
    let requiredMenu = 3
    
    var dashboardSections: [EnumDashboardSection] {
        let shortcutsCount = dashboard?.shortcuts.count ?? 0
        let notificationCount = notifications.count
        var shortCuts: [EnumDashboardSection] = [.newNotification, .workingHour, .shortCut, .statistic, .todoListHeader, .todoList]
        if shortcutsCount == 0 {
            if let index = shortCuts.firstIndex(of: .shortCut) {
                shortCuts.remove(at: index)
            }
        }
        if notificationCount == 0 {
            if let index = shortCuts.firstIndex(of: .newNotification) {
                shortCuts.remove(at: index)
            }
        }
        return shortCuts
    }
    var dashboard: Dashboard?
    var hiddenShortCuts: [ShortCut] = []
    var attendanceLocation: AttendanceLocation?
    var notifications: [NotificationData] = []
    var todoLists: [TodoList] = []
    
    required init(view: DashboardView) {
        self.view = view
    }
    
// MARK: Setup data
    
    func getDashboardData() {
        THRApi.instance.request(ApiDashboard.getDashboard, success: { (json) in
            self.dashboard = Dashboard(json: json)
            self.todoLists = self.dashboard?.todoLists ?? [TodoList]()
            self.view.onSuccessGetDashboard()
        }) { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }
    }
    
    func getLocation() {
        THRApi.instance.request(ApiAttendance.getAttendance, success: { (json) in
            self.attendanceLocation = AttendanceLocation(json: json)
            self.view.onSuccessGetLocation()
        }) { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }
    }
    
    func getNotification() {
        THRApi.instance.request(ApiNotification.getNotification, success: { (json) in
            guard let json = json else {
                return
            }
            self.notifications = []
            for data in json["notifications"].arrayValue {
                self.notifications.append(NotificationData(json: data))
            }
            self.view.onSuccesGetNotification()
        }) { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }
    }
    
    func getTodoLists() {
        THRApi.instance.request(ApiTodo.getTodo) { (json) in
            guard let json = json else {
                return
            }
            self.todoLists = [TodoList]()
            for data in json["to_do_list"].arrayValue {
                self.todoLists.append(TodoList(json: data))
            }
            self.view.onSuccessGetTodo()
        } error: { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }

    }
    
// MARK: Setup view
    
    func registerCells(withController vc: DashboardViewController) {
        vc.tableView.register(UINib(nibName: String(describing: WorkingHourTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: WorkingHourTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: NewNotificationTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: NewNotificationTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: ShortCutTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ShortCutTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: StatisticTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: StatisticTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: TodoListHeaderTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TodoListHeaderTableViewCell.self))
        vc.tableView.register(UINib(nibName: String(describing: TodoListTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TodoListTableViewCell.self))
        vc.tableView.backgroundColor = .clear
        vc.tableView.separatorStyle = .none
    }
    
    func setupShortCutCell(withCell cell: ShortCutTableViewCell) {
        if let data = dashboard {
            if data.shortcuts.count > requiredMenu {
                data.shortcuts.removeLast(data.shortcuts.count - requiredMenu)
            }
            data.shortcuts.append(ShortCut(id: EnumShortCut.more.rawValue))
            hiddenShortCuts = data.getFilteredShortcut()
            cell.shortCuts = data.shortcuts
        }
    }
    
    func setupWorkingHourCell(withCell cell: WorkingHourTableViewCell) {
        if let data = dashboard {
            cell.startTimeValueLabel.text = data.workingHour?.checkinFormatted
            cell.endTimeValueLabel.text = data.workingHour?.checkoutFormatted
            cell.lateValueLabel.text = data.workingHour?.late
            cell.workValueLabel.text = data.workingHour?.work
            cell.overtimeValueLabel.text = data.workingHour?.overtime
        }
    }
}
