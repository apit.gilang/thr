//
//  DetailApprovalPresenter.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol DetailApprovalViewPresenter: class {
    init(view: DetailApprovalView)
    func submitApproval(withId id: Int, status: EnumStatusApprovalType)
}

protocol DetailApprovalView: class {
    func onSuccessSubmitApproval()
    func onErrorSubmitApproval(withMessage message: String)
}

class DetailApprovalPresenter: DetailApprovalViewPresenter {
    
    static func config(withDetailApprovalViewController vc: DetailApprovalViewController) {
        let presenter = DetailApprovalPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: DetailApprovalView
    
    required init(view: DetailApprovalView) {
        self.view = view
    }
    
    func submitApproval(withId id: Int, status: EnumStatusApprovalType) {
        THRApi.instance.request(ApiApproval.submitApproval(id: id, type: status), success: { (json) in
            self.view.onSuccessSubmitApproval()
        }) { (error) in
            self.view.onErrorSubmitApproval(withMessage: error)
        }
    }
}
