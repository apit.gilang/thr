//
//  DetailOffsiteRequestPresenter.swift
//  THR
//
//  Created by Apit Aprida on 19/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol DetailOffsiteRequestViewPresenter: class {
    init(view: DetailOffsiteRequestView)
    // TODO: Declare view presenter methods
}

protocol DetailOffsiteRequestView: class {
    // TODO: Declare view methods
}

class DetailOffsiteRequestPresenter: DetailOffsiteRequestViewPresenter {
    
    static func config(withDetailOffsiteRequestViewController vc: DetailOffsiteRequestViewController) {
        let presenter = DetailOffsiteRequestPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: DetailOffsiteRequestView
    
    required init(view: DetailOffsiteRequestView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
