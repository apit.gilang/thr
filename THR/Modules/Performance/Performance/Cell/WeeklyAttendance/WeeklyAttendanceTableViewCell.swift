//
//  WeeklyAttendanceTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 16/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class WeeklyAttendanceTableViewCell: UITableViewCell {

    @IBOutlet weak var lateValueLabel: UILabel!
    @IBOutlet weak var workValueLabel: UILabel!
    @IBOutlet weak var overtimeValueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupWeeklyContent(withData data: WorkingHour) {
        lateValueLabel.text = data.late
        workValueLabel.text = data.work
        overtimeValueLabel.text = data.overtime
    }
    
}
