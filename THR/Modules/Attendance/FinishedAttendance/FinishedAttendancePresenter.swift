//
//  FinishedAttendancePresenter.swift
//  THR
//
//  Created by Apit Aprida on 05/08/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol FinishedAttendanceViewPresenter: class {
    init(view: FinishedAttendanceView)
    // TODO: Declare view presenter methods
}

protocol FinishedAttendanceView: class {
    // TODO: Declare view methods
}

class FinishedAttendancePresenter: FinishedAttendanceViewPresenter {
    
    static func config(withFinishedAttendanceViewController vc: FinishedAttendanceViewController) {
        let presenter = FinishedAttendancePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: FinishedAttendanceView
    
    required init(view: FinishedAttendanceView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
