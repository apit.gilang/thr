//
//  PerformanceViewController.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/29/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantPerformance {
    // TODO: Add segue ids
}

enum EnumPerformanceSection {
    case workHour, leave, statistic
}

// MARK: -  PerformanceViewController

class PerformanceViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: PerformancePresenter!
    var sections:[EnumPerformanceSection] = [.workHour, .leave, .statistic]
    var dashboard: Dashboard?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        PerformancePresenter.config(withPerformanceViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: String(describing: WeeklyAttendanceTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: WeeklyAttendanceTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: LeaveAmountTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: LeaveAmountTableViewCell.self))
        tableView.register(UINib(nibName: String(describing: StatisticTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: StatisticTableViewCell.self))
    }
}

extension PerformanceViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch sections[indexPath.section] {
        case .workHour:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WeeklyAttendanceTableViewCell.self), for: indexPath) as! WeeklyAttendanceTableViewCell
            if let workingHour = dashboard?.workingHour {
                cell.setupWeeklyContent(withData: workingHour)
            }
            return cell
        case .leave:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LeaveAmountTableViewCell.self), for: indexPath) as! LeaveAmountTableViewCell
            if let leave = dashboard?.leave {
                cell.setupLeaveContent(withData: leave)
            }
            return cell
        case .statistic:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: StatisticTableViewCell.self), for: indexPath) as! StatisticTableViewCell
            if let performances = dashboard?.performances, performances.count > 0 {
                cell.setupChartData(withData: performances)
                cell.noDataImage.isHidden = true
            } else {
                cell.noDataImage.isHidden = false
            }
            return cell
        }
    }
}

// MARK: - PerformanceView

extension PerformanceViewController: PerformanceView {
    // TODO: implement view methods
}
