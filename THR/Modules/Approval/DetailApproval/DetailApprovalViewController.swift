//
//  DetailApprovalViewController.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit
import BottomPopup
import AssistantKit
import NVActivityIndicatorView
import GoogleMaps

// MARK: - SegueConstants

enum SegueConstantDetailApproval {
    // TODO: Add segue ids
}

protocol DetailApprovalDelegate {
    func approvalSucceded()
}

// MARK: -  DetailApprovalViewController

class DetailApprovalViewController: CustomBottomPopupViewController {
    
    // MARK: Properties
    @IBOutlet weak var requestByLabel: UILabel!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var leaveTypeLabel: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var approvalActionView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var stackView: UIStackView!
    
    var presenter: DetailApprovalPresenter!
    var approval: ApprovalData?
    var leave: LeaveData?
    var overtime: OvertimeData?
    var offsite: OffsiteRequestData?
    var delegate: DetailApprovalDelegate?
    var locationManager = CLLocationManager()
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DetailApprovalPresenter.config(withDetailApprovalViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let data = overtime {
            senderNameLabel.text = "You"
            dateLabel.text = data.date
            timeLabel.text = data.getTimeToShow(forDetail: true)
            descriptionLabel.text = data.reason
            approvalActionView.isHidden = true
            mapView.isHidden = true
        } else if let data = leave {
            senderNameLabel.text = "You"
            dateLabel.text = data.getDateToShow()
            leaveTypeLabel.text = data.leaveType
            timeView.isHidden = true
            descriptionLabel.text = data.reason
            approvalActionView.isHidden = true
            mapView.isHidden = true
        } else if let data = approval {
            senderNameLabel.text = data.userFullname
            dateLabel.text = data.getDateToShow()
            timeLabel.text = data.note
            descriptionLabel.text = data.reason
            dateView.isHidden = data.approvalableType == .leave ? false : true
            timeView.isHidden = data.approvalableType == .overtime ? false : true
            mapView.isHidden = true
        } else if let data = offsite {
            requestByLabel.isHidden = true
            senderNameLabel.isHidden = true
            timeView.isHidden = true
            approvalActionView.isHidden = true
            dateLabel.text = data.getDateToShow()
            descriptionLabel.text = data.reason
            
            locationManager.delegate = self
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.requestLocation()
            } else {
                locationManager.requestWhenInUseAuthorization()
            }
        }
    }
    
    override var cornerRadius: CGFloat {
        return 20
    }
    
    override var height: CGFloat {
        switch Device.screen {
        case .inches_4_7, .inches_5_5, .inches_4_0:
            return stackView.frame.size.height
        default:
            return stackView.frame.size.height
        }
    }
    
    @IBAction func checkButtonPressed(_ sender: Any) {
        presenter.submitApproval(withId: approval?.id ?? 0, status: .approved)
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        presenter.submitApproval(withId: approval?.id ?? 0, status: .rejected)
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func drawCircle(position: CLLocationCoordinate2D, radius: Double) {
        let circle = GMSCircle(position: position, radius: radius)
        circle.strokeColor = UIColor.appColor(.blue)
        circle.fillColor = UIColor.appColor(.lightBlue).withAlphaComponent(0.25)
        circle.map = mapView
    }
}

extension DetailApprovalViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let data = offsite else {
            return
        }
        let position = CLLocationCoordinate2D(latitude: data.latitude ?? 0, longitude: data.longtitude ?? 0)
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "ic_offsite_location")
        marker.map = mapView
        drawCircle(position: position, radius: data.radius ?? 0)
        mapView.camera = GMSCameraPosition(target: position, zoom: 15, bearing: 0, viewingAngle: 0)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

// MARK: - DetailApprovalView

extension DetailApprovalViewController: DetailApprovalView {
    func onSuccessSubmitApproval() {
        dismiss(animated: true) {
            self.delegate?.approvalSucceded()
        }
    }
    
    func onErrorSubmitApproval(withMessage message: String) {
        dismiss(animated: true) {
            self.showPageError(withMessage: message)
        }
    }
}
