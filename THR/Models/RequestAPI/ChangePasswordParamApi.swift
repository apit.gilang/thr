//
//  ForgotPasswordParamApi.swift
//  THR
//
//  Created by Apit Aprida on 29/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

class ChangePasswordParamApi {
    
    var currentPassword: String?
    var password: String?
    var passwordConfirmation: String?
    
    func buildForParameters() -> [String: AnyObject] {
        var parameters: [String: AnyObject] = [:]
        parameters["user"] = [
            "current_password": currentPassword as AnyObject,
            "password": password as AnyObject,
            "password_confirmation": passwordConfirmation as AnyObject
        ] as AnyObject
        return parameters
    }
}
