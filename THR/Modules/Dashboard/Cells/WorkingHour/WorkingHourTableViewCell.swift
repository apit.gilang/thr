//
//  WorkingHourTableViewCell.swift
//  GreenHcm
//
//  Created by Apit Aprida on 06/03/20.
//  Copyright © 2020 Personal. All rights reserved.
//

import UIKit

class WorkingHourTableViewCell: UITableViewCell {

    @IBOutlet weak var startTimeValueLabel: UILabel!
    @IBOutlet weak var endTimeValueLabel: UILabel!
    @IBOutlet weak var lateValueLabel: UILabel!
    @IBOutlet weak var workValueLabel: UILabel!
    @IBOutlet weak var overtimeValueLabel: UILabel!
    
    @IBOutlet weak var startTimelabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var lateLabel: UILabel!
    @IBOutlet weak var lateHourLabel: UILabel!
    @IBOutlet weak var workLabel: UILabel!
    @IBOutlet weak var workHourLabel: UILabel!
    @IBOutlet weak var overtimeLabel: UILabel!
    @IBOutlet weak var overtimeHourLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
}
