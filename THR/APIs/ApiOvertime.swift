//
//  ApiOvertime.swift
//  THR
//
//  Created by Apit Aprida on 08/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiOvertime: URLRequestConvertible {
    
    case getOvertime
    case createOvertime(param: OvertimeParamApi)
    case getOvertimeType
    
    var path: String {
        switch self {
        case .getOvertimeType:
            return "/overtimes/overtime_type"
        default:
            return "/overtimes"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getOvertime, .getOvertimeType:
            return .get
        case .createOvertime:
            return .post
        }
    }
    
    var parameter: [String: AnyObject] {
        switch self {
        case .createOvertime(let param):
            return param.buildForParameters()
        default:
            return [:]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        urlRequest.thrHeaders(withToken: Settings.accessToken, secretKey: Settings.secretKey)
        return try HTTPMethod.get.rawValue == urlRequest.httpMethod ? URLEncoding.default.encode(urlRequest, with: self.parameter) : JSONEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
