//
//  AppDelegate.swift
//  THR
//
//  Created by Apit Aprida on 28/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import netfox
import SDWebImage
import Firebase

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let context = appDelegate.persistentContainer.viewContext

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Register google map api key
        GMSServices.provideAPIKey(ApiConstants.GoogleMaps.apiKey)
//        NFX.sharedInstance().start()
        registerForPushNotifications()
        if Settings.accessToken == "" {
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let rootView = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            window?.rootViewController = UINavigationController(rootViewController: rootView)
        } else {
            let storyboard = UIStoryboard(name: "CustomTabbar", bundle: nil)
            let rootView = storyboard.instantiateViewController(withIdentifier: "CustomTabbarViewController") as! CustomTabbarViewController
            window?.rootViewController = UINavigationController(rootViewController: rootView)
        }
        FirebaseApp.configure()
        Messaging.messaging().delegate = self

        // Setup push notification
        registerForPushNotifications()
        window?.makeKeyAndVisible()
        return true
    }
    
    private func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, _) in
            print("Permission granted: \(granted)")
            guard granted else { return }
            self.getNotificationSettings()
        }
    }

    private func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        print("Device Token: \(token)")

        Messaging.messaging().apnsToken = deviceToken
//        Messaging.messaging().setAPNSToken(deviceToken, type: .sandbox)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }

    func getResponseNotif(_ application: UIApplication, userInfo: [AnyHashable: Any]) {

        // Extract push notification info
        let pushNotificationInfo = PushNotificationInfo(userInfo: userInfo)

        // Check if application was active, if yes then show alert message
        if application.applicationState == .active {
            self.showNotification(showNotification: pushNotificationInfo)
        } else {
            self.checkNotification(showAsAlertMessage: pushNotificationInfo)
        }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {

        // Handle push notification
        self.getResponseNotif(application, userInfo: userInfo)

        // Log receive push notification
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)

        // Handle push notification
        self.getResponseNotif(application, userInfo: userInfo)
        print(userInfo)
        // Log receive push notification
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        // Check if was valid url component
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let url = userActivity.webpageURL,
            let _ = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
                return false
        }

//        // Open root app
//        if DeepLinkUtils.isRootWithUrl(userActivity.webpageURL) {
//            openMainPage()
//            return true
//        }
//
//        // Open product id if was valid
//        if let productId: Int = DeepLinkUtils.getProductIdWithUrl(userActivity.webpageURL) {
//            openProductDetailWithId(productId)
//            return true
//        }
//
//        // Open promo url in app
//        if DeepLinkUtils.isPromoWithUrl(userActivity.webpageURL) {
//            if let urlInString = userActivity.webpageURL?.absoluteString {
//                showWebPage(urlInString)
//                return true
//            }
//        }

        // Open web page if was invalid
        if let url = userActivity.webpageURL {
            if application.canOpenURL(url) {
                application.open(url)
            }
        }

        return false
    }

    //Show Notif when app is active

    func showNotification(showNotification: ShowAsNotification) {
        let alert = UIAlertController(title: showNotification.getAlertTitle(), message: showNotification.getAlertMessage(), preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Close", style: .default, handler: nil))
        alert.addAction(UIAlertAction.init(title: "Open", style: .default, handler: { (_) in
            self.checkNotification(showAsAlertMessage: showNotification)
        }))
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }

    func checkNotification(showAsAlertMessage: ShowAsAlertMessage) {
        if showAsAlertMessage.isActionOpenWeb() {
//            self.showWebPage(showAsAlertMessage.getUrl(), titleData: showAsAlertMessage.getTitle())
        } else if showAsAlertMessage.isApproval() {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "showApproval"), object: nil)
        } else if showAsAlertMessage.isOvertime() {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "showOvertime"), object: nil)
        }
    }

    func openApproval() {
        let storyboard = UIStoryboard(name: "Approval", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ApprovalViewController") as! ApprovalViewController
        self.window?.rootViewController = UINavigationController(rootViewController: viewController)
        self.window?.makeKeyAndVisible()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "THR")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension AppDelegate: MessagingDelegate {

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        if let token = fcmToken {
            print(token)
        }
        Settings.fcmToken = fcmToken
    }
}

