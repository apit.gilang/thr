//
//  ComingSoonViewController.swift
//  THR
//
//  Created by Apit Aprida on 29/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SegueConstants

enum SegueConstantComingSoon {
    // TODO: Add segue ids
}

// MARK: -  ComingSoonViewController

class ComingSoonViewController: ViewController {
    
    // MARK: Properties
    
    var presenter: ComingSoonPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ComingSoonPresenter.config(withComingSoonViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true)
    }
}

// MARK: - ComingSoonView

extension ComingSoonViewController: ComingSoonView {
    // TODO: implement view methods
}
