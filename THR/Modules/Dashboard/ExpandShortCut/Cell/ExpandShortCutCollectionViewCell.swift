//
//  ExpandShortCutCollectionViewCell.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit

class ExpandShortCutCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var shortCutImage: UIImageView!
    @IBOutlet weak var shortCutTitleLabel: UILabel!
    
    func setupContentShortCut(withShortCut shortCut: ShortCut) {
        shortCutImage.image = shortCut.image
        shortCutTitleLabel.text = shortCut.name
    }

}
