//
//  THRApi.swift
//  THR
//
//  Created by Apit Aprida on 07/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

typealias SuccessResponse = (JSON?) -> Void
typealias ErrorResponse  = (String) -> Void

class THRApi {

    static let instance = THRApi()

    var alamoFireManager: Session = Session.default
    var request: Request?
    private var indicator: NVActivityIndicatorView!

    init() {
        self.setAFconfig()
    }

    func setAFconfig() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 60
        self.alamoFireManager = Session(configuration: configuration)
    }

    func request(_ request: URLRequestConvertible, success:@escaping SuccessResponse, error:@escaping ErrorResponse) {
        showActivityIndicator()
        self.request = alamoFireManager.request(request).responseJSON { response in
            #if DEBUG
            if let errorMessage = response.error?.localizedDescription {
                print(errorMessage)
            }
            #endif
            let statusCode = response.response?.statusCode ?? 500
            if 500 == response.response?.statusCode {
                self.hideActivityIndicator()
                if let reachability = Reachability(), reachability.isReachable == false {
                    error(Response.MESSAGE_NOT_CONNECTED_INTERNET)
                } else {
                    if let _ = response.error?.localizedDescription {
                        error(Response.MESSAGE_INTERNAL_SERVER_ERROR)
                    } else {
                        error(Response.MESSAGE_SYSTEM_ERROR)
                    }
                }
            } else if let value = response.value {
                self.hideActivityIndicator()
                let jsonValue = JSON(value)
                print(jsonValue)
                if statusCode >= 300 {
                    let message = jsonValue["error"].stringValue
                    if statusCode == 401 && message == "token expired" {
                        self.showTokenExpired()
                    } else {
                        if message.isEmpty {
                            error(Response.MESSAGE_SYSTEM_ERROR)
                        } else {
                            error(message)
                        }
                    }
                } else {
                    if jsonValue["success"].boolValue {
                        success(jsonValue)
                    } else {
                        error(jsonValue["message"].stringValue)
                    }
                }
            } else {
                self.hideActivityIndicator()
                self.showPageError(withMessage: Response.MESSAGE_NOT_FOUND)
            }
//            self.invalidateAndConfigure()
        }
    }

    func uploadImage(_ request: URLRequestConvertible, multipartFormData: @escaping (MultipartFormData) -> Void, success: @escaping SuccessResponse, error:@escaping ErrorResponse) {
        showActivityIndicator()
        let req = alamoFireManager.upload(multipartFormData: multipartFormData, with: request).uploadProgress(queue: .main) { (progress) in
            print("uploading file \(progress.fractionCompleted)")
        }
        req.responseJSON { (response) in
            #if DEBUG
            if let errorMessage = response.error?.localizedDescription {
                print(errorMessage)
            }
            #endif
            self.hideActivityIndicator()
            let statusCode = response.response?.statusCode ?? 500
            if 500 == response.response?.statusCode {
                if let reachability = Reachability(), reachability.isReachable == false {
                    error(Response.MESSAGE_NOT_CONNECTED_INTERNET)
                } else {
                    if let errorMessage = response.error?.localizedDescription {
                        error(errorMessage)
                    } else {
                        error(Response.MESSAGE_SYSTEM_ERROR)
                    }
                }
            } else if let value = response.value {
                let jsonValue = JSON(value)
                print(jsonValue)
                if statusCode >= 300 {
                    let message = jsonValue["error"].stringValue
                    if statusCode == 401 && message == "token expired" {
                        self.showTokenExpired()
                    } else {
                        if message.isEmpty {
                            error(Response.MESSAGE_SYSTEM_ERROR)
                        } else {
                            error(message)
                        }
                    }
                } else {
                    if jsonValue["success"].boolValue {
                        success(jsonValue)
                    } else {
                        error(jsonValue["message"].stringValue)
                    }
                }
            } else {
                self.showPageError(withMessage: Response.MESSAGE_NOT_FOUND)
            }
        }
    }
    
    private func showPageError(withMessage message: String) {
        UIApplication.shared.endIgnoringInteractionEvents()
        let errorVc = UIStoryboard(name: "PageError", bundle: nil).instantiateViewController(withIdentifier: "PageErrorViewController") as! PageErrorViewController
        errorVc.modalPresentationStyle = .fullScreen
        errorVc.message = message
        appDelegate.window?.makeKeyAndVisible()
        appDelegate.window?.rootViewController?.present(errorVc, animated: true, completion: nil)
    }
    
    private func showTokenExpired() {
        UIApplication.shared.endIgnoringInteractionEvents()
        let alert = UIAlertController(title: "", message: "Token Expired", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (_) in
            Settings.accessToken = ""
            Settings.secretKey = ""
            Settings.userFullname = ""
            let loginVc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            loginVc.modalPresentationStyle = .fullScreen
            appDelegate.window?.rootViewController = UINavigationController(rootViewController: loginVc)
            appDelegate.window?.makeKeyAndVisible()
        }
        alert.addAction(ok)
        appDelegate.window?.windowLevel = UIWindow.Level.alert + 1
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    private func showActivityIndicator() {
        UIApplication.shared.beginIgnoringInteractionEvents()
        let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin, color: UIColor.appColor(.blue), padding: 20)
        appDelegate.window?.addSubview(indicator)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        if let xAnchor = appDelegate.window?.centerXAnchor {
            indicator.centerXAnchor.constraint(equalTo: xAnchor).isActive = true
        }
        if let yAnchor = appDelegate.window?.centerYAnchor {
            indicator.centerYAnchor.constraint(equalTo: yAnchor).isActive = true
        }
        DispatchQueue.main.async {
            self.indicator.startAnimating()
        }
    }
    
    private func hideActivityIndicator() {
        UIApplication.shared.endIgnoringInteractionEvents()
        self.indicator.removeFromSuperview()
        DispatchQueue.main.async {
            self.indicator.stopAnimating()
        }
    }

    func invalidateAndConfigure() {
        self.alamoFireManager.session.finishTasksAndInvalidate()
        self.setAFconfig()
    }

    class Response {
        static let MESSAGE_SYSTEM_ERROR = "System failure"
        static let MESSAGE_INTERNAL_SERVER_ERROR = "Server failure"
        static let MESSAGE_NOT_CONNECTED_INTERNET = "No internet connection"
        static let MESSAGE_NOT_FOUND = "The server cannot find the requested page."
        static let MESSAGE_FORBIDDEN = "Access is forbidden to the requested page."

        static func shouldHiddenWarningImage(withMessage message: String?) -> Bool {
            return MESSAGE_INTERNAL_SERVER_ERROR == message || MESSAGE_NOT_CONNECTED_INTERNET == message
        }

        static func isMessageError(withMessage message: String?) -> Bool {
            return MESSAGE_SYSTEM_ERROR == message
        }

        static func isMessageNotConnectedInternet(withMessage message: String?) -> Bool {
            return MESSAGE_NOT_CONNECTED_INTERNET == message
        }
    }
}
