//
//  Notification.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/15/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class NotificationData {
    var id: Int?
    var notifableType: String?
    var notifableId: Int?
    var isRead: Bool?
    var isNewNotif: Bool?
    var fromUserFullname: String?
    var fromUserImage: String?
    var message: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        
        id = json["id"].int
        notifableType = json["notifable_type"].string
        notifableId = json["notifable_id"].int
        isRead = json["is_read"].bool
        isNewNotif = json["is_new_notif"].bool
        fromUserFullname = json["from_user_fullname"].string
        fromUserImage = json["from_user_image"].string
        message = json["message"].string?.htmlToString
    }
}
