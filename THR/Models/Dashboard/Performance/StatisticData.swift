//
//  StatisticData.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class StatisticData {
    var color: String?
    var name: String?
    var result: Int?

    init(json: JSON?, color: String) {
        guard let json = json else {
            return
        }
        self.color = color
        name = json["name"].string
        result = json["result"].int
    }
}
