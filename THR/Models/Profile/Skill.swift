//
//  Skill.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Skill {
    var id: Int?
    var userId: Int?
    var name: String?
    var percentage: Int?
    var note: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        userId = json["user_id"].int
        name = json["name"].string
        percentage = json["percentage"].int
        note = json["note"].string
    }
}
