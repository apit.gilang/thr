//
//  CustomTabbarPresenter.swift
//  THR
//
//  Created by Apit Gilang Aprida on 6/28/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON
import ESTabBarController_swift

protocol CustomTabbarViewPresenter: class {
    init(view: CustomTabbarView)
    func setupCustomTabbar(withController vc: CustomTabbarViewController)
    func setupDashboardNavigation(withController vc: CustomTabbarViewController)
}

protocol CustomTabbarView: class {
    func handlingNotification()
}

class CustomTabbarPresenter: CustomTabbarViewPresenter {
    
    static func config(withCustomTabbarViewController vc: CustomTabbarViewController) {
        let presenter = CustomTabbarPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: CustomTabbarView
    
    required init(view: CustomTabbarView) {
        self.view = view
    }
    
    func setupCustomTabbar(withController vc: CustomTabbarViewController) {
        vc.tabBar.shadowImage = UIImage(named: "transparent")
        vc.tabBar.backgroundImage = UIImage(named: "background_dark")
        vc.tabBar.addShadow(location: .top, color: .darkGray, opacity: 0.2, radius: 6)
        
        let dashboard = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        let absence = UIStoryboard(name: "Attendance", bundle: nil).instantiateViewController(withIdentifier: "AttendanceViewController") as! AttendanceViewController
        let profile = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
        dashboard.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "ic_home")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "ic_home_selected")?.withRenderingMode(.alwaysOriginal))
        absence.tabBarItem = ESTabBarItem.init(ExampleIrregularityContentView(), title: "Clock In", image: UIImage(named: "ic_check_in"), selectedImage: UIImage(named: "ic_check_in"))
        profile.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "ic_profile")?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage(named: "ic_profile_selected")?.withRenderingMode(.alwaysOriginal))
        
        vc.viewControllers = [dashboard, absence, profile]
    }
    
    func setupDashboardNavigation(withController vc: CustomTabbarViewController) {
//        vc.navigationController?.navigationBar.backIndicatorImage = iconBack
//        vc.navigationController?.navigationBar.backIndicatorTransitionMaskImage = iconBack
        vc.navigationController?.navigationBar.tintColor = UIColor.white
        vc.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        vc.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        vc.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        vc.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
        vc.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        vc.navigationController?.navigationBar.shadowImage = UIImage()
        vc.navigationController?.navigationBar.barTintColor = UIColor.appColor(.blue)
        vc.navigationController?.navigationBar.barStyle = .black
        vc.navigationController?.navigationBar.isTranslucent = false
    }
}
