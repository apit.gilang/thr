//
//  FaceScanningPresenter.swift
//  THR
//
//  Created by Apit Aprida on 30/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol FaceScanningViewPresenter: class {
    init(view: FaceScanningView)
    func requestAttendance(withParamter param: AttendanceParamApi, isCheckout: Bool)
    func registerFace(withImage image: Data)
}

protocol FaceScanningView: class {
    func onSuccessRequestLeave()
    func onSuccessRegisterFace()
    func onErrorRequestAPI(withMessage message: String)
}

class FaceScanningPresenter: FaceScanningViewPresenter {
    
    static func config(withFaceScanningViewController vc: FaceScanningViewController) {
        let presenter = FaceScanningPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: FaceScanningView
    
    required init(view: FaceScanningView) {
        self.view = view
    }
    
    func registerFace(withImage image: Data) {
        THRApi.instance.uploadImage(ApiProfile.registerFace, multipartFormData: { (multipartFormData) in
            multipartFormData.append(image, withName: "face_account", fileName: "images.jpg", mimeType: "image/jpg")
        }, success: { (_) in
            self.view.onSuccessRegisterFace()
        }) { (error) in
            self.view.onErrorRequestAPI(withMessage: error)
        }
    }
    
    func requestAttendance(withParamter param: AttendanceParamApi, isCheckout: Bool) {
        if let image = param.image, let longitude = param.longtitude, let latitude = param.latitude {
            if isCheckout {
                print("Start request \(Date().toString(format: .isoDateTimeSec))")
                THRApi.instance.uploadImage(ApiAttendance.checkout(id: param.id ?? 0), multipartFormData: { (multipartFormData) in
                    multipartFormData.append(image, withName: "attendance[image]", fileName: "images.jpg", mimeType: "image/jpg")
                    multipartFormData.append("\(longitude)".data(using: .utf8)!, withName: "attendance[longtitude]")
                    multipartFormData.append("\(latitude)".data(using: .utf8)!, withName: "attendance[latitude]")
                }, success: { (json) in
                    print("End request \(Date().toString(format: .isoDateTimeSec))")
                    self.view.onSuccessRequestLeave()
                }) { (error) in
                    print("End request \(Date().toString(format: .isoDateTimeSec))")
                    self.view.onErrorRequestAPI(withMessage: error)
                }
            } else {
                print("Start request \(Date().toString(format: .isoDateTimeSec))")
                THRApi.instance.uploadImage(ApiAttendance.checkin, multipartFormData: { (multipartFormData) in
                    multipartFormData.append(image, withName: "attendance[image]", fileName: "images.jpg", mimeType: "image/jpg")
                    multipartFormData.append("\(longitude)".data(using: .utf8)!, withName: "attendance[longtitude]")
                    multipartFormData.append("\(latitude)".data(using: .utf8)!, withName: "attendance[latitude]")
                }, success: { (json) in
                    self.view.onSuccessRequestLeave()
                    print("End request \(Date().toString(format: .isoDateTimeSec))")
                }) { (error) in
                    self.view.onErrorRequestAPI(withMessage: error)
                    print("End request \(Date().toString(format: .isoDateTimeSec))")
                }
            }
        } else {
            self.view.onErrorRequestAPI(withMessage: "Your location not detected, try to check the location permission in privacy setting.")
        }
    }
}
