//
//  History.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class History {
    var name: String?
    var start: String?
    var end: String?
    var note: String?
    var type: String?
    var badge: String?
    var sort: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        name = json["name"].string
        start = json["start"].string
        end = json["end"].string
        note = json["note"].string
        type = json["type"].string
        badge = json["badge"].string
        sort = json["sort"].string
    }
    
    func getDate() -> String {
        return "\(start ?? "") - \(end ?? "")"
    }
    
    func getType() -> String {
        return "\(type ?? "") At"
    }
    
    func getColor() -> UIColor {
        return type == "education" ? UIColor.appColor(.orange) : UIColor.appColor(.blue)
    }
    
    func getTimeImage() -> UIImage {
        return type == "education" ? UIImage(named: "ic_time_orange") ?? UIImage() : UIImage(named: "ic_time_blue") ?? UIImage()
    }
    
    func getSkillImage() -> UIImage {
        return type == "education" ? UIImage(named: "ic_education") ?? UIImage() : UIImage(named: "ic_work") ?? UIImage()
    }
}
