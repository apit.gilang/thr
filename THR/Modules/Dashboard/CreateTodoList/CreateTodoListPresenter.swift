//
//  CreateTodoListPresenter.swift
//  THR
//
//  Created by Apit Aprida on 05/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol CreateTodoListViewPresenter: class {
    init(view: CreateTodoListView)
    func createTodo(withParameter parameter: CreateTodoParamApi)
}

protocol CreateTodoListView: class {
    func onSuccessCreateTodo()
    func onErrorCreateTodo(withMessage message: String)
}

class CreateTodoListPresenter: CreateTodoListViewPresenter {
    
    static func config(withCreateTodoListViewController vc: CreateTodoListViewController) {
        let presenter = CreateTodoListPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: CreateTodoListView
    
    required init(view: CreateTodoListView) {
        self.view = view
    }
    
    func createTodo(withParameter parameter: CreateTodoParamApi) {
        THRApi.instance.request(ApiDashboard.createTodo(parameter: parameter), success: { (json) in
            self.view.onSuccessCreateTodo()
        }) { (error) in
            self.view.onErrorCreateTodo(withMessage: error)
        }
    }
    
    func updateTodo(with id: Int, parameter: CreateTodoParamApi) {
        THRApi.instance.request(ApiTodo.editTodo(id: id, parameter: parameter), success: { (json) in
            self.view.onSuccessCreateTodo()
        }) { (error) in
            self.view.onErrorCreateTodo(withMessage: error)
        }
    }
    
    func deleteTodo(with id: Int) {
        THRApi.instance.request(ApiTodo.deleteTodo(id: id), success: { (json) in
            self.view.onSuccessCreateTodo()
        }) { (error) in
            self.view.onErrorCreateTodo(withMessage: error)
        }
    }
}
