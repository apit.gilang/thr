//
//  NSNotificationName+Extension.swift
//  THR
//
//  Created by Apit Aprida on 13/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    static let didReceivedWorkingHour = NSNotification.Name("didReceivedWorkingHour")
    static let didReceivedNotificationCount = NSNotification.Name("didReceivedNotificationCount")
}
