//
//  StatisticTableViewCell.swift
//  THR
//
//  Created by Apit Aprida on 29/06/20.
//  Copyright © 2020 apit. All rights reserved.
//

import UIKit
import Charts

class StatisticTableViewCell: UITableViewCell {
    
    @IBOutlet weak var performanceChart: LineChartView!
    @IBOutlet weak var noDataImage: UIImageView!
    
    var chartDataEntry: ChartDataEntry?
    var dates = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        setViewChart(chartView: performanceChart)
    }
    
    func setupChartData(withData performances: [Performance]) {
        var weekDegree = [[StatisticData]]()
        
        var greenDataArr = [ChartDataEntry]()
        var greenArr = LineChartDataSet()
        var redDataArr = [ChartDataEntry]()
        var redArr = LineChartDataSet()
        var orangeDataArr = [ChartDataEntry]()
        var orangeArr = LineChartDataSet()
        var purpleDataArr = [ChartDataEntry]()
        var purpleArr = LineChartDataSet()
        
        for dataPerformance in performances {
            weekDegree.append(dataPerformance.statisticData)
            dates.append(dataPerformance.date!)
        }
        
        for thedata in weekDegree.enumerated() {
            for data in thedata.element {
                if  data.color == "green" {
                    chartDataEntry = ChartDataEntry(x: Double(thedata.offset), y: Double(data.result!))
                    greenDataArr.append(chartDataEntry!)
                    greenArr = setDataCount(dataEntry: greenDataArr, label: data.name!, color: .green)
                } else  if  data.color == "red" {
                    chartDataEntry = ChartDataEntry(x: Double(thedata.offset), y: Double(data.result!))
                    redDataArr.append(chartDataEntry!)
                    redArr = setDataCount(dataEntry: redDataArr, label: data.name!, color: .red)
                } else  if  data.color == "orange" {
                    chartDataEntry = ChartDataEntry(x: Double(thedata.offset), y: Double(data.result!))
                    orangeDataArr.append(chartDataEntry!)
                    orangeArr = setDataCount(dataEntry: orangeDataArr, label: data.name!, color: .orange)
                } else  if  data.color == "purple" {
                    chartDataEntry = ChartDataEntry(x: Double(thedata.offset), y: Double(data.result!))
                    purpleDataArr.append(chartDataEntry!)
                    purpleArr = setDataCount(dataEntry: purpleDataArr, label: data.name!, color: .purple)
                }
            }
        }
        
        let data = LineChartData(dataSets: [orangeArr, redArr, greenArr, purpleArr])
        
        performanceChart.data = data
    }
    
    func setDataCount(dataEntry: [ChartDataEntry], label: String, color: UIColor) -> LineChartDataSet {
        let set1 = LineChartDataSet(entries: dataEntry, label: label)
        set1.setColor(color)
        set1.lineWidth = 2.5
        set1.setCircleColor(color)
        set1.drawCircleHoleEnabled = false
        set1.circleRadius = 5
        set1.circleHoleRadius = 2.5
        set1.mode = .cubicBezier
        set1.highlightColor = UIColor.appColor(.blue)
        set1.drawValuesEnabled = false
        set1.axisDependency = .left
        
        return set1
        
    }
    
    func setViewChart(chartView: LineChartView, isAbsence: Bool = false) {
        chartView.delegate = self
        chartView.backgroundColor = UIColor.white
        chartView.leftAxis.enabled = true
        chartView.rightAxis.enabled = false
        chartView.legend.enabled = true
        
        let xAxis = chartView.xAxis
        xAxis.valueFormatter = self
        xAxis.labelPosition = .bottom
        xAxis.granularity = 1
        xAxis.drawAxisLineEnabled = true
        
        let leftAxis = chartView.leftAxis
        if !isAbsence {
            leftAxis.axisMaximum = 100.0
            leftAxis.axisMinimum = 60.0
        } else {
            leftAxis.axisMaximum = 1.5
            leftAxis.axisMinimum = 0.0
        }
        xAxis.labelTextColor = UIColor.appColor(.darkGray)
        leftAxis.labelTextColor = UIColor.appColor(.darkGray)
        chartView.animate(xAxisDuration: 2, yAxisDuration: 2)
    }
}

extension StatisticTableViewCell: ChartViewDelegate, IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        var results = [String]()
        for dateString in dates {
            if let date = Date(fromString: dateString, format: .isoDate) {
                results.append(date.toString(format: .custom("MMM")))
            }
        }
        return results[Int(value) % results.count]
    }
}
