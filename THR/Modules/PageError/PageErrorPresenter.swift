//
//  PageErrorPresenter.swift
//  THR
//
//  Created by Apit Aprida on 29/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol PageErrorViewPresenter: class {
    init(view: PageErrorView)
    // TODO: Declare view presenter methods
}

protocol PageErrorView: class {
    // TODO: Declare view methods
}

class PageErrorPresenter: PageErrorViewPresenter {
    
    static func config(withPageErrorViewController vc: PageErrorViewController) {
        let presenter = PageErrorPresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: PageErrorView
    
    required init(view: PageErrorView) {
        self.view = view
    }
    
    // TODO: Implement view presenter methods
}
