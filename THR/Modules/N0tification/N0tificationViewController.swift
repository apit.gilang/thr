//
//  N0tificationViewController.swift
//  THR
//
//  Created by Apit Gilang Aprida on 7/11/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import UIKit


// MARK: - SegueConstants

enum SegueConstantN0tification {
    // TODO: Add segue ids
}

// MARK: -  N0tificationViewController

class N0tificationViewController: ViewController {
    
    var presenter: N0tificationPresenter!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        N0tificationPresenter.config(withN0tificationViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

}

// MARK: - N0tificationView

extension N0tificationViewController: N0tificationView {
    // TODO: implement view methods
}

