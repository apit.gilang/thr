//
//  Contact.swift
//  THR
//
//  Created by Apit Aprida on 02/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Contact {
    var id: Int?
    var userId: Int?
    var emergencyName: String?
    var emergencyNumber: String?
    var emergencyRelation: String?
    var home: String?
    var handphone: String?
    var office: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        userId = json["user_id"].int
        emergencyName = json["emergency_name"].string
        emergencyNumber = json["emergency_number"].string
        emergencyRelation = json["emergency_relation"].string
        home = json["home"].string
        handphone = json["handphone"].string
        office = json["office"].string
    }
}
