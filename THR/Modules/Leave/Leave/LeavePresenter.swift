//
//  LeavePresenter.swift
//  THR
//
//  Created by Apit Aprida on 03/07/20.
//  Copyright © 2020 apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol LeaveViewPresenter: class {
    init(view: LeaveView)
    //  Setup View
    func registerCells(withViewController vc: LeaveViewController)
    // Setup data
    func getLeave(isShowToast: Bool)
}

protocol LeaveView: class {
    func onSuccessGetLeave(isShowToast: Bool)
    func onErrorGetLeave(withMessage message: String)
}

class LeavePresenter: LeaveViewPresenter {
    
    static func config(withLeaveViewController vc: LeaveViewController) {
        let presenter = LeavePresenter(view: vc)
        vc.presenter = presenter
    }
    
    let view: LeaveView
    var leave: Leave?
    
    required init(view: LeaveView) {
        self.view = view
    }
    
    //  Setup View
    func registerCells(withViewController vc: LeaveViewController) {
        vc.tableView.register(UINib(nibName: String(describing: RequestAttendanceTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: RequestAttendanceTableViewCell.self))
        vc.collectionView.register(UINib(nibName: String(describing: RequestAttendanceCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: RequestAttendanceCollectionViewCell.self))
    }
    
    // Setup data
    func getLeave(isShowToast: Bool) {
        THRApi.instance.request(ApiLeave.getLeave, success: { (json) in
            self.leave = Leave(json: json)
            self.view.onSuccessGetLeave(isShowToast: isShowToast)
        }) { (error) in
            self.view.onErrorGetLeave(withMessage: error)
        }
    }
}
